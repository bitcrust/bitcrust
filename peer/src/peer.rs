use std::fmt::{self, Debug};
use std::sync::Arc;
use std::io;
use std::net;
use std::time::{ Instant, Duration};
use std::net::{Ipv6Addr};
use std::str::FromStr;
use std::time::{UNIX_EPOCH, SystemTime};

use crossbeam::channel;
use network_encoding::*;

use bitcrust_net::*;

use crate::sync;
use bitcrust_store as store;
use self::store::ResultExt;

use crate::connection;
use rayon;

#[derive(Debug)]
pub enum PeerError {
    ExpectedVersion,
    ExpectedVerack,
    NoNetworkService,
    UnexpectedVersion,
    UnexpectedVerack,

    OutOfOrderHeader,

    IoError(io::Error),
    StoreError(store::StoreError),
    VerifyError(store::VerifyError),

    Stalled { speed: u64, median_speed: u64},


}

impl From<io::Error> for PeerError {
    fn from(e: io::Error) -> PeerError {
        PeerError::IoError(e)
    }
}
impl From<store::StoreError > for PeerError {
    fn from(e: store::StoreError) -> PeerError {
        PeerError::StoreError(e)
    }
}
impl From<store::VerifyError > for PeerError {
    fn from(e: store::VerifyError) -> PeerError {
        PeerError::VerifyError(e)
    }
}

impl fmt::Display for PeerError {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "{:?}", self)
    }
}


enum LocalMessage {
    BlockFetched(Result<Hash, PeerError>),
}

/// Peer bootstrapping follows the following sequence:
/// New client == -->
/// Remote peer == <--
///
/// --> Version
/// <-- Version
/// --> Verack
///
/// After the handshake, other communication can occur
pub struct Peer {
    db: Arc<store::Db>,
    host: String,
    pub peerinfo: sync::PeerInfo,
    connection: connection::Connection,

    peer_height: u32,
    send_compact: bool,
    send_headers: bool,
    synced_headers: bool,
    synced: bool,
    last_send_getheaders: Option<Instant>,
    last_local_manage: Instant,
    receiver: channel::Receiver<LocalMessage>,
    sender: channel::Sender<LocalMessage>,
    busy_list: sync::BusyList,

}

impl Debug for Peer {
    fn fmt(&self, f: &mut fmt::Formatter) -> Result<(), fmt::Error> {
        write!(f,
               r"Peer {{
    send_compact: {},
    send_headers: {},
    }}",
               self.send_compact,
               self.send_headers,
               )
    }
}

impl Peer {
    pub fn new(addr: net::SocketAddrV6,
               db: store::Db)
                                -> Result<Peer, PeerError> {

        // Setup panic handler

        let connection = connection::Connection::new(addr)?;
        let (sender, receiver) = channel::unbounded();
        Ok(Peer {
            db: Arc::new(db),
            host: addr.to_string(),
            connection: connection,
            peerinfo: sync::PeerInfo::new(addr),
            peer_height: 0,
            send_compact: false,
            send_headers: false,
            synced_headers: false,
            last_send_getheaders: None,
            last_local_manage: Instant::now(),
            synced: false,
            sender,
            receiver,
            busy_list: sync::BusyList::open()?,
        })
    }

    fn handle_message_block(db: &store::Db, block: BlockMessage) -> Result<Hash, PeerError> {

        let header = match store::Header::add(db, &block.payload[0..80]).unwrap() {
            store::AddResult::New ( header )      => header,
            store::AddResult::Orphan { .. }       => return Err(PeerError::OutOfOrderHeader),
            store::AddResult::Exists { hash, .. } => store::Header::get(db, &hash).unwrap(),
        };

        match header.add_transactions(db, &block.payload[80..])? {

            store::AddResult::New(_) => {}, // all good; do nothing

            store::AddResult::Exists { .. } => {
                // Also not really a problem. Can happen occasionally when multiple peers race for the same block
                warn!("Adding duplicate block {}", header.hash)
            },

            store::AddResult::Orphan { .. } => {
                // all good
                crate::bg_connect::spawn()?;
            },
        }

        Ok(header.hash)
    }

    fn handle_message_headers(&mut self, headers: HeadersMessage) -> Result<(), PeerError> {

        let len = headers.headers.len();
        if len == 0 {
            self.synced_headers = true;
            self.peerinfo.task_current = None;
            return Ok(());
        }

        let first = &headers.headers[0].hdr.clone()[..];
        {
            match store::Header::add(&self.db, &first).unwrap_dberr() {
                Err(x) => return Err(PeerError::from(x)),

                Ok(store::AddResult::Exists { hash }) => {
                    // This means someone else is doing the same job faster;
                    // We aren't synced yet; but will await trying
                    debug!("Header {} already added!", hash);
                    self.last_send_getheaders = Some(Instant::now());
                },

                Ok(store::AddResult::Orphan { hash, .. }) => {
                    warn!("Out of order header {}", hash);
                    return Err(PeerError::OutOfOrderHeader);
                },

                Ok(store::AddResult::New ( _header )) => {
                    self.last_send_getheaders = None;
                    // Add all
                    let count = headers.headers.len();
                    for hdr in headers.headers {
                        match store::Header::add(&self.db, &hdr.hdr[..]).unwrap_dberr() {
                            Ok(store::AddResult::Orphan { .. }) => {
                                return Err(PeerError::OutOfOrderHeader)
                            },
                            Ok(_) => (),
                            Err(x) => {
                                return Err(PeerError::from(x))
                            }
                        }
                    }
                    info!("Added {} headers ", count);
                    if count < 2000 {
                        self.synced_headers = true;
                    }
                }
            }
        }
        self.peerinfo.task_current = None;

        Ok(())
    }


    fn handle_message(&mut self, message: Message) -> Result<(), PeerError> {
        match message {

            Message::Version(_) => {
                // handshake already done here
                return Err(PeerError::UnexpectedVersion);
            }
            Message::Verack => {
                // handshake already done here
                return Err(PeerError::UnexpectedVerack);
            }
            Message::Ping(nonce) => {
                let _ = self.connection.send_message(&Message::Pong(nonce));
            },
            Message::Pong(_nonce) => {
                // Thanks. Ignore
            }
            Message::SendHeaders => {
                self.send_headers = true;
            }

            Message::Addr(_addrs) => {
                /*let _ = self.sender.try_send(ClientMessage::Addrs(addrs.addrs.clone()));
                // Ensure that we don't realocate repeatedly in here
                self.addrs.reserve(addrs.addrs.len());
                for addr in addrs.addrs {
                    self.addrs.insert(addr);
                }
                */
            }
            Message::GetAddr => {
                //let msg = AddrMessage { addrs: self.addrs.iter().cloned().collect() };
                //let _ = self.connection.send_message(&Message::Addr(msg));
            }
            Message::GetHeaders(_msg) => {

            }

            Message::Headers(headers) => {
                self.handle_message_headers(headers)?;

            }
            Message::Inv(_inv) => {}
            Message::Tx(_transaction) => {},

            Message::GetData(_data) => {}

            Message::Block(block) => {

                let db = self.db.clone();
                let sender = self.sender.clone();
                rayon::spawn(move || {
                    let result = Self::handle_message_block(&db, block);
                    sender.try_send(LocalMessage::BlockFetched(result)).unwrap();
                });
            },


            Message::NotFound(_not_found) => {}
            _ => {
                debug!("Ignoring unsupported message: {:?}", message);
            }
        };
        Ok(())
    }


    /// Performs the version/verack handshake
    pub fn handshake(&mut self) -> Result<(), PeerError> {

        // Send version
        self.connection.send_message(&Peer::version())?;

        // Read version
        let msgs = self.connection.read_message(None)?;
        if msgs.len() != 1 {
            return Err(PeerError::ExpectedVersion)
        }
        match msgs[0] {
            Message::Version(ref v) => {
                self.peer_height = v.start_height as u32;
                self.peerinfo.user_agent = v.user_agent.clone();
                self.peerinfo.services = v.services.as_u64();

                if !v.services.network() {
                    debug!("Node does not provide IBD services");
                    return Err(PeerError::NoNetworkService);
                }
            },
            _ => return Err(PeerError::ExpectedVersion)
        }

        // Send verack
        self.connection.send_message(&Message::Verack)?;

        // Read verack
        let msgs = self.connection.read_message(None)?;
        if msgs.len() != 1 || !msgs[0].is_verack() {
            return Err(PeerError::ExpectedVerack)
        }

        info!("Handshake completed");

        Ok(())
    }

    pub fn run(&mut self, _send_version: bool) -> Result<(), PeerError> {

        self.handshake()?;

        self.send_get_headers()?;
        loop {

            self.peerinfo.write()?; // write current state to filesystem

            // Handle messages coming in from the network
            for msg in self.connection.read_message(None)? {
                self.handle_message(msg)?;
            };

            // Run any new IBD/catch up task
            if self.peerinfo.task_current.is_none() {
                self.run_next_task()?;
            }

            let (read,written) = self.connection.extract_counters();
            self.peerinfo.add_counters(read,written);

            // Process any messages posted asynchronously
            for local_msg in self.receiver.try_iter() {
                match local_msg {
                    LocalMessage::BlockFetched(result) => {
                        let hash = result?;
                        self.busy_list.unclaim(&hash);
                        if self.peerinfo.blocks_in_flight == 0 {
                            warn!("Unexpected block");
                        } else {
                            self.peerinfo.blocks_in_flight -= 1;
                            if self.peerinfo.blocks_in_flight == 0 {
                                self.peerinfo.task_current = None;
                            }
                        }

                    }
                }
            }
            if self.peerinfo.task_current.is_none() {
                self.local_manage()?;
                self.run_next_task()?;
            }

        }
    }


    fn run_next_task(&mut self) -> Result<(), PeerError> {

        if !self.synced_headers {

            // Run get_headers unless we just tried and we're outpaced by another peer
            let send_headers = match self.last_send_getheaders {
                None => true,
                Some(inst) => (Instant::now() - inst).as_secs() > 5
            };
            if send_headers {
                return self.send_get_headers();
            }
        }

        // acquire block download task

        // first find a reasonable count
        let next_needed = store::Header::iter_needs_transactions(&self.db).next();
        let count = if let Some(hdr) = next_needed {
            match hdr.height {
                0..=100_000 => 2000,
                100_001..=150_000 => 1000,
                150_001..=250_000 => 100,
                250_001..=480_000 => 20,
                480_001..=600_000 => 200,
                _ => 40,
            }
        } else { 1 };
        const CLAIM_TIMEOUT: std::time::Duration = std::time::Duration::from_secs(180);
        let blocks_needed: Vec<Hash> = store::Header::iter_needs_transactions(&self.db)
            .map(|h| h.hash)
            .filter(|h| self.busy_list.claim(&h, CLAIM_TIMEOUT))
            .take(count)
            .collect();

        if blocks_needed.is_empty() {
            self.synced = self.synced_headers;
        } else {
            let in_flight = blocks_needed.len();
            let first = blocks_needed[0];
            let msg = Message::GetData(GetdataMessage::new_get_blocks(blocks_needed));

            let task = format!("GET BLOCKS {}+{}", first, in_flight).to_owned();
            info!("Starting {}", task);
            self.peerinfo.blocks_in_flight = in_flight;
            self.peerinfo.task_current = Some(task);

            self.connection.send_message(&msg)?;
        }

        Ok(())
    }

    pub fn version() -> Message {
        Message::Version(VersionMessage {
            version: 70015,
            services: Services::from(0b00100001u64),
            timestamp: SystemTime::now().duration_since(UNIX_EPOCH).unwrap().as_secs() as i64,
            addr_recv: NetAddr::new(Services::from(0b00100001u64),
                                    Ipv6Addr::from_str("::ffff:127.0.0.1").unwrap(),
                                    8333),
            addr_send: NetAddr::new(Services::from(0b00100001u64),
                                    Ipv6Addr::from_str("::ffff:127.0.0.1").unwrap(),
                                    8333),
            nonce: 1,
            user_agent: "/bitcrust:0.1.0/".into(),
            start_height: 0,
            relay: false,
        })
    }

    fn _addr(ip: Ipv6Addr, port: u16, time: u32) -> NetAddr {
        NetAddr {
            time: time,
            services: Services::from(0b00100001u64),
            ip: ip,
            port: port,
        }
    }

    fn send_get_headers(&mut self) -> Result<(), PeerError> {
        let tip = store::Header::get_best_non_validated(&self.db);
        let locator = tip.locator(&self.db);
        let task_s = format!("GET HEADERS {}", tip.hash).to_owned();
        self.peerinfo.task_current = Some(task_s);

        Ok(self.connection.send_message(&Message::GetHeaders(GetheadersMessage {
            version: 70015,
            locator_hashes: locator,
            hash_stop: Hash::null(),
        }))?)
    }


    /// Periodic management for this process
    ///
    /// For now, the only management is killing self if too slow
    fn local_manage(&mut self) -> Result<(), PeerError> {

        // only do periodically
        const LOCAL_MANAGE_DELAY: Duration = Duration::from_millis(20 *1000);
        if Instant::now() - self.last_local_manage < LOCAL_MANAGE_DELAY {
            return Ok(());
        }
        self.last_local_manage = Instant::now();

        if self.synced {
            return Ok(());
        }
        let median_speed = sync::PeerInfo::get_median_read_speed()?;
        let speed = self.peerinfo.bytes_read.average_s;
        if  speed < median_speed / 2 {
            //Err(PeerError::Stalled { speed, median_speed })
            Ok(())
        }
        else {
            //sync::AddrInfo::set_peer_result(self.peerinfo.address, Some());
            Ok(())
        }
    }

}
