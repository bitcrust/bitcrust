use std::path::PathBuf;

use clap::{App, Arg, ArgMatches};

use dirs;



pub struct Arguments {
    pub data_dir: PathBuf,
    pub fixed_addr: Option<String>,
    pub remove: bool,
    pub stderr: bool,
    pub stdio: bool,
    pub loglevel: String,
}

impl Arguments {
    pub fn from_args() -> Arguments {
        let matches = matches();
        let data_dir: PathBuf = matches.value_of("data-dir").map(|p| PathBuf::from(&p)).unwrap_or_else(|| {
            let mut path = dirs::home_dir().expect("Can't figure out where your $HOME is");
            path.push("bitcrust-data");
            path
        });
        let loglevel = matches.value_of("loglevel").unwrap_or("info").to_owned();
        let fixed_addr = matches.value_of("address").map(|s| s.to_owned());
        let remove = matches.is_present("remove");
        let stderr = matches.is_present("stderr");
        let stdio = matches.is_present("stdio");
        Arguments {
            data_dir, fixed_addr, remove, stderr, stdio, loglevel
        }
    }
}


fn matches<'a>() -> ArgMatches<'a> {
    App::new("bitcrust-peer")
        .version(crate_version!())
        .author("Chris M., Tomas W.")
        .arg(Arg::with_name("data-dir")
            .short("b")
            .long("data-dir")
            .takes_value(true)
            .help("Location of the Bitcrust data, default: $HOME/bitcrust-data"))
        .arg(Arg::with_name("remove")
            .short("r")
            .long("remove")
            .takes_value(false)
            .help("Removes all data before starting"))
        .arg(Arg::with_name("bg-connect")
                .long("bg-connect")
                .takes_value(false)
                .help(
"Runs a background connect procedure. This shouldn't be used manually.
Peers will automatially spawn an instance with --bg-connect when needed"))
        .arg(Arg::with_name("loglevel")
            .short("v")
            .long("verbose")
            .takes_value(true)
            .help(
"Sets the logging level.
You can pass either trace,debug,info,warn,error.
You can also pass comma separated clauses to specify per module logging
For example: --verbose info,spendindex=trace sets the logging to info,
but the logging on the spendindex to trace.

Modules include spendindex,headerstate,netbytes,netmsg,difficulty"))
        .arg(Arg::with_name("stderr")
            .long("stderr")
            .takes_value(false)
            .help("Logs to stderr instead of logfile"))
        .arg(Arg::with_name("address")
            .short("a")
            .long("address")
            .takes_value(true)
            .help("The network address and port to connect to. Set to 'auto' to find a suitable address"))
        .get_matches()
}

