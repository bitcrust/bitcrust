use mio;
use network_encoding::*;
use std::io;
use std::io::Read;
use std::io::Write;
use std::net;
use std::net::TcpStream;
use std::os::unix::io::AsRawFd;
use std::time::Duration;

pub struct Connection {
    socket: TcpStream,

    //watch_descriptor: Option<inotify::WatchDescriptor>,
    //pub watch_task_name: Option<String>,

    /// A INotify handle that matches the tasks that postponed is waiting for
    //inotify_handle: inotify::Inotify,

    /// This will register the events that need polling
    poller: mio::Poll,

    events: mio::Events,

    // Temporary counters. These will be periodically added to peer_info
    bytes_written: u64,
    bytes_read: u64,
}

impl Connection {
    pub fn new(host: net::SocketAddrV6) -> Result<Connection, io::Error> {
        let socket = TcpStream::connect_timeout(&host.into(), Duration::from_millis(4000))?;

        socket.set_read_timeout(Some(Duration::from_secs(2)))?;
        socket.set_write_timeout(Some(Duration::from_secs(2)))?;

        //let inotify = inotify::Inotify::init()?;

        let poller = mio::Poll::new()?;
        // Listen to socket
        poller.register(
            &mio::unix::EventedFd(&socket.as_raw_fd()),
            mio::Token(0),
            mio::Ready::readable(),
            mio::PollOpt::level(),
        )?;
        // Listen to pending tasks that we are awaiting
        /*poller.register(
            &mio::unix::EventedFd(&inotify.as_raw_fd()),
            mio::Token(1),
            mio::Ready::readable(),
            mio::PollOpt::level(),
        )?;
*/
        Ok(Connection {
            socket: socket,
//            watch_descriptor: None,
//            watch_task_name: None,

//            inotify_handle: inotify,
            poller: poller,
            events: mio::Events::with_capacity(1000),

            bytes_read: 0,
            bytes_written: 0,
        })
    }
/*
    fn set_watch(&mut self, new_watch: Option<String>) -> Result<bool, io::Error> {
        if self.watch_task_name == new_watch {
            return Ok(false);
        } else {
            if let Some(wd) = self.watch_descriptor.take() {
                self.inotify_handle
                    .rm_watch(wd)
                    .expect("Couldn't remove watch");
                self.watch_task_name = None;
            }
            if let Some(task) = new_watch {
                debug!("Setting watch to {}", task);
                self.watch_descriptor = match self
                    .inotify_handle
                    .add_watch(&task, inotify::WatchMask::DELETE_SELF)
                {
                    Err(ref e) if e.kind() == io::ErrorKind::NotFound => {
                        // if the file doesn't exist, we're already done watching for its deletion
                        return Ok(true);
                    }
                    x => Some(x.unwrap()),
                };
                self.watch_task_name = Some(task);
            }
            Ok(false)
        }
    }
*/
    /// Reads and resets the read/write counters
    pub fn extract_counters(&mut self) -> (u64, u64) {
        let result = (self.bytes_read, self.bytes_written);
        self.bytes_read = 0;
        self.bytes_written = 0;
        result
    }

    /// Reads and returns available incoming messages
    ///
    /// This blocks until a message is available or a timeout occurs
    ///
    /// The messages can arrive from different sources:
    ///
    /// * Send by the peer over the socket
    /// * Signalled for retry if the message was postponed (using postpone_message) and the blocking task is done
    /// * Incoming via relay
    pub fn read_message(
        &mut self,
        _await_file: Option<String>,
    ) -> Result<Vec<Message>, io::Error> {
        // Set a watch to the await file; if it is already gone, we can return true
//        if self.set_watch(await_file).unwrap() {
//            return Ok((vec![], true));
//        }

        let mut result = Vec::new();
        self.poller
            .poll(&mut self.events, Some(Duration::from_millis(50)))?;


        for ev in self.events.iter() {
            if ev.token() == mio::Token(0) {
                result.push(read_message_from_socket(
                    &mut self.socket,
                    &mut self.bytes_read,
                )?);
            }
        }
        Ok(result)
    }

    pub fn send_message(&mut self, msg: &Message) -> Result<(), io::Error> {
        trace!(target: "netmsg", "Send: {:?}", msg);

        let enc = &msg.encode(false);
        trace!(target: "netbytes", ">> {:?}", to_hex(&enc[..]));

        self.socket.write_all(enc)?;
        self.bytes_written += enc.len() as u64;

        Ok(())
    }
}

fn read_message_from_socket<R: Read>(
    socket: &mut R,
    counter: &mut u64,
) -> Result<Message, io::Error> {
    // buffered decoding in two reads
    let mut buf_hdr = [0u8; ::std::mem::size_of::<MsgHeader>()];
    socket.read_exact(&mut buf_hdr)?;
    trace!(target: "netbytes", "<< {}", to_hex(&buf_hdr[..]));

    let hdr: MsgHeader = decode_buf(&buf_hdr[..])?;
    let mut payload: Vec<u8> = Vec::new();
    payload.resize(hdr.length as usize, 0);
    socket.read_exact(&mut payload)?;
    trace!(target: "netbytes", "<< {}", to_hex(&payload[..]));

    *counter += hdr.length as u64 + ::std::mem::size_of::<MsgHeader>() as u64;

    let msg = match &hdr.command {
        b"version\0\0\0\0\0" => Message::Version(decode_buf(&payload)?),
        b"verack\0\0\0\0\0\0" => Message::Verack,
        b"sendheaders\0" => Message::SendHeaders,

        b"ping\0\0\0\0\0\0\0\0" => Message::Ping(decode_buf(&payload)?),
        b"pong\0\0\0\0\0\0\0\0" => Message::Pong(decode_buf(&payload)?),

        b"getaddr\0\0\0\0\0" => Message::GetAddr,
        b"addr\0\0\0\0\0\0\0\0" => Message::Addr(decode_buf(&payload)?),

        b"headers\0\0\0\0\0" => Message::Headers(decode_buf(&payload)?),
        b"getheaders\0\0" => Message::GetHeaders(decode_buf(&payload)?),
        b"inv\0\0\0\0\0\0\0\0\0" => Message::Inv(decode_buf(&payload)?),
        b"block\0\0\0\0\0\0\0" => Message::Block(BlockMessage::new(payload)),

        _ => {
            let cmd: Vec<u8> = hdr.command.iter().map(|x| *x).take_while(|x| *x>0).collect();
            let cmd = String::from_utf8_lossy(&cmd);
            warn!("Received unknown command '{}'", cmd);
            Message::MemPool
        }
    };
    debug!(target: "netmsg", "Receive: {:?}", &msg);

    Ok(msg)
}

fn to_hex(buf: &[u8]) -> String {
    buf.iter()
        .map(|n| format!("{:02x}", n))
        .collect()
}
