///
/// Defines the Bitcoin network messages and their serialization
///
///
///

use std::io;
use std::io::Write;
use std::net::{IpAddr,Ipv6Addr,SocketAddr};
use derive_more::{From,Into};
use serde;
use serde_bitcoin;

use bitcrust_store::Hash;


mod version_message;
pub use self::version_message::Version;

//mod addr_message;
//mod block_message;
//mod getdata_message;
//mod getblocks_message;
//mod getheaders_message;
//mod header_message;
//mod inv_message;
//mod sendcmpct_message;
//mod transaction_message;
//mod notfound_message;


//pub use self::version_message::VersionMessage;
//pub use self::addr_message::AddrMessage;
//pub use self::block_message::BlockMessage;
//pub use self::getdata_message::GetdataMessage;
//pub use self::getblocks_message::GetblocksMessage;
//pub use self::getheaders_message::GetheadersMessage;
//pub use self::header_message::HeadersMessage;

//pub use self::inv_message::InvMessage;
//pub use self::sendcmpct_message::SendCmpctMessage;
//pub use self::transaction_message::TransactionMessage;
//pub use self::notfound_message::NotfoundMessage;


const MAGIC: u32 = 0xE8F3E1E3u32;

/// Header of a protocol message
#[derive(serde::Deserialize, serde::Serialize)]
struct MsgHeader {
    magic: u32,
    name: [u8; 12],
    len: u32,
    hash: [u8; 4],
}

#[derive(Debug)]
pub enum Message {
    Version(Version),
    Verack(VerAck),

    Ping(u64),
    Pong(u64),

    Unknown(String),
}


impl Message {

    /// Serializes and sends the message to the given writer
    pub fn send<W: io::Write>(&self, wr: &mut W) -> Result<(), io::Error>
    {
        // Split self into name and message specific payload
        let (name, payload) = match *self {
            Message::Verack(_) => (VerAck::ID, vec![0u8;0]),
            Message::Version(ref ver) => (Version::ID, serde_bitcoin::to_bytes(&ver)),
            Message::Ping(nonce) => (Ping::ID, serde_bitcoin::to_bytes(&nonce)),
            Message::Pong(nonce) => (Pong::ID, serde_bitcoin::to_bytes(&nonce)),
            _ => unimplemented!(),
        };

        // Construct header
        let magic = MAGIC;
        let len   = payload.len() as u32;
        let hash  = Hash::from_data(&payload).into_inner();
        let hash  = [hash[0], hash[1], hash[2], hash[3]];
        let hdr   = MsgHeader { magic, name, len, hash };

        // Collect header+payload
        let buf: Vec<u8> = serde_bitcoin::to_bytes(&hdr)
            .into_iter().chain(payload.into_iter()).collect();

        wr.write_all(&buf)
    }

    /// Reads and deserializes one message from the reader.
    ///
    /// This will block until a message comes in
    pub fn recv<R: io::Read>(r: &mut R) -> Result<Message, serde_bitcoin::Error> {

        // Read msg header
        let mut hdr = [0u8; 4 + 12 + 4 + 4];
        r.read_exact(&mut hdr)?;
        let hdr: MsgHeader = serde_bitcoin::from_bytes(&hdr).unwrap();

        // Read payload
        let mut payload = vec![0u8; hdr.len as usize];
        r.read_exact(&mut payload)?;

        if hdr.magic != MAGIC {
            panic!("Incorrect magic number in message");
        }

        let msg = match hdr.name {
            Version::ID => Message::Version(serde_bitcoin::from_bytes(&payload)?),
            VerAck::ID => Message::Verack(serde_bitcoin::from_bytes(&payload)?),
            Ping::ID => Message::Ping(serde_bitcoin::from_bytes(&payload)?),
            Pong::ID => Message::Pong(serde_bitcoin::from_bytes(&payload)?),

            other => {
                let cmd: Vec<u8> = other.iter().map(|x| *x)
                    .take_while(|x| *x>0).collect();
                Message::Unknown(String::from_utf8_lossy(&cmd).to_string())
            }
        };
        Ok(msg)

    }
}

#[derive(Debug, serde::Serialize, serde::Deserialize)]
pub struct VerAck {}
impl VerAck {
    const ID: [u8;12] = *b"verack\0\0\0\0\0\0";
}


#[derive(Debug, PartialEq, Eq, serde::Serialize, serde::Deserialize, From, Into)]
pub struct ServiceFlags(u64);

impl ServiceFlags {
    pub fn has_network(&self) -> bool {
        const NETWORK: u64    = 0b00000001;
        self.0 & NETWORK == NETWORK
    }
}

pub struct Ping {}
impl Ping {
    const ID: [u8; 12] = *b"ping\0\0\0\0\0\0\0\0";
}

pub struct Pong {}
impl Pong {
    const ID: [u8; 12] = *b"pong\0\0\0\0\0\0\0\0";
}

        /*

#[cfg(test)]
mod tests {
    use std::str::FromStr;
    use std::net::Ipv6Addr;

    use crate::net_addr::NetAddr;
    use super::*;
    use crate::services::Services;

    #[test]
    fn it_encodes_a_version_message() {

        let expected = vec![
          // Message Header:
          0xE3, 0xE1, 0xF3, 0xE8,                                                                                                                                    //- Main network magic bytes
          0x76, 0x65, 0x72, 0x73, 0x69, 0x6F, 0x6E, 0x00, 0x00, 0x00, 0x00, 0x00,                                                                                    //- "version" command
          0x64, 0x00, 0x00, 0x00,                                                                                                                                    //- Payload is 100 bytes long
          0x30, 0x42, 0x7C, 0xEB,                                                                                                                                    //- payload checksum

          // Version message:
          0x62, 0xEA, 0x00, 0x00,                                                                                                                                     //- 60002 (protocol version 60002)
          0x01, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,                                                                                                             //- 1 (NODE_NETWORK services)
          0x11, 0xB2, 0xD0, 0x50, 0x00, 0x00, 0x00, 0x00,                                                                                                             //- Tue Dec 18 10:12:33 PST 2012
          0x01, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0xFF, 0xFF, 0x0A, 0x00, 0x00, 0x01, 0x20, 0x8D, //- Recipient address info - see Network Address
          0x01, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0xFF, 0xFF, 0x0A, 0x00, 0x00, 0x01, 0x20, 0x8D, //- Sender address info - see Network Address
          0x3B, 0x2E, 0xB3, 0x5D, 0x8C, 0xE6, 0x17, 0x65,                                                                                                             //- Nonce
          0x0F, 0x2F, 0x53, 0x61, 0x74, 0x6F, 0x73, 0x68, 0x69, 0x3A, 0x30, 0x2E, 0x37, 0x2E, 0x32, 0x2F,                                                             //- "/Satoshi:0.7.2/" sub-version string (string is 15 bytes long)
          0xC0, 0x3E, 0x03, 0x00                                                                                                                                      //- Last block sending node has is block #212672
        ];
        let version = VersionMessage {
            version: 60002,
            services: Services::from(1),
            timestamp: 1355854353,
            addr_recv: NetAddr::new(Services::from(1), Ipv6Addr::from_str("::ffff:10.0.0.1").unwrap(), 8333),
            addr_send: NetAddr::new(Services::from(1), Ipv6Addr::from_str("::ffff:10.0.0.1").unwrap(), 8333),
            nonce: 7284544412836900411,
            user_agent: "/Satoshi:0.7.2/".into(),
            start_height: 212672,
            relay: false,
        };
        let actual = Message::Version(version).encode(false);
        assert_eq!(expected, actual);
    }
}

pub enum Message {
    Version(VersionMessage),
    Verack,
    Ping(u64),
    Pong(u64),

    SendHeaders,
    Block(BlockMessage),
    SendCompact(SendCmpctMessage),
    GetAddr,
    MemPool,
    GetData(GetdataMessage),
    GetBlocks(GetblocksMessage),
    GetHeaders(GetheadersMessage),
    Addr(AddrMessage),
    Headers(HeadersMessage),
    Inv(InvMessage),
    Unparsed(String, Vec<u8>),
    FeeFilter(u64),
    Tx(TransactionMessage),
    NotFound(NotfoundMessage),
}

#[derive(Serialize, Deserialize)]
pub struct MsgHeader {
    pub magic: u32,
    pub command: [u8; 12],
    pub length: u32,
    pub checksum: u32
}

impl fmt::Debug for Message {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match self {
            Message::Version(x)     => write!(f, "version[{:?}]", x),
            Message::Verack         => write!(f, "verack[]"),
            Message::GetHeaders(x)  => write!(f, "getheaders[{:?}]", x),
            Message::Headers(x)     => write!(f, "headers[{:?}]", x),
            Message::Ping(x)        => write!(f, "ping[{}]", x),
            Message::Pong(x)        => write!(f, "pong[{}]", x),
            Message::SendHeaders    => write!(f, "sendheaders[]"),
            Message::MemPool        => write!(f, "mempool[]"),
            Message::SendCompact(_) => write!(f, "sendcompact[...]"),
            Message::Block(_)       => write!(f, "block[...]"),
            Message::Inv(_)         => write!(f, "inv[...]"),
            Message::GetData(_)     => write!(f, "getdata[...]"),
            Message::GetBlocks(_)   => write!(f, "getblocks[...]"),
            Message::Addr(_)        => write!(f, "addr[...]"),
            Message::FeeFilter(_)   => write!(f, "feefilter[...]"),
            Message::GetAddr        => write!(f, "getaddr[]"),
            _          => write!(f, "unknown[]", ),
        }
    }
}

impl Serialize

macro_rules! packet {
    ($testnet: expr, $packet_type: expr => $payload: expr) => {{
        let len = $payload.len();
        let mut packet: Vec<u8> = Vec::with_capacity(len + 24);
        if $testnet {
            let _ = 0xDAB5BFFAu32.encode(&mut packet);
        } else {
            let _ = 0xE8F3E1E3u32 /*  0xD9B4BEF9u32*/.encode(&mut packet);
        }
        // Write the message type
        if $packet_type.len() > 12 {
            let _ = $packet_type.as_bytes()[0..11].encode(&mut packet);
        } else {
            let _ = $packet_type.as_bytes().encode(&mut packet);
        }

        for _ in 1 .. (12-$packet_type.len()) {
          let _ = 0x00u8.encode(&mut packet);
        }
        let _ = 0x00u8.encode(&mut packet);
        // Write the length of the payload
        let _ = (len as u32).encode(&mut packet);

        // write the checksum
        let output = Hash::from_data(&$payload).into_inner();
        let _ = output[0..4].encode(&mut packet);
        // write the message payload
        packet.extend($payload);
        packet
    }};
    ($testnet: expr, $packet_type: expr, $packet: ident) => {{
        let mut packet_vec = Vec::with_capacity($packet.len());
        let _ = $packet.encode(&mut packet_vec);
        packet!($testnet, $packet_type => packet_vec)
    }};
    ($testnet: expr, $packet: ident) => {{
        let mut packet_vec = Vec::with_capacity($packet.len());
        let _ = $packet.encode(&mut packet_vec);
        packet!($testnet, $packet.name() => packet_vec)
    }};
    ($testnet: expr, $packet_type: expr) => {{
        packet!($testnet, $packet_type => vec![0;0])
    }}
}

impl Message {
    pub fn encode(&self, testnet: bool) -> Vec<u8> {
        match *self {
            // Empty message types
            Message::Verack => packet!(testnet, "verack"),
            Message::SendHeaders => packet!(testnet, "sendheaders"),
            Message::GetAddr => packet!(testnet, "getaddr"),

            // Simple Types
            Message::Ping(nonce) => {
                let mut v = Vec::with_capacity(8);
                let _ = nonce.encode(&mut v);
                packet!(testnet, "ping" => v)
            }
            Message::Pong(nonce) => {
                let mut v = Vec::with_capacity(8);
                let _ = nonce.encode(&mut v);
                packet!(testnet, "pong" => v)
            }
            Message::FeeFilter(filter) => {
                let mut v = Vec::with_capacity(8);
                let _ = filter.encode(&mut v);
                packet!(testnet, "feefilter" => v)
            },

            // Complex Types
            Message::Version(ref message) => packet!(testnet, message),
            Message::Block(ref block) => packet!(testnet, block),
            Message::GetData(ref msg) => packet!(testnet, msg),
            Message::GetBlocks(ref msg) => packet!(testnet, msg),
            Message::GetHeaders(ref msg) => packet!(testnet, msg),
            Message::Addr(ref addr) => packet!(testnet, addr),
            Message::Inv(ref inv) => packet!(testnet, inv),
            Message::Headers(ref headers) => packet!(testnet, headers),
            Message::SendCompact(ref message) => packet!(testnet, message),
            //Message::Tx(ref tx) => packet!(testnet, tx),
            Message::NotFound(ref notfound) => packet!(testnet, notfound),
            // Unparsed messages
            Message::Unparsed(_, ref v) => v.clone(),
            _ => unimplemented!()
        }
    }

    pub fn is_version(&self) -> bool {
        match self {
            Message::Version(_) => true,
            _ => false
        }
    }

    pub fn is_verack(&self) -> bool {
        match self {
            Message::Verack => true,
            _ => false
        }
    }
}
*/
