use std::{io,fmt,net};
use std::time::{UNIX_EPOCH,SystemTime};

use super::{ServiceFlags};

#[derive(serde::Serialize, serde::Deserialize)]
pub struct Version {
    pub version: i32,
    pub services: ServiceFlags,
    pub timestamp: i64,
    pub unused1: u64,

    #[serde(with = "serde_bitcoin::net_addr")]
    pub addr_recv: net::SocketAddr,
    pub unused2: u64,
    pub unused3: [u8;18],
    pub nonce: u64,
    pub user_agent: String,
    pub start_height: i32,
    pub relay: bool
}

impl Version {
    pub const ID: [u8;12] = *b"version\0\0\0\0\0";

    pub fn new(addr_recv: net::SocketAddr) -> Self {

        Version {
            version: 70015,
            services: ServiceFlags::from(0b00100001u64),
            timestamp: SystemTime::now().duration_since(UNIX_EPOCH).unwrap().as_secs() as i64,
            unused1: 0,
            addr_recv: addr_recv,
            unused2: 0,
            unused3: [0; 18],
            nonce: 1,
            user_agent: "/bitcrust:0.1.0/".into(),
            start_height: 0,
            relay: false,
        }
    }
}

impl fmt::Debug for Version {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "v={}, srv={:x}, h={}, agent={}",self.version, self.services.0,self.start_height, self.user_agent)
    }
}
