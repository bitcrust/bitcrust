


///
/// Field Size	Description	Data type	Comments
/// 4	version	int32_t	Transaction data format version (note, this is signed)
/// 0 or 2	flag	optional uint8_t[2]	If present, always 0001, and indicates the presence of witness data
/// 1+	tx_in count	var_int	Number of Transaction inputs (never zero)
/// 41+	tx_in	tx_in[]	A list of 1 or more transaction inputs or sources for coins
/// 1+	tx_out count	var_int	Number of Transaction outputs
/// 9+	tx_out	tx_out[]	A list of 1 or more transaction outputs or destinations for coins
/// 0+	tx_witnesses	tx_witness[]	A list of witnesses, one for each input; omitted if flag is omitted above
/// 4	lock_time	uint32_t	The block number or timestamp at which this transaction is unlocked:
/// Value	Description
/// 0	Not locked
/// < 500000000	Block number at which this transaction is unlocked
/// >= 500000000	UNIX timestamp at which this transaction is unlocked
/// If all TxIn inputs have final (0xffffffff) sequence numbers then lock_time is irrelevant. Otherwise, the transaction may not be added to a block until after lock_time (see NLockTime).
///
#[derive(Debug, Default, PartialEq)]
pub struct TransactionMessage {
    pub version: i32,
}

