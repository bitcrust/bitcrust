
use serde::{Serialize, Deserialize};
use bitcrust_store::Hash;


#[derive(Debug, Default, PartialEq, Serialize, Deserialize)]
pub struct InventoryVector {
    flags: InvFlags,
    pub hash: Hash,
}

bitflags! {
  #[derive(Default, Serialize, Deserialize)]
  struct InvFlags: u32 {
      const ERROR               = 0b0;
      const MSG_TX              = 0b00000001;
      const MSG_BLOCK           = 0b00000010;
      const MSG_FILTERED_BLOCK  = 0b00000100;
      const MSG_CMPCT_BLOCK     = 0b00001000;
  }
}

impl InventoryVector {
    pub fn new(flags: u32, hash: Hash) -> InventoryVector {

        InventoryVector {
            flags: InvFlags { bits: flags },
            hash: hash,
        }
    }
}
