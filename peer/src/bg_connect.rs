//!
//! Background connector
//!
//! Background connection is triggered whenever a peer receives blocks out of order;
//! typically during IBD.
//!

use std::{io,process,time};

use bitcrust_store as store;
use store::ResultExt;
use crate::config;


const CONNECT_BUSY_TIMEOUT: time::Duration = time::Duration::from_secs(15);
const SLEEP_ON_IDLE: time::Duration = time::Duration::from_secs(1);

/// Spawns a background-connect process if it's not already running
pub fn spawn() -> Result<(), io::Error> {
    if sync::is_pid_file_active("bg-connect")? {
        return Ok(());
    }
    info!("Spawning bg-connect");
    process::Command::new(std::env::current_exe()?)
        .arg("--bg-connect")
        .arg("--data-dir")
        .arg(".")
        .spawn()?;
    Ok(())
}



pub fn run(config: config::Arguments) {
    info!(target: "bg-connect", "Starting bg-connect");
    sync::init(&config.data_dir).unwrap();

    // Run once
    if !sync::set_pid_file("bg-connect").unwrap() {
        return;
    }

    let _ = super::init_log(&config);
    let db = store::init(&config.data_dir).unwrap();

    // list used to mark we're busy with a data object
    let busy = sync::BusyList::open().unwrap();

    loop {
        let headers: Vec<&store::Header> = store::Header::iter_needs_connection(&db).take(50).collect();
        if headers.is_empty() {
            info!(target: "bg-connect", "All done");
            break;
        }

        // find a suitable connect candidate
        let target_header = headers.into_iter()
            .find(|hdr| hdr.has_transactions() && hdr.parent(&db).unwrap().is_connected());


        if let Some(header) = target_header {
            if busy.claim(&header.hash.into_inner(), CONNECT_BUSY_TIMEOUT)
            {
                info!(target: "bg-connect", "Start connecting {} at {}", header.hash, header.height);

                // Run connect; we're not interested in the result as we are not the one
                // that submitted the block
                let _ = header.connect(&db).unwrap_dberr();
                busy.unclaim(&header.hash.into_inner());
                continue;
            }
            else {
                warn!(target: "bg-connect", "Awaiting claim");
            }
        }
        info!(target: "bg-connect", "Sleeping");
        std::thread::sleep(SLEEP_ON_IDLE);

    }
}
