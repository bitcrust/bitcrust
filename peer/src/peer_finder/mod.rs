use std::net::{SocketAddr, SocketAddrV6,ToSocketAddrs};
use std::io;
use std::collections::HashSet;
use crate::config::Arguments;
use crate::sync::{AddrInfo,PeerInfo};

const SEEDERS: [&'static str; 4] = [
    "seed.bitcoinabc.org:8333",
//    "seed-abc.bitcoinforks.org:8333",
    "btccash-seeder.bitcoinunlimited.info:8333",
    "seed.bitprim.org:8333",
    "seed.deadalnix.me:8333",
//    "seeder.criptolayer.net:8333"
];



/// Load from hardcoded DNS if no addresses available
fn load_from_dns() -> Result<(), io::Error> {
    info!(target: "findpeer", "Initializing addresses from DNS");
    for seeder in SEEDERS.iter() {
        debug!(target: "findpeer", "Trying seed: {}", seeder);
        match seeder.to_socket_addrs() {
            Ok(addrs) => {
                for addr in addrs {
                    let _ = AddrInfo::new(&addr)?;
                }
            },
            Err(e) => {
                warn!(target: "findpeer", "Seed {} error {:?}", seeder, e);
                continue;
            }
        }
    }
    Ok(())
}

/// Finds a suitable peer to connect to
pub fn find_peer(config: &Arguments) -> Result<Option<SocketAddr>, io::Error> {

    // use command line argument if available
    if let Some(addr) = config.fixed_addr.clone() {

        return Ok(addr.to_socket_addrs().ok()
            .and_then(|mut addrs| addrs
                .next()
            ));
    }

    if AddrInfo::count().expect("addrinfo::count() failed") == 0 {
        load_from_dns().expect("load_from_dns failed");
    }

    // Get current connected peers to prevent double connecting and same subnet connections
    let peers: HashSet<SocketAddr> = PeerInfo::get_all().expect("peerinfo::get_all failed")
        .iter()
        .map(PeerInfo::get_address)
        .collect();


    // Function to determine inferiority of choice for certain addresses
    // This is rather bare at the moment
    const MAX_INFERIORITY: u32 = 6;
    let inferiority = |addr: &AddrInfo| -> u32 {

        // Never reuse connected peer
        if peers.contains(&addr.address) { return MAX_INFERIORITY + 1; }

        let mut inferiority = 0;

        // Punish same subnet
        if peers.iter().any(|peer| addr.is_same_subnet(peer)) { inferiority += 2};

        // Lightly punish already tried
        if addr.peer_info.is_some() { inferiority += 1 };

        // Punish errors
        if addr.peer_error.is_some() { inferiority += 3 };

        return inferiority;
    };

    // Get all addresses in random order, and then *stable* sort by inferiority
    let mut addrs = AddrInfo::get_all().expect("addrinfo::get_all failed");
    addrs.sort_by(|addr1, addr2| {
        inferiority(addr1).cmp(&inferiority(addr2))
    });

    debug!("Comparing {} available addresses", addrs.len());
    if addrs.len() > 0 {
        debug!("Minimum inferiority is {}", inferiority(&addrs[0]));
    }

    Ok(addrs
        .get(0)
        .and_then(|addr| if inferiority(addr) > MAX_INFERIORITY { None } else { Some(addr) })
        .map(|addr| addr.address)
    )
}
