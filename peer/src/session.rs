
use std::fmt::{self, Debug};
use std::sync::Arc;
use std::sync::mpsc;
use std::io;
use std::net;
use std::time;
use std::net::{Ipv6Addr};
use std::str::FromStr;
use std::time::{UNIX_EPOCH, SystemTime};
use std::thread;

use crossbeam::channel;

use crate::sync;
use bitcrust_store as store;
use self::store::ResultExt;

use crate::message::{self,Message};
use derive_more::From;


const HANDSHAKE_TIMEOUT: time::Duration = time::Duration::from_secs(1);

#[derive(Debug, From)]
pub enum PeerError {
    ExpectedVersion,
    ExpectedVerack,
    NoNetworkService,
    UnexpectedVersion,
    UnexpectedVerack,

    OutOfOrderHeader,

    SerdeError(serde_bitcoin::Error),
    IoError(io::Error),
    StoreError(store::StoreError),
    VerifyError(store::VerifyError),

    Stalled { speed: u64, median_speed: u64},
}

#[derive(Debug)]
pub enum ChannelMsg {
    Incoming(Result<Message, serde_bitcoin::Error>, u64),
}

pub fn run(mut reader: std::fs::File,
           writer: &mut impl io::Write,
           addr: net::SocketAddr)
    -> Result<(), PeerError>
{

    // Spawn reader thread
    let (sender, receiver) = mpsc::sync_channel(10);
    thread::spawn(move || { reader_thread(reader, sender) });

    let mut peer_info = handshake(&receiver, writer, addr)?;

    // Main processing loop
    loop {
        match receiver.recv().unwrap() {

            ChannelMsg::Incoming(msg, bytes_read) => {
                info!("recv: {:?}", msg);

                handle_message(&mut peer_info, writer, msg?)?;
            },

            _ => todo!(),
        }

    }
}


fn handle_message(peer_info: &mut sync::PeerInfo, writer: &mut impl io::Write, message: Message) -> Result<(), PeerError> {
    match message {

        Message::Version(_) => {
            // handshake already done here
            return Err(PeerError::UnexpectedVersion);
        }
        Message::Verack(_) => {
            // handshake already done here
            return Err(PeerError::UnexpectedVerack);
        },

        Message::Ping(nonce) => {
            Message::Pong(nonce).send(writer)?;
        },
        Message::Pong(_nonce) => {
            // Thanks. Ignore
        },
        /*
        Message::SendHeaders => {
            self.send_headers = true;
        }

        Message::Addr(_addrs) => {
            /*let _ = self.sender.try_send(ClientMessage::Addrs(addrs.addrs.clone()));
            // Ensure that we don't realocate repeatedly in here
            self.addrs.reserve(addrs.addrs.len());
            for addr in addrs.addrs {
                self.addrs.insert(addr);
            }
            */
        }
        Message::GetAddr => {
            //let msg = AddrMessage { addrs: self.addrs.iter().cloned().collect() };
            //let _ = self.connection.send_message(&Message::Addr(msg));
        }
        Message::GetHeaders(_msg) => {

        }

        Message::Headers(headers) => {
            self.handle_message_headers(headers)?;

        }
        Message::Inv(_inv) => {}
        Message::Tx(_transaction) => {},

        Message::GetData(_data) => {}

        Message::Block(block) => {

            let db = self.db.clone();
            let sender = self.sender.clone();
            rayon::spawn(move || {
                let result = Self::handle_message_block(&db, block);
                sender.try_send(LocalMessage::BlockFetched(result)).unwrap();
            });
        },
*/

        Message::Unknown(s) => {
            debug!("Ignoring unsupported message: {:?}", s);
        },
    };
    Ok(())
}


pub fn reader_thread(mut reader: std::fs::File, sender: mpsc::SyncSender<ChannelMsg>) {
    loop {
        let msg = Message::recv(&mut reader);
        if sender.send(ChannelMsg::Incoming(msg,0)).is_err() {
            break;
        }
    }

}

pub fn handshake(receiver: &mpsc::Receiver<ChannelMsg>, writer: &mut impl io::Write,
           addr: net::SocketAddr) -> Result<sync::PeerInfo, PeerError>
{
    // send/recv version
    let my_version = Message::Version(message::Version::new(addr));
    my_version.send(writer)?;

    let msg = receiver.recv_timeout(HANDSHAKE_TIMEOUT).unwrap();
    let ver = if let ChannelMsg::Incoming(Ok(Message::Version(your_version)),_) = msg {

        if !your_version.services.has_network() {
            return Err(PeerError::NoNetworkService);
        }
        your_version
    }
    else {
        return Err(PeerError::ExpectedVersion);
    };

    // send/recv verack
    Message::Verack(message::VerAck{}).send(writer)?;
    let msg = receiver.recv_timeout(HANDSHAKE_TIMEOUT).unwrap();
    if let ChannelMsg::Incoming(Ok(Message::Verack(_)),_) = msg {
        info!("Handshake completed");
    }
    else {
        return Err(PeerError::ExpectedVerack);
    }

    let mut peer_info = sync::PeerInfo::new(addr);
    peer_info.user_agent = ver.user_agent;
    peer_info.services = ver.services.into();


    Ok(peer_info)
}

