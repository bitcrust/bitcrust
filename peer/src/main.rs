//! bitcrust-peer
//!
//! This is the front-end application that connects to a single peer,
//! syncs the blockchain and then starts relaying transactions and blocks
//!
//! Multiple instance can be run and will work together if they share the same --data-dir


#[macro_use]
extern crate clap;
#[macro_use]
extern crate log;
//extern crate bitcrust_net;
extern crate flexi_logger;

extern crate bitcrust_store;
extern crate bitcrust_sync as sync;
extern crate rand;
extern crate rayon;

extern crate libc;
extern crate mio;
extern crate inotify;

extern crate network_encoding;
extern crate dirs;

use std::os::unix::io::FromRawFd;
mod message;
mod session;

use bitcrust_store as store;

mod config;
//mod peer;
mod peer_finder;

use crate::config::Arguments;
use std::{path,thread,net,fs,time,env};


/// Main peer entry point
fn main() {

    let config = Arguments::from_args();

    let _check_log_func = init_log(&config);

    // clear database?
    if config.remove {
        let _db = store::init_empty(&config.data_dir)
            .expect("Store initialization failed");
    }

    sync::init(&config.data_dir)
        .expect("Could not initialize sync directories");

    info!("Started in single process mode at {}", config.data_dir.to_string_lossy());

    if true {
        let mut stdin = unsafe { fs::File::from_raw_fd(0) };
        let mut stdout = unsafe { fs::File::from_raw_fd(1) };
        let addr: String  = env::var("NCAT_REMOTE_ADDR").unwrap_or(String::new());
        let port: String =  env::var("NCAT_REMOTE_PORT").unwrap_or(String::new());
        info!("{}:{}", addr,port);

        let saddr: net::SocketAddr = format!("[{}]:{}", addr,port).parse().unwrap();

        session::run(stdin, &mut stdout, saddr).unwrap();
    }

    /* Loop over full connect-handle-disconnect cycle */
    loop {
        let db = store::init(&config.data_dir)
            .expect("Store initialization failed");

        let address = match peer_finder::find_peer(&config).expect("Find Peer failed") {
            Some(a) => a,
            None => continue,
        };

        // connect and run
        info!(target:"netmsg", "Connecting to {}", address.to_string());

        /*
        let _ = match peer::Peer::new(address, db) {
            Err(e) => sync::AddrInfo::set_peer_result(&address, None, Some(e.to_string())),
            Ok(mut peer) => {

                match peer.run(true) {
                    Err(e) => sync::AddrInfo::set_peer_result(&address, Some(peer.peerinfo), Some(e.to_string())),

                    Ok(_) => sync::AddrInfo::set_peer_result(&address, Some(peer.peerinfo), None),
                }
            }
        };
*/
        // Wait a bit to prevent constantly trying if no network is available
        thread::sleep(time::Duration::from_millis(2000));
    }
}


fn init_log(config: &Arguments) -> impl FnMut() -> () {

    // initialize logger from --verbose argument
    let logger = flexi_logger::Logger::with_env_or_str(&config.loglevel)
        .format(flexi_logger::opt_format);

    // Log to {data_dir}/log unless --stderr was given
    let logger = if !config.stderr {
        let log_path: path::PathBuf = [&config.data_dir, path::Path::new("log")].iter().collect();
        logger
            .log_to_file()
            .suppress_timestamp()
            .suffix(format!("{}.log",::std::process::id()))
            .directory(log_path)
    } else {
        logger
    };

    let mut x = logger.start()
        .expect("Failed to initialize logger");

    let f = move || {
        x.parse_new_spec("trace");
    } ;
    f

}
