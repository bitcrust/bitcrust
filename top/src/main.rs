mod event;
mod stats;
mod peers;

use std::path;
use std::io;
use std::io::stdout;

use termion::event::Key;
use termion::input::MouseTerminal;
use termion::raw::IntoRawMode;
use termion::screen::AlternateScreen;
use tui::backend::TermionBackend;
use tui::layout::{Constraint, Direction, Layout};
use tui::style::{Color, Style};
use tui::widgets::{Block, Borders, Tabs, Widget};
use tui::Terminal;

use bitcrust_store as store;
use bitcrust_store::Db;


use clap::{crate_version,Arg,ArgMatches,App};
use event::{Event,Events};


pub fn matches<'a>() -> ArgMatches<'a> {
    App::new("bitcrust-top")
        .version(crate_version!())
        .author("Tomas van der Wansem")
        .arg(Arg::with_name("data-dir")
            .short("d")
            .long("data-dir")
            .takes_value(true)
            .help("Location of the Bitcrust data files. Default: $HOME/bitcrust/"))
            .get_matches()


}

fn print_status(s:&str, y: u16) {
    let s_fmt = s
        .replace("[", &format!("{}[", termion::color::Fg(termion::color::Yellow)))
        .replace("]", &format!("]{}", termion::color::Fg(termion::color::White)));

    print!("{}{}", termion::cursor::Goto(2, y), s_fmt);
}

fn main() -> Result<(), io::Error> {
    let matches = matches();
    let data_dir: path::PathBuf = matches.value_of("data-dir").map(|p| path::PathBuf::from(&p)).unwrap_or_else(|| {
        let mut path = dirs::home_dir().expect("Can't figure out where your $HOME is");
        path.push("bitcrust-data");
        path
    });
    let db = store::init(&data_dir)
        .expect(&format!("Couldn't initialize store at {}", &data_dir.to_string_lossy()));


    let events = Events::new();
    print!("{}", termion::clear::All);

    let stdout = io::stdout().into_raw_mode()?;
    let stdout = MouseTerminal::from(stdout);
    //hides panic let stdout = AlternateScreen::from(stdout);

    let backend = TermionBackend::new(stdout);
    let mut terminal = Terminal::new(backend)?;
    terminal.hide_cursor()?;

    // State
    let mut tab_index = 0;
    let mut state_stats = stats::State::new(&db, data_dir.clone());
    let mut state_peers = peers::State::new(&db, data_dir.clone());



    // Main loop
    loop {
      terminal.draw(|mut f| {
          let size = f.size();
          let chunks = Layout::default()
              .direction(Direction::Vertical)
              .margin(0)
              .constraints([Constraint::Length(3), Constraint::Min(0)].as_ref())
              .split(size);

          Tabs::default()
              .block(Block::default().borders(Borders::ALL))
              .titles(&["[s]tats", "[p]eers", "[a]ddrs", "[c]hain"])
              .select(tab_index)
              .style(Style::default().fg(Color::Cyan))
              .highlight_style(Style::default().fg(Color::Yellow))
              .render(&mut f, chunks[0]);


          match tab_index {
              0 => state_stats.draw(&mut f, chunks[1]),

              1 => state_peers.draw(&mut f, chunks[1]),

              2 => Block::default()
                  .title("Inner 2")
                  .borders(Borders::ALL)
                  .render(&mut f, chunks[1]),
              3 => Block::default()
                  .title("Inner 3")
                  .borders(Borders::ALL)
                  .render(&mut f, chunks[1]),
              _ => {}
          }


      })?;


        match events.next().unwrap() {
            Event::Input(input) => match input {
                Key::Char('q') => {
                    break;
                }
                Key::Char('s') => tab_index = 0,
                Key::Char('p') => tab_index = 1,
                Key::Char('a') => tab_index = 2,
                Key::Char('c') => tab_index = 3,
                c => match tab_index {
                    0 => state_stats.send_key(c),
                    1 => state_peers.send_key(c),
                    _ => {},
                }
            },
            Event::Tick => {
                match tab_index {
                    0 => state_stats.update(&db, data_dir.clone()),
                    _ => state_peers.update(),
                }
            }
        }

  }
  Ok(())
}
