//!
//! Peer stats

use separator::Separatable;

use tui::backend::Backend;
use tui::Frame;
use tui::layout::{Constraint,Rect,Layout,Direction};
use tui::widgets::*;
use tui::style::*;
use bitcrust_store::{Db};
use bitcrust_sync as sync;
use std::time;
use termion::event::Key;
use std::path;


pub struct State {
    pub line: usize,
    pub grep: String,
    pub peers: Vec<sync::PeerInfo>,
}

impl State {
    pub fn new(db: &Db, path: path::PathBuf) -> Self {
        sync::init(path).unwrap();
        let mut state = State {
            line: 0,
            grep: String::new(),
            peers: vec![]
        };
        state.update();
        state

    }
    pub fn update(&mut self) {
        self.peers = sync::PeerInfo::get_all().unwrap();
        self.peers.sort_unstable_by_key(|p| p.pid);
    }

    pub fn send_key(&mut self, c: Key) {
        match c {
            Key::Char('j') => if self.line < self.peers.len() { self.line += 1 },
            Key::Char('k') => if self.line > 0 { self.line -= 1 },
            _ => { },
        }
    }

    pub fn draw<B>(&self, f: &mut Frame<B>, area: Rect) where B: Backend
    {
        // layout of columns
        let cols = Layout::default()
            .direction(Direction::Horizontal)
            .margin(0)
            .constraints([
                Constraint::Percentage(20),
                Constraint::Percentage(10),
                Constraint::Percentage(20),
                Constraint::Percentage(30),
                Constraint::Percentage(10)].as_ref())
            .split(area);

        // created columnized peers
        let items: Vec<_> = self.peers
            .iter()
            .map(|p| format!("{0:1$.1$} {2:3$.3$} {4:5$.5$} {6:7$.7$} {8:9$.9$}",
                p.get_address().to_string(), cols[0].width as usize,
                p.state().unwrap().to_string(), cols[1].width as usize,
                p.user_agent, cols[2].width as usize,
                p.task_current.clone().unwrap_or("None".to_owned()), cols[3].width as usize,
                p.bytes_read.average_s.to_string(), cols[4].width as usize,
            ))
            .collect();

        let area1 = Rect::new(area.x,area.y,area.width, (area.height-1)/3);
        let area2 = Rect::new(area.x,area.y+area1.height, area.width, area.height-1-area1.height);

        // top area is peer list
        SelectableList::default()
            .block(Block::default().borders(Borders::ALL).title("peers"))
            .items(&items)
            .select(Some(self.line))
            .highlight_style(Style::default().fg(Color::Yellow).modifier(Modifier::BOLD))
            .highlight_symbol(">")
            .render(f, area1);
        Block::default().borders(Borders::ALL).render(f,area2);
        super::print_status("[j]/[k] up/down", area.y + area.height)
    }
}
