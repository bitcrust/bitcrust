//!
//! Stats retrieval and rendering
use separator::Separatable;

use tui::backend::TermionBackend;
use tui::backend::Backend;
use tui::Frame;
use tui::layout::{Layout,Constraint,Rect};
use tui::widgets::*;
use tui::style::*;
use bitcrust_store as store;
use bitcrust_store::Db;
use std::time;
use termion::event::Key;
use std::path;
use std::collections::HashMap;

pub struct Stats {
    pub tick: time::Instant,
    pub db_stats: HashMap<String, u64>,
    pub usage: u64,
}

impl Stats {
    pub fn new(db: &Db, path: path::PathBuf) -> Self {

        let usage = std::process::Command::new("du")
            .arg("-s")
            .arg("-b")
            .arg(path.as_os_str())
            .output()
            .unwrap()
            .stdout;
        let usage =  String::from_utf8(usage).unwrap();
        let usage = usage.split_whitespace().next().unwrap();

        let usage2: u64 = usage.parse().expect(&format!("Wrong output of du: '{}'",usage));

        Stats {
            tick: time::Instant::now(),
            db_stats: store::stats_get(db).unwrap(),
            usage: usage2,
        }
    }
}


pub struct State {
    pub prev_stats: Stats,
    pub stats: Stats,
    pub line: usize,
}

impl State {
    pub fn new(db: &Db, path: path::PathBuf) -> Self {
        let prev_stats = Stats::new(db, path.clone());
        std::thread::sleep(std::time::Duration::from_millis(2));
        let stats = Stats::new(db, path.clone());
        State {
            prev_stats, stats,
            line: 0,
        }
    }

    pub fn update(&mut self, db: &Db, path: path::PathBuf) {
        std::mem::swap(&mut self.prev_stats, &mut self.stats);
        self.stats = Stats::new(db, path);
    }

    pub fn send_key(&mut self, c: Key) {
        match c {
            Key::Char('j') => if self.line < 5 { self.line += 1 },
            Key::Char('k') => if self.line > 0 { self.line -= 1 },
            _ => { },
        }


    }

    pub fn get_lines(&self) -> Vec<(String,String)> {

        let delta = ((self.stats.tick - self.prev_stats.tick).as_millis() ) as u64;
        let fmt = |name: &str, val: u64, prev:u64, unit:&str| {
            format!("{:15}{:>15}{:>15} {}", name, val.separated_string(),
                if delta == 0 { String::new() } else { ((val-prev) * 1000 / delta).separated_string()}
                , unit
            )
        };

        let mut v = vec![];
        v.push((
            fmt("tx written", self.stats.db_stats["blocks.transactions"], self.prev_stats.db_stats["blocks.transactions"], "tx/s"),
            "The total number of transactions written. This may include orphaned, transactions on side chains and even ooccasional doubles".to_owned(),
        ));
        v.push((
            fmt("tx connected", self.stats.db_stats["tx_out.transactions"], self.prev_stats.db_stats["tx_out.transactions"], "tx/s"),
            "The total number of transactions written and connected to the chain. This includes transactions on side chains".to_owned(),
        ));
        v.push((String::new(),String::new()));
        v.push((
            fmt("headers written", self.stats.db_stats["headers.count"], self.prev_stats.db_stats["headers.count"], "blk/s"),
            "The total number of headers. This may include orphaned, transactions on side chains and even ooccasional doubles".to_owned(),
        ));
        v.push((String::new(),String::new()));
        v.push((
            fmt("bytes written", self.stats.usage, self.prev_stats.usage, "b/s"),
            "The total number bytes in use by bitcrust. This includes logs, addresses and relay information".to_owned(),
        ));


        v
    }



    pub fn draw<B>(&self, f: &mut Frame<B>, area: Rect)
        where B: Backend,
    {

        let area1 = Rect::new(area.x,area.y,area.width, (area.height-2)*3/4);
        let area2 = Rect::new(area.x,area.y+area1.height, area.width, area.height-2-area1.height);

        let lines = self.get_lines();
        let items: Vec<_> = lines.iter().map(|x| x.0.clone()).collect();

        SelectableList::default()
                        .block(Block::default().borders(Borders::ALL))
                        .items(&items)
                        .select(Some(self.line))
                        .highlight_style(Style::default().fg(Color::Yellow).modifier(Modifier::BOLD))
                        .highlight_symbol(">")
                        .render(f, area1);

        let txt = [Text::raw(lines[self.line].1.clone())];
        Paragraph::new(txt.iter())
            .block(Block::default().borders(Borders::ALL))
            .render(f, area2);


        super::print_status("[j]/[k] up/down", area.y + area.height)
    }
}
