use std::fmt::{self, Debug};
use std::io;

use network_encoding::*;

bitflags! {
#[derive(Default)]
  struct ServiceFlags: u64 {
      const NETWORK      = 0b00000001;
      const UTXO         = 0b00000010;
      const BLOOM        = 0b00000100;
      const BITCOIN_CASH = 0b00100000;
  }
}

/// The following services are currently assigned:
///
/// Value Name  Description
/// 1 NODE_NETWORK  This node can be asked for full blocks instead of just headers.
/// 2 NODE_GETUTXO  See [BIP 0064](https://github.com/bitcoin/bips/blob/master/bip-0064.mediawiki)
/// 4 NODE_BLOOM  See [BIP 0111](https://github.com/bitcoin/bips/blob/master/bip-0111.mediawiki)
#[derive(Clone, Copy, Default, PartialEq, Eq, Hash)]
pub struct Services {
    flags: ServiceFlags,
}

impl Debug for Services {
    fn fmt(&self, f: &mut fmt::Formatter) -> Result<(), fmt::Error> {

        write!(f,
               r"Services {{
    network: {},
    utxo: {},
    bloom: {},
    bitcoin_cash: {}}}",
               self.network(),
               self.utxo(),
               self.bloom(),
               self.bitcoin_cash())
    }
}

impl Services {
    pub fn as_i64(&self) -> i64 {
        self.flags.bits as i64
    }

    pub fn as_u64(&self) -> u64 {
        self.flags.bits as u64
    }

    pub fn from(input: u64) -> Services {
        Services { flags: ServiceFlags::from_bits_truncate(input) }
    }

    pub fn network(&self) -> bool {
        self.flags.contains(ServiceFlags::NETWORK)
    }

    pub fn utxo(&self) -> bool {
        self.flags.contains(ServiceFlags::UTXO)
    }

    pub fn bloom(&self) -> bool {
        self.flags.contains(ServiceFlags::BLOOM)
    }

    pub fn bitcoin_cash(&self) -> bool {
        self.flags.contains(ServiceFlags::BITCOIN_CASH)
    }
}
impl Decode for Services {
    fn decode<R: io::Read>(r: &mut R) -> Result<Self, io::Error> {
        Ok(Services::from(u64::decode(r)?))
    }
}

impl Encode for Services {
    fn encode<W: io::Write>(&self, w: &mut W) -> Result<(), io::Error> {
        self.flags.bits.encode(w)
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn it_identifies_a_network_node() {
        let svc = Services::from(0b00000001u64);
        println!("{:?}", svc);
        assert!(svc.network());
    }

    #[test]
    fn it_identifies_a_utxo_node() {
        let svc = Services::from(0b00000010u64);
        println!("{:?}", svc);
        assert!(svc.utxo());
    }

    #[test]
    fn it_identifies_a_bloom_node() {
        let svc = Services::from(0b00000100u64);
        println!("{:?}", svc);
        assert!(svc.bloom());
    }

    #[test]
    fn it_identifies_a_bloom_network_node() {
        let svc = Services::from(0b00000101u64);
        println!("{:?}", svc);
        assert!(svc.network());
        assert!(svc.bloom());
    }
}
