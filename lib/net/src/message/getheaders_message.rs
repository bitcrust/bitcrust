use std::fmt;
use network_encoding::*;

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn it_implements_types_required_for_protocol() {
        let m =  GetheadersMessage::default();
        assert_eq!(m.name(), "getheaders");
        assert_eq!(m.len(), 45);
    }
}

#[derive(Default, Encode, Decode)]
pub struct GetheadersMessage {
    pub version: u32,
    pub locator_hashes: Vec<Hash>,
    pub hash_stop: Hash,
}



impl fmt::Debug for GetheadersMessage {

    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        if self.locator_hashes.len() == 0 {
            write!(f, "INVALID (no locator)")
        }
        else if self.hash_stop.is_null() {
            write!(f, "{} +{} locs",
                   self.locator_hashes[0],
                   self.locator_hashes.len()-1)
        } else {
            write!(f, "{} +{} locs (stop={})",
                   self.locator_hashes[0],
                   self.locator_hashes.len()-1,
                   self.hash_stop)
        }
    }
}


impl GetheadersMessage {
    #[inline]
    pub fn len(&self) -> usize {
       4 + 9 + ( self.locator_hashes.len() * 32 ) + 32
    }

    #[inline]
    pub fn name(&self) -> &'static str {
        "getheaders"
    }
}