use std::fmt;
use network_encoding::*;


#[derive(Decode, Encode)]
pub struct HeaderMessage {
    pub hdr: [u8;80],
    pub txcount: usize
}

#[derive(Decode, Encode)]
pub struct HeadersMessage {
    pub headers: Vec<HeaderMessage>,
}

impl fmt::Debug for HeaderMessage {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "({} {} txs)", "?", self.txcount)
    }
}

impl fmt::Debug for HeadersMessage {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        if self.headers.len() == 0 {
            write!(f, "EMPTY")
        } else if self.headers.len() == 1 {
            write!(f, "1 header {:?}", &self.headers[0])
        } else {
            write!(f, "{} headers {:?} .. {:?}", self.headers.len(),
                   &self.headers[0], &self.headers[self.headers.len()-1])
        }
    }
}


impl HeadersMessage {
    #[inline]
    pub fn len(&self) -> usize {
        8 + (81 * self.headers.len())
    }

    #[inline]
    pub fn name(&self) -> &'static str {
        "headers"
    }

}
