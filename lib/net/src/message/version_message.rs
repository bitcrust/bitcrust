use std::{io,fmt};

use network_encoding::*;
use crate::net_addr::NetAddr;
use crate::services::Services;

#[cfg(test)]
mod tests {
    use std::net::Ipv6Addr;
    use std::str::FromStr;
    use super::*;
    #[test]
    fn it_parses_a_version_message() {}

    #[test]
    fn it_encodes_a_version_message() {
        let v = VersionMessage {
            version: 60002,
            services: Services::from(1),
            timestamp: 1495102309,
            addr_recv: NetAddr {
                time: 0,
                services: Services::from(1),
                ip: Ipv6Addr::from_str("::ffff:127.0.0.1").unwrap(),
                port: 8333,
            },
            addr_send: NetAddr {
                time: 0,
                services: Services::from(1),
                ip: Ipv6Addr::from_str("::ffff:127.0.0.1").unwrap(),
                port: 8333,
            },
            nonce: 1,
            user_agent: "bitcrust".into(),
            start_height: 0,
            relay: false,
        };
        let mut encoded = vec![];
        v.encode(&mut encoded).unwrap();
        let expected: Vec<u8> =
            vec![98, 234, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 101, 115, 29, 89, 0, 0, 0, 0, 1, 0, 0, 0,
                 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 255, 255, 127, 0, 0, 1, 32, 141, 1, 0,
                 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 255, 255, 127, 0, 0, 1, 32, 141,
                 1, 0, 0, 0, 0, 0, 0, 0, 8, 98, 105, 116, 99, 114, 117, 115, 116, 0, 0, 0, 0];
        assert_eq!(expected, encoded);
    }

    #[test]
    fn it_implements_types_required_for_protocol() {
        //let m =  VersionMessage::default();
        //assert_eq!(m.name(), "version");
        //assert_eq!(m.len(), 86);
    }

}


pub struct VersionMessage {
    pub version: i32,
    pub services: Services,
    pub timestamp: i64,
    pub addr_recv: NetAddr,
    pub addr_send: NetAddr,
    pub nonce: u64,
    pub user_agent: String,
    pub start_height: i32,
    pub relay: bool
}

impl fmt::Debug for VersionMessage {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "v={}, srv={:x}, h={}, agent={}",self.version, self.services.as_i64(),self.start_height, self.user_agent)
    }
}

impl Decode for VersionMessage {
    fn decode<R: io::Read>(r: &mut R) -> Result<Self, io::Error> {
        let version = decode(r)?;

        Ok(VersionMessage {
            version:   version,
            services:  decode(r)?,
            timestamp: decode(r)?,
            addr_recv: NetAddr::new(decode(r)?, decode(r)?, decode(r)?),
            addr_send: NetAddr::new(decode(r)?, decode(r)?, decode(r)?),
            nonce:     decode(r)?,
            user_agent: decode(r)?,
            start_height: decode(r)?,
            relay:     if version >= 70001 { decode(r)? } else { true }
        })
    }
}

impl Encode for VersionMessage {
    fn encode<W: io::Write>(&self, w: &mut W) -> Result<(), io::Error> {

        self.version.encode(w)?;
        self.services.encode(w)?;
        self.timestamp.encode(w)?;
        self.addr_recv.services.encode(w)?;
        self.addr_recv.ip.encode(w)?;
        self.addr_recv.port.encode(w)?;
        self.addr_send.services.encode(w)?;
        self.addr_send.ip.encode(w)?;
        self.addr_send.port.encode(w)?;
        self.nonce.encode(w)?;
        self.user_agent.encode(w)?;
        self.start_height.encode(w)?;
        if self.version >= 70001 {
            self.relay.encode(w)?;
        }
        Ok(())
    }
}

impl VersionMessage {
    #[inline]
    pub fn len(&self) -> usize {
        86 + self.user_agent.len()
    }

    #[inline]
    pub fn name(&self) -> &'static str {
        "version"
    }
}
