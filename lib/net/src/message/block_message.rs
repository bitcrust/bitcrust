use network_encoding::*;



#[derive(Debug, Default, Decode, Encode)]
pub struct BlockMessage {
    pub payload: Vec<u8>

}

impl BlockMessage {
    #[inline]
    pub fn len(&self) -> usize {
        self.payload.len()
    }

    #[inline]
    pub fn name(&self) -> &'static str {
        "block"
    }

    pub fn new(payload: Vec<u8>) -> Self {
        BlockMessage { payload: payload }
    }


}