//! Implements the `#[command]` and `command_map!` attribute
//!
//! This is used by rpc to ergonomically create commands
//!
//! See [command](fn.command.html) for usage

extern crate proc_macro;
extern crate proc_macro2;

#[macro_use]
extern crate lazy_static;

extern crate syn;

use std::sync::Mutex;
use proc_macro::TokenStream;

// The info parsed from the source code, needed to create the command map
#[derive(Clone)]
struct CmdInfo {
    name: String,
    help: String,
    params: Vec<String>
}

// Commands are added to this global for generating the command map
lazy_static! {
    static ref CMDS: Mutex<Vec<CmdInfo>> = Mutex::new(vec![]);
}


/// Defines a command
///
/// Usage:
///
/// ```
///
/// extern crate derive_command;
/// extern crate bitcrust_store as store;
/// use derive_command::command;
///
/// pub enum CommandError { MyCommandError }
/// pub type CommandResult<T> = Result<T, CommandError>;
///
///
///
/// #[command]
/// pub fn my_command(db: &store::Db, my_arg: u32) -> CommandResult<u32> {
///     Ok(3)
/// }
///
/// pub fn main() {}
/// ```
///
#[proc_macro_attribute]
pub fn command(_args: TokenStream, input: TokenStream) -> TokenStream {

    // We want to use syn, but this doesn't really work with macro_attibute yet
    // So we'll just to string parsing

    // let x: syn::token::Fn = syn::parse(input.clone()).unwrap();
    let s = input.to_string();

    let info = CmdInfo {
        name: get_name(&s),
        help: get_help(&s),
        params: get_params(&s)
    };

    // Save the command for the command-map generation
    CMDS.lock().unwrap().push(info);

    input
}


// Extracts the parameter types of the passed source code
fn get_params(str: &str) -> Vec<String> {

    // TODO rewrite using TokenStream or syn; ugly code ahead
    for l in str.lines() {
        if l.starts_with("pub fn ") {
            let params: String = l.chars()
                .skip_while(|x| *x!= '(')
                .take_while(|x| *x!= ')')
                .collect();
            return params.split(',').map(|x| {
                x.chars()
                    .skip_while(|x| *x != ':')
                    .skip(1)
                    .skip_while(|x| *x == ' ')
                    .collect()
            }).collect();
        }
    }

    vec!["u32".to_string()]
}


// Extracts the function name from the full command code
fn get_name(str: &str) -> String {

    // we should use TokenStream here, but for now we'll just use regex
    for l in str.lines() {
        if l.starts_with("pub fn ") {
            let n = l.find("(").unwrap();
            return (&l[7..n]).to_string();

        }
    }
    panic!("Could not extract function name. Start a line exactly with'pub fn name(...' ");
}

/// Extracts the documentation from the full command code
fn get_help(str: &str) -> String {

    // we should use TokenStream here, but for now we'll just parse strings
    let doclines: Vec<_> = str.lines()
        .filter(|s| s.starts_with("///"))
        .map(|s| if s.len() > 3 { &s[4..] } else { &s[3..] })
        .collect();

    doclines.join("\n")

}


/// Generates and returns the CommandMap
#[proc_macro]
pub fn command_map(_input: TokenStream) -> TokenStream {

    let mut result = "{ let mut h = ::std::collections::HashMap::new();\n\n".to_string();

    let cmds:Vec<CmdInfo> = CMDS.lock().unwrap().clone();

    for cmd in cmds {

        let invokejson_args : String = cmd.params
            .iter()
            .skip(1)
            .enumerate()
            .map(| (n,_)| {
                format!("indexed_parameter(json_input.clone(),{})?", n)

            })
            .collect::<Vec<_>>()
            .join(",");

            let invokejson = format!(
                "|db, json_input| {{ let _ = json_input; let res = ({}(db, {}))?; Ok(do_convert_to_json_value(res)) }}",
                &cmd.name, invokejson_args);

        let cmddef = format!("
            h.insert(\"{}\", Command {{
                invoke_json: {},
                get_help: || {{ \"{}\".to_string() }}
            }});\n", &cmd.name, invokejson, &cmd.help);

        result.push_str(&cmddef);

    }
    result.push_str("h }");
    result.parse().unwrap()
}


#[cfg(test)]
mod tests {

    #[test]
    fn test_param_parsing() {
        assert_eq!(
            super::get_params("pub fn testje_as12(alpha: u32, beta: String) -> u32"),
            vec!["u32".to_string(), "String".to_string()]
        );
        assert_eq!(
            super::get_params("pub fn testje_as12(alpha: Option<u32>, beta: String) -> u32"),
            vec!["Option<u32>".to_string(), "String".to_string()]
        );
    }
}