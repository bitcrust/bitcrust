//! Counter that maintains a rolling average
use serde_derive::{Serialize,Deserialize};

use time;


// 1/alpha's for average1 and average2 exponential moving average
const ALPHA_SLOW_CHANGE: u64 = 20;
const ALPHA_FAST_CHANGE: u64 = 5;

/// A counter that measures speed through exponential moving averages
#[derive(Default,Serialize,Deserialize,Clone)]
pub struct TimedCounter {

    pub total: u64,

    // Exponential averages with small and big alpha
    pub average_s: u64,
    pub average_f: u64,

    #[serde(skip)]
    pub last_measurement: Option<u64>,

    #[serde(skip)]
    pub total_since: u64,
}

impl TimedCounter {

    pub fn add(&mut self, new_value: u64) {
        let new_timestamp = time::precise_time_ns() / 1000;
        self.total_since += new_value;

        let delta = new_timestamp - self.last_measurement.unwrap_or(0);
        if delta > 2_000_000 {
            // speed in b/s
            let cur_speed = (self.total_since * 1_000_000) / delta;
            self.last_measurement = Some(new_timestamp);
            self.total += self.total_since;
            self.total_since = 0;

            self.average_s = cur_speed / ALPHA_SLOW_CHANGE + self.average_s - self.average_s / ALPHA_SLOW_CHANGE;
            self.average_f = cur_speed / ALPHA_FAST_CHANGE + self.average_f - self.average_f / ALPHA_FAST_CHANGE;

        }
    }
}
