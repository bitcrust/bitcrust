//!
//! Manages the peerinfo structure and access to the corresponding files
//! on <DATA-DIR>/peer/<PID>
//!

use std::{fs,path,io,net,fmt};
use serde_derive::{Serialize,Deserialize};
use serde_json;
use std::process;
use crate::util;
use crate::timed_counter::TimedCounter;

const DIR: &'static str = "peer";
const LOG_DIR: &'static str = "log";

pub(crate) fn init() -> Result<(), io::Error> {

    util::create_shared_memory_dir(DIR)
}


/// Information of a running peer
#[derive(Serialize, Deserialize, Clone)]
pub struct PeerInfo {

    address: net::SocketAddrV6,
    pub user_agent: String,
    pub pid: u32,
    pub services: u64,
    pub is_incoming: bool,

    pub blocks_in_flight: usize,

    pub task_current: Option<String>,

    pub bytes_read: TimedCounter,
    pub bytes_written: TimedCounter,
}

/// Task to_string is used for the filename
impl fmt::Display for PeerInfo {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "peer/{}", self.pid)
    }
}

/// Status of a Peer
#[derive(Debug,PartialEq)]
pub enum PeerState {
    /// The peer is active
    Running,

    /// The peer is not running. This could be the result of a panic or of a kill signal
    Crashed,

    /// The peer is running but appears to be hanging. This state is returned if the
    /// peer has not updated it's peerinfo in a while
    Hanging,
}

impl fmt::Display for PeerState {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "{:?}", self)
    }
}



fn map_json_err(_: serde_json::Error) -> io::Error {
    io::Error::new(io::ErrorKind::Other, "json error")
}

impl PeerInfo {

    pub fn new(address: net::SocketAddrV6) -> Self {
        PeerInfo {
            pid: process::id(),
            services: 0,
            address: address,
            user_agent: "".to_string(),
            is_incoming: false,
            blocks_in_flight: 0,
            task_current: None,
            bytes_read: Default::default(),
            bytes_written: Default::default(),
        }
    }

    pub fn open(path: path::PathBuf) -> Result<Self, io::Error> {
        let s = fs::read_to_string(path.clone());
        if s.is_err() { panic!("Read from {:?} failed", path)}
        let s = s?;
        Ok(serde_json::from_str(&s)?)
    }

    /// Write the peerinfo atomically to the filesystem
    pub fn write(&mut self) -> Result<(), io::Error> {
        let name = self.to_string();
        let new_name = name.clone() + ".new";
        fs::write(
            &new_name,
            &serde_json::to_string_pretty(self).map_err(map_json_err)?
        )?;
        fs::rename(&new_name, &name)

    }

    /// Returns all peers (including crashed and stalled)
    pub fn get_all() -> Result<Vec<PeerInfo>, io::Error> {
        Ok(fs::read_dir("peer/")?
            .filter_map(|entry| entry.ok())
            .filter(|entry| !entry.path().to_string_lossy().ends_with(".new"))
            .filter_map(|entry| Self::open(entry.path()).ok())
            .collect())
    }

    /// Returns the dynamically set loglevel
    pub fn get_loglevel(&self) -> Option<String> {
        let path = format!("{}/{}", LOG_DIR, self.pid);
        fs::read_to_string(&path).ok()
    }

    /// Returns the ipv6 address of the peer
    /// IPv4 address are mapped to ipv6
    pub fn get_address(&self) -> net::SocketAddrV6 {
        self.address
    }

    /// Update the read and write counters in memory
    /// Does not yet write to filesystem
    pub fn add_counters(&mut self, read: u64, written: u64) {
        self.bytes_read.add(read);
        self.bytes_written.add(written);
    }

    /// Returns the median of the read rate of all running peers in bytes/sec
    pub fn get_median_read_speed() -> Result<u64, io::Error> {

        let mut read_speeds = Self::get_all()?
            .iter()
            .filter(|p| p.state().unwrap_or(PeerState::Crashed) == PeerState::Running)
            .map(|p| p.bytes_read.average_s)
            .collect::<Vec<_>>();

        read_speeds.sort_unstable();
        if read_speeds.len() == 0 {
            Ok(0)
        }
        else {
            // take median
            Ok((read_speeds[read_speeds.len()/2] + read_speeds[(read_speeds.len()-1)/2])/2)
        }

    }

    /// Returns whether the peer is CRASHED, HANGING or RUNNING
    ///
    pub fn state(&self) -> Result<PeerState, io::Error> {

        Ok(if ! path::Path::new(&format!("/proc/{}", self.pid)).exists() {
            PeerState::Crashed
        } else {
            let modified = fs::metadata(&self.to_string())?.modified()?;
            if modified.elapsed()
                .map_err(|_| io::Error::new(io::ErrorKind::Other, "Time error"))?
                .as_secs() < 5
            {
                PeerState::Running
            } else {
              PeerState::Hanging
            }
        })

    }
}
