//!
//! Provides access to the state of known endpoint addresses
//! stored in <DATA-DIR>/addr/<IPV6-ADDRRESS>

use std::{fs,io,net,path};
use std::io::Write;
use serde_derive::{Serialize,Deserialize};
use rand::{thread_rng, Rng};
use crate::peerinfo::PeerInfo;


const DIR: &'static str = "addr";


pub fn init() -> Result<(), io::Error> {
    fs::create_dir_all(DIR)
}

fn addr_to_path(addr: &net::SocketAddrV6) -> path::PathBuf {
    let mut _s: Vec<_> = addr
        .ip()
        .segments()
        .iter()
        .map(|segment| format!("{:04x}", segment))
        .collect();
    // Alternative format without : and [.
    //format!("{}/{}-{}", DIR, &s.join("-"), addr.port())
    path::PathBuf::from(format!("{}/{}", DIR, addr.to_string()))
}

#[derive(Serialize, Deserialize)]
pub struct Session {

}


#[derive(Serialize, Deserialize)]
pub struct AddrInfo {
    pub address: net::SocketAddrV6,

    #[serde(skip_serializing_if = "Option::is_none")]
    pub peer_info: Option<PeerInfo>,

    #[serde(skip_serializing_if = "Option::is_none")]
    pub peer_error: Option<String>

}

impl AddrInfo {

    /// Constructs a new addrinfo
    /// This will overwrite any previous version
    pub fn new(address: &net::SocketAddrV6) -> Result<AddrInfo,io::Error> {
        let name = addr_to_path(address);
        let addr = AddrInfo {
            address: *address,
            peer_info: None,
            peer_error: None,
        };
        if let Ok(mut f) = fs::OpenOptions::new()
            .create_new(true)
            .write(true)
            .open(&name) {
            f.write_all(&serde_json::to_string_pretty(&addr).map_err(map_json_err)?.into_bytes())?;
        }

        Ok(addr)

    }

    fn read(path: &path::Path) -> Result<AddrInfo, io::Error> {
        let s = fs::read_to_string(path)?;
        Ok(serde_json::from_str(&s)?)
    }

    /// Atomically write the address info to disk
    pub fn write(&self) -> Result<(), io::Error> {
        let name = addr_to_path(&self.address);
        let bytes = serde_json::to_string_pretty(self).map_err(map_json_err)?.into_bytes();
        let new_names: String = name.to_string_lossy().into_owned() + ".new";
        let new_name = path::Path::new(&new_names);
        fs::write(new_name, bytes)?;
        fs::rename(new_name, name)
    }

    /// Returns the number of known addresses
    pub fn count() -> Result<usize, io::Error> {
        Ok(fs::read_dir(DIR)?.count())
    }

    /// Returns all known addresses in random order
    pub fn get_all() -> Result<Vec<AddrInfo>, io::Error> {
        let mut addrs: Vec<_> = fs::read_dir(DIR)?
            .filter_map(|entry| entry.ok())
            .filter(|entry| !entry.path().to_string_lossy().ends_with(".new"))
            .filter_map(|entry| Self::read(&entry.path()).ok())
            .collect();

        thread_rng().shuffle(&mut addrs);
        Ok(addrs)
    }

    pub fn start_peer_session(_addr: &net::SocketAddrV6) {

    }

    // Store the result of a peer session
    pub fn set_peer_result(addr: &net::SocketAddrV6, peer_info: Option<PeerInfo>, peer_error: Option<String>) -> Result<(), io::Error> {
        let name = addr_to_path(addr);
        let mut addr_info = AddrInfo::read(&name).or(AddrInfo::new(addr))?;
        addr_info.peer_info = peer_info;
        if peer_error.is_some() {
            error!("Peer {:?} returned {:?}", addr, peer_error);
        }
        addr_info.peer_error = peer_error;
        addr_info.write()?;
        Ok(())

    }

    /// Detect if this is on the same subnet as `other`
    ///
    /// This is a just rough indication, used to deter eclipses
    pub fn is_same_subnet(&self, other: &net::SocketAddrV6) -> bool {
        let ip1 = self.address.ip().segments();
        let ip2 = other.ip().segments();

        // We test if first and third. Or first and second are the same.
        // Actually ipv6 seems to use 4 segments for subnet but these pairs seems prevalent and suspicious enough.
        (ip1[0] == ip2[0] && ip1[1] == ip2[1] && ip1[0] != 0 && ip1[1] != 0)
        || (ip1[0] == ip2[0] && ip1[2] == ip2[2] && ip1[0] != 0 && ip1[2] != 0)
        // Same ipv4 address? We only test first byte of ipv4 and #5 is ffff
        || (ip1[5] == 0xffff && ip2[5] == 0xffff && (ip1[6] & 0xff00) == (ip2[6] & 0xff00) )

    }
}



fn map_json_err(_: serde_json::Error) -> io::Error {
    io::Error::new(io::ErrorKind::Other, "json error")
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_subnets() {
        fn is_same_subnet(s1: &str, s2: &str) -> bool {
            let a1 = AddrInfo { address: s1.to_owned().parse().unwrap(), peer_info: None, peer_error:None};
            a1.is_same_subnet(&s2.to_owned().parse::<net::SocketAddrV6>().unwrap())
        }

        assert!(is_same_subnet("[::ffff:158.69.156.53]:8333", "[::ffff:158.69.156.52]:8333") );
        assert!(!is_same_subnet("[::ffff:158.69.156.53]:8333", "[::ffff:159.203.120.173]:8333") );
        assert!(!is_same_subnet("[::ffff:158.69.156.53]:8333", "[2001:0:9d38:6abd:1897:1c10:ce44:6836]:8333") );
        assert!(is_same_subnet("[2001:0:9d38:6abd:c3b:1eee:2c03:ac9f]:8333", "[2001:0:9d38:6abd:1897:1c10:ce44:6836]:8333") );
        assert!(!is_same_subnet("[2001:0:9d38:6abd:c3b:1eee:2c03:ac9f]:8333", "[2001:19f0:9002:35e:5400:ff:fe77:4364]:8333") );

    }
}
