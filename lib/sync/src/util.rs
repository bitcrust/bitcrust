
use std::{fs,path,io};
use std::os::unix;
use libc;
use rand::{self,Rng};

const SHARED_MEM_PATH: &'static str = "/dev/shm/bitcrust";

/// Creates a shared memory link if supported by the platform.
///
/// Creates a normal dir if this is not supported. This shouldn't have too much impact
/// as we're never flushing these small files anyway
pub fn create_shared_memory_dir(dir: &str) -> Result<(), io::Error> {

    // Already exists?
    if let Ok(metadata) = fs::metadata(&dir) {
        if metadata.is_dir() {
            return Ok(());
        } else {
            return Err(io::Error::new(io::ErrorKind::Other, "Target sync path is not a directory"));
        }
    }

    // First try as symlink to a shm. We randomize the shm location as we don't want instances using
    // different data-dirs using the same shm location.
    let shm_subdir = format!("{}-{}", dir, rand::thread_rng().sample_iter(&rand::distributions::Alphanumeric).take(15).collect::<String>());
    let shm_dir = path::PathBuf::from(SHARED_MEM_PATH).join(shm_subdir);
    let _ = fs::remove_dir_all(&shm_dir);
    let _ = fs::create_dir_all(&shm_dir);
    let _ = unix::fs::symlink(&shm_dir, &dir);

    // create normal dir if symlink didn't succeed
    let _ = fs::create_dir(&dir);

    // check
    fs::metadata(&dir).map(|_| ())
}

/// Returns true if the process with the given pid is currently running
/// Uses kill(pid,0) to check
pub fn is_process_running(pid: libc::pid_t) -> bool {
    debug_assert!(pid != 0);

    let result = unsafe { libc::kill(pid, 0) };
    result == 0
}
