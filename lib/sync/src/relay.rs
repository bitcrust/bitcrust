
use std::io;

use crate::util;
use memmap;

pub mod busy;

const DIR: &'static str = "relay";


pub fn init() -> Result<(), io::Error> {

    util::create_shared_memory_dir(DIR)

}

pub struct Relays {
    addr_relay: Relay,
    tx_relay: Relay,
    block_relay: Relay,
}


pub struct Relay {
    _mmap: memmap::Mmap,
    buffer: &'static [u8]
}
