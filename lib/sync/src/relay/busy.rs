//! Busy List
//!
//! This is a small hash table in shared memory to keep track who is busy fetching an object
//!
use network_encoding::*;
use std::{time,fs,io,mem,path};
use crate::util;

const FILE_NAME: &str = "relay/busy";
const INDEX_BITS: usize = 22;

#[derive(Debug,Default,Copy,Clone)]
struct Entry {

    pid: libc::pid_t, // pid that is busy
    timestamp: u32,   // wrapping timestamp in milliseconds
}


/// Busy List
///
/// This is a small hash table in shared memory to keep track who is busy fetching an object
///
/// # Examples
///
/// ```rust
/// use hex_literal::hex;
/// use network_encoding::*;
/// use bitcrust_sync as sync;
///
/// sync::init("tst-busy-list").unwrap();
///
/// let busy_list = sync::BusyList::open().unwrap();
///
/// let hash1 = hash!("000000000019d6689c085ae165831e934ff763ae46a2a6c172b3f1b60a8ce26f");
/// let hash2 = hash!("681649a3bef76a2be9325227bdee29980f2f44ee11016aac89c81192bc34eef1");
///
/// assert!(busy_list.claim(&hash1, 2000));
/// assert!(busy_list.claim(&hash2, 2000));
///
/// // Can't claim again
/// assert!(!busy_list.claim(&hash1, 2000));
///
/// // Takeover after small timeout
/// std::thread::sleep(std::time::Duration::from_millis(60));
/// assert!(busy_list.claim(&hash2, 50));
///
/// // Takeover after `unclaim()`
/// busy_list.unclaim(&hash1);
/// assert!(busy_list.claim(&hash1, 2000));
/// ```
pub struct BusyList {
    _mmap:       memmap::Mmap,
    entries:     &'static [atomic::Atomic<Entry>],
}

// Returns the timestamp wrapped at 32 bits
fn get_timestamp() -> u32 {
    time::SystemTime::now()
        .duration_since(time::UNIX_EPOCH)
        .unwrap()
        .as_millis()
        as u32
}


fn mini_hash(hash: &Hash) -> usize {
    // We could consider hashing with a local seed here
    // to prevent peers from filling
    let hash_bytes = hash.into_inner();
    (hash_bytes[16] as usize
        | ((hash_bytes[17] as usize) << 8)
        | ((hash_bytes[18] as usize) << 16)
        | ((hash_bytes[19] as usize) << 24))
        & ((1 << INDEX_BITS)-1)
}


impl BusyList {
    pub fn open() -> Result<Self, io::Error> {

        const FILE_LEN: u64 = (1<<INDEX_BITS) * mem::size_of::<Entry>() as u64;

        let path = path::Path::new(FILE_NAME);
        if !path.exists() {
            let file = fs::File::create(FILE_NAME)?;
            file.set_len(FILE_LEN)?;
        }

        let rw_file = fs::OpenOptions::new().read(true).write(true).open(&path)?;

        let _mmap = memmap::Mmap::open_with_offset(&rw_file,
                memmap::Protection::ReadWrite,
                0,
                FILE_LEN as usize)?;

        let index_ptr =  _mmap.ptr() as *const atomic::Atomic<Entry>;
        let entries = unsafe { ::std::slice::from_raw_parts(index_ptr, 1 << INDEX_BITS) };

        Ok(BusyList {
            _mmap,entries
        })
    }

    /// Claim the given Hash
    ///
    /// This will return false if another process has already claimed this hash,
    /// or a colliding hash within the last `timeout` milliseconds, and the process is still running
    ///
    /// Otherwise it will return true and mark the hash as busy.
    /// The caller should ensure `unclaim()` is called when done
    pub fn claim(&self, hash: &Hash, timeout: time::Duration) -> bool {
        let timeout = timeout.as_millis() as u32;
        let mini_hash = mini_hash(hash);
        let now = get_timestamp();
        let new_entry = Entry {
            pid: unsafe { libc::getpid() },
            timestamp: get_timestamp(),
        };
        loop {
            let prev = self.entries[mini_hash].load(atomic::Ordering::Relaxed);

            if prev.timestamp > 0 {
                // Already busy; check for timeout
                if now.wrapping_sub(prev.timestamp) > timeout {
                    warn!("Taking over slow task {} from {}", hash, prev.pid);

                }
                else if now.wrapping_sub(prev.timestamp) > 1000 && !util::is_process_running(prev.pid) {
                    warn!("Taking over task {} from closed process{}", hash, prev.pid);

                }
                else {
                    return false;
                }
            }

            if self.entries[mini_hash].compare_exchange_weak
                (prev, new_entry, atomic::Ordering::Relaxed, atomic::Ordering::Relaxed).is_ok()
            {
                trace!(target: "busy_list", "Set lock for {} of {}", hash, new_entry.pid);
                return true;
            }
        }
    }

    pub fn unclaim(&self, hash: &Hash) {
        let mini_hash = mini_hash(hash);
        loop {
            let prev = self.entries[mini_hash].load(atomic::Ordering::Relaxed);
            if prev.pid != unsafe { libc::getpid() } {
                // already taken over
                warn!("Hash {} already taken over by {}", hash, prev.pid);
                break;
            }
            if self.entries[mini_hash].compare_exchange_weak
                (prev, Entry::default(), atomic::Ordering::Relaxed, atomic::Ordering::Relaxed).is_ok()
            {
                trace!(target: "busy_list", "Released lock for {} of {}", hash, prev.pid);
                break;
            }
        }
    }
}
