//! Library for IPC synchronization primitives using files and shared memory
//!
//! The library methods are stateless and assume the CWD is correctly set to the data dir
//! as done in init()
//!
//! The only errors returned io::Error and usually unrecoverable. The caller may unwrap.
//!
//! # Examples
//!
//! ```
//! use bitcrust_sync as sync;
//!
//! sync::init("tst-sync-lib2").unwrap();
//!
//! let all_running_peers = sync::PeerInfo::get_all().unwrap();
//! let all_known_addrs = sync::AddrInfo::get_all().unwrap();
//! assert_eq!(all_running_peers.len(), all_known_addrs.len());
//! ```
//!

pub mod peerinfo;
pub mod addrinfo;
pub mod relay;
mod util;
mod timed_counter;

pub use crate::peerinfo::PeerInfo;
pub use crate::addrinfo::AddrInfo;
pub use crate::relay::busy::BusyList;

use std::{env,fs,path,io};

use libc;
#[macro_use]
extern crate log;

extern crate network_encoding;

extern crate rand;


/// Initialize the directories if needed
/// and set the correct working directory
pub fn init<P: AsRef<path::Path>>(path: P) -> Result<(), io::Error> {

    let _ = fs::create_dir_all(&path);
    env::set_current_dir(&path)?;

    peerinfo::init()?;
    addrinfo::init()?;
    relay::init()?;

    Ok(())
}


/// Returns true if the given pidfile is present and contains a pid of a program that is
/// still running
pub fn is_pid_file_active<P: AsRef<path::Path>>(path: P) -> Result<bool, io::Error> {

    let s = match fs::read_to_string(path) {
        Ok(s) => s,
        Err(e) if e.kind() == io::ErrorKind::NotFound => return Ok(false),
        Err(e) => return Err(e)
    };
    if let Ok(pid) = s.parse::<libc::pid_t>() {
        return Ok(util::is_process_running(pid))
    }
    else {
        error!("Invalid lockfile content {}. Expected pid", s);
        return Ok(false); // No pid in content? Then no lock
    }
}

pub fn set_pid_file<P: AsRef<path::Path>>(path: P) -> Result<bool, io::Error> {
    if is_pid_file_active(path.as_ref())? {
        return Ok(false);
    }
    //let path: &path::Path = ;
    let new_path = path.as_ref().with_extension(".new");
    fs::write(&new_path, unsafe { libc::getpid() }.to_string())?;
    Ok(fs::rename(new_path, path).is_ok())
}
