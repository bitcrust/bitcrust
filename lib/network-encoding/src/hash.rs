//! SHA-2 256 hash object

use ring;
use std::{io,fmt,cmp};
use crate::Encode;
use crate::Decode;


/// Hash constants
///
/// # Examples
///
/// ```rust
/// #[macro_use]
/// extern crate hex_literal;
///
/// #[macro_use]
/// extern crate network_encoding;
/// use self::network_encoding::Hash;
///
/// fn main() {
///     const GENESIS_BLOCK_HASH: Hash = hash!("000000000019d6689c085ae165831e934ff763ae46a2a6c172b3f1b60a8ce26f");
/// }
/// ```
#[macro_export]
macro_rules! hash {
    ($e:expr) => { Hash::from_bytes_rev(&hex!($e)) }
}

/// SHA-2 256-bit hash object
#[derive(Default,PartialEq,Eq,Copy,Clone)]
pub struct Hash([u8; 32]);

impl From<[u8;32]> for Hash {
    fn from(v: [u8;32]) -> Hash {
        Hash(v)
    }
}

impl AsRef<[u8;32]> for Hash {
    fn as_ref(&self) -> &[u8;32] {
        &self.0
    }
}

impl<'a> From<&'a [u8]> for Hash {
    fn from(v: &'a [u8]) -> Hash {
        let mut result = [0;32];
        result.copy_from_slice(&v[0..32]);
        Hash(result)
    }
}


impl<'a> From<&'a str> for Hash {
    fn from(v: &'a str) -> Hash {
        Hash::try_from(v).expect("Invalid hex-string")
    }
}

impl ::std::hash::Hash for Hash {
    fn hash<H: ::std::hash::Hasher>(&self, state: &mut H) {
        self.0.hash(state);
    }
}

impl Encode for Hash {
    fn encode<W: io::Write>(&self, w: &mut W) -> Result<(), io::Error> {
        self.0.encode(w)
    }
}

impl Decode for Hash {
    #[inline(always)]
    fn decode<R: io::Read>(r: &mut R) -> Result<Self, io::Error> {
        Decode::decode(r).map(|v| Hash(v))
    }
}

impl fmt::Debug for Hash {
    fn fmt(&self, f: &mut fmt::Formatter) -> Result<(), fmt::Error> {

        write!(f, "{:<6}", self)
    }
}

impl fmt::Display for Hash {
    fn fmt(&self, f: &mut fmt::Formatter) -> Result<(), fmt::Error> {
        for b in self.0.iter().rev() {
            write!(f, "{:02x}", b)?;
        }
        Ok(())
    }
}

impl Ord for Hash {
    fn cmp(&self, other: &Hash) -> cmp::Ordering {
        self.0.cmp(&other.0)
    }
}

impl PartialOrd for Hash {
    fn partial_cmp(&self, other: &Hash) -> Option<cmp::Ordering> {
        Some(self.cmp(other))
    }
}

impl Hash {

    pub const fn into_inner(self) -> [u8;32] {
        self.0
    }

    pub const fn null() -> Hash {
        Hash([0;32])
    }

    pub fn is_null(&self) -> bool {
        self.0 == [0;32]
    }

    /// Hashes the input twice with SHA256 and returns an owned buffer;
    pub fn from_data(input: &[u8]) -> Hash {
        let hash1 = ring::digest::digest(&ring::digest::SHA256, input);
        let hash2 = ring::digest::digest(&ring::digest::SHA256, hash1.as_ref());

        Hash::from(hash2.as_ref())
    }

    pub fn from_pair(first: &Hash, second: &Hash) -> Hash {
        let mut v: Vec<u8> = Vec::new();
        v.extend(first.0.iter());
        v.extend(second.0.iter());

        Self::from_data(&v)
    }

    pub const fn from_bytes_rev(hash: &[u8]) -> Self {
        // Without loop for constant evalutaion
        // Change to loop once this lands in rust
        Hash([
             hash[31],hash[30],hash[29],hash[28],hash[27],hash[26],hash[25],hash[24],
             hash[23],hash[22],hash[21],hash[20],hash[19],hash[18],hash[17],hash[16],
             hash[15],hash[14],hash[13],hash[12],hash[11],hash[10],hash[9],hash[8],
             hash[7],hash[6],hash[5],hash[4],hash[3],hash[2],hash[1],hash[0],
        ])
    }

    pub fn try_from(v: &str) -> Option<Hash> {
        let mut b = Vec::with_capacity(v.len() / 2);
        let mut modulus = 0;
        let mut buf = 0;

        for byte in v.bytes() {
            buf <<= 4;

            match byte {
                b'A'..=b'F' => buf |= byte - b'A' + 10,
                b'a'..=b'f' => buf |= byte - b'a' + 10,
                b'0'..=b'9' => buf |= byte - b'0',
                b' '|b'\r'|b'\n'|b'\t' => {
                    buf >>= 4;
                    continue
                }
                _ => {
                    return None;
                }
            }

            modulus += 1;
            if modulus == 2 {
                modulus = 0;
                b.push(buf);
            }
        }

        b = b.into_iter().rev().collect();
        if b.len() != 32 { return None }
        let slice: &[u8] = &b;
        Some(Hash::from(slice))
    }
}

#[cfg(test)]
mod tests {
    use super::*;


    #[test]
    fn test_double_hash() {


        const HASH1: &'static str = "212300e77d897f2f059366ed03c8bf2757bc2b1dd30df15d34f6f1ee521e58e8";
        const HASH2: &'static str = "4feec9316077e49b59bc23173303e13be9e9f5f9fa0660a58112a04a65a84ef1";
        const HASH3: &'static str = "03b750bf691caf40b7e33d8e15f64dd16becf944b39a82710d6d257159361b93";

        let hash1 = Hash::from(HASH1);
        let hash2 = Hash::from(HASH2);
        let hash3 = Hash::from(HASH3);

        let paired = Hash::from_pair(&hash1, &hash2);

        assert_eq!(hash3, paired);

    }
}
