use std::io;
use std::num::NonZeroU64;
use std::net::Ipv6Addr;
use std::marker::Sized;
use byteorder::{LittleEndian, BigEndian, ReadBytesExt};
use ring;
use crate::Hash;

/// Allows a type to be decoded from a byte-stream in Bitcoin network-format
pub trait Decode {
    fn decode<R: io::Read>(_: &mut R) -> Result<Self, io::Error> where Self: Sized;

}

impl Decode for u8 {

    #[inline(always)]
    fn decode<R: io::Read>(r: &mut R) -> Result<Self, io::Error> {
        r.read_u8()
    }
}

impl Decode for u16 {
    #[inline(always)]
    fn decode<R: io::Read>(r: &mut R) -> Result<Self, io::Error> {
        r.read_u16::<BigEndian>()
    }
}


impl Decode for u32 {
    #[inline(always)]
    fn decode<R: io::Read>(r: &mut R) -> Result<Self, io::Error> {
        r.read_u32::<LittleEndian>()
    }
}

impl Decode for i32 {
    #[inline(always)]
    fn decode<R: io::Read>(r: &mut R) -> Result<Self, io::Error> {
        r.read_i32::<LittleEndian>()
    }
}

impl Decode for u64 {
    #[inline(always)]
    fn decode<R: io::Read>(r: &mut R) -> Result<Self, io::Error> {
        r.read_u64::<LittleEndian>()
    }
}

impl Decode for i64 {
    #[inline(always)]
    fn decode<R: io::Read>(r: &mut R) -> Result<Self, io::Error> {
        r.read_i64::<LittleEndian>()
    }
}

impl Decode for bool {
    #[inline(always)]
    fn decode<R: io::Read>(r: &mut R) -> Result<Self, io::Error> { Ok(r.read_u8()? != 0) }
}

impl Decode for usize {
    #[inline(always)]
    fn decode<R: io::Read>(r: &mut R) -> Result<Self, io::Error> {
        let byte1: u8 = r.read_u8()?;
        Ok(match byte1 {
            0xff => r.read_u64::<LittleEndian>()? as usize,
            0xfe => r.read_u32::<LittleEndian>()? as usize,
            0xfd => r.read_u16::<LittleEndian>()? as usize,
            _ => byte1 as usize
        })
    }
}

impl<T> Decode for [T;4] where T: Decode {
    #[inline(always)]
    fn decode<R: io::Read>(r: &mut R) -> Result<Self, io::Error> {
        Ok([
            T::decode(r)?,
            T::decode(r)?,
            T::decode(r)?,
            T::decode(r)?,
        ])
    }
}
impl<T> Decode for [T;6] where T: Decode {
    #[inline(always)]
    fn decode<R: io::Read>(r: &mut R) -> Result<Self, io::Error> {
        Ok([
            T::decode(r)?,
            T::decode(r)?,
            T::decode(r)?,
            T::decode(r)?,
            T::decode(r)?,
            T::decode(r)?,
        ])
    }
}
impl<T> Decode for [T;8] where T: Decode {
    #[inline(always)]
    fn decode<R: io::Read>(r: &mut R) -> Result<Self, io::Error> {
        Ok([
            T::decode(r)?,
            T::decode(r)?,
            T::decode(r)?,
            T::decode(r)?,
            T::decode(r)?,
            T::decode(r)?,
            T::decode(r)?,
            T::decode(r)?,
        ])
    }
}

impl Decode for [u8;32] {
    #[inline(always)]
    fn decode<R: io::Read>(r: &mut R) -> Result<Self, io::Error> {
        let mut buf = [0u8;32];
        r.read_exact(&mut buf[..])?;
        Ok(buf)
    }
}

impl Decode for [u8;80] {
    #[inline(always)]
    fn decode<R: io::Read>(r: &mut R) -> Result<Self, io::Error> {
        let mut buf = [0u8;80];
        r.read_exact(&mut buf[..])?;
        Ok(buf)
    }
}

impl Decode for [u8;12] {
    #[inline(always)]
    fn decode<R: io::Read>(r: &mut R) -> Result<Self, io::Error> {
        let mut buf = [0u8;12];
        r.read_exact(&mut buf[..])?;
        Ok(buf)
    }
}

impl Decode for String {
    #[inline(always)]
    fn decode<R: io::Read>(r: &mut R) -> Result<Self, io::Error> {
        let bytes = <Vec<u8>>::decode(r)?;

        Ok(String::from_utf8(bytes).unwrap_or("invalid-utf8".to_owned()))
    }
}

impl<T: From<NonZeroU64>> Decode for Option<T> {
    #[inline(always)]
    fn decode<R: io::Read>(r: &mut R) -> Result<Self, io::Error> {
        Ok(NonZeroU64::new(r.read_u64::<LittleEndian>()?).map(T::from))

    }

}


impl<T : Decode> Decode for Vec<T> {
    #[inline(always)]
    fn decode<R: io::Read>(r: &mut R) -> Result<Self, io::Error> {

        let len = usize::decode(r)?;
        let mut result = Vec::with_capacity(len);
        for _ in 0..len {
            result.push(T::decode(r)?);
        }
        Ok(result)
    }
}

impl Decode for Ipv6Addr {

    fn decode<R: io::Read>(r: &mut R) -> Result<Self, io::Error> {

        Ok(Ipv6Addr::new(
            r.read_u16::<BigEndian>()?,
            r.read_u16::<BigEndian>()?,
            r.read_u16::<BigEndian>()?,
            r.read_u16::<BigEndian>()?,
            r.read_u16::<BigEndian>()?,
            r.read_u16::<BigEndian>()?,
            r.read_u16::<BigEndian>()?,
            r.read_u16::<BigEndian>()?
        ))
    }
}


/// WithHash represents an object and its double-sha256 hash
///
/// When decoding WithHash<object>, the hash is calculated automatically.
///
/// When encoding WithHash<object>, the hash is omitted.
///
/// # Examples
///
/// ```rust
/// use network_encoding::*;
///
/// // a decodable byte sequence
/// let data = &[4u8,0,0,0];
///
/// // decode normally
/// let decoded: u32 = decode_buf(data).unwrap();
///
/// // decode with hash
/// let decoded2: WithHash<u32> = decode_buf(data).unwrap();
///
/// assert_eq!(decoded, decoded2.d);
///
/// ```
#[derive(Debug)]
pub struct WithHash<D> {
    pub d: D,
    pub hash: Hash
}


#[cfg(test)]
impl<T : crate::Encode + crate::Decode> From<T> for WithHash<T> {
    fn from(s: T) -> WithHash<T> {
        let buf = crate::encode_buf(&s);
        crate::decode_buf(&buf).unwrap()
    }
}


impl<D> crate::Encode for WithHash<D>
where D: crate::Encode {
    #[inline(always)]
    fn encode<W: io::Write>(&self, w: &mut W) -> Result<(), io::Error> {
        self.d.encode(w)
    }
}



impl<D> Decode for WithHash<D>
where D: Decode {
    #[inline(always)]
    fn decode<R: io::Read>(r: &mut R) -> Result<Self, io::Error> {

        struct Reader<R : io::Read> {
            inner: R,
            pub hash_ctx: ring::digest::Context
        }
        impl<R> io::Read for Reader<R> where R : io::Read {
            fn read(&mut self, buf: &mut [u8]) -> io::Result<usize> {
                let read = self.inner.read(buf)?;
                self.hash_ctx.update(&buf[0..read]);
                Ok(read)
            }
        }
        let mut inner_reader = Reader {
            inner: r,
            hash_ctx: ring::digest::Context::new(&ring::digest::SHA256)
        };
        let d = D::decode(&mut inner_reader)?;
        let digest1 = inner_reader.hash_ctx.finish();
        let digest2 = ring::digest::digest(&ring::digest::SHA256, digest1.as_ref());

        Ok(
            WithHash {
                d: d,
                hash: Hash::from(digest2.as_ref())
        })
    }
}
