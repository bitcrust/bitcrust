#![feature(const_fn,const_if_match)]


//! Bitcoin network encoding and decoding
//!
//! Uses standard conventions:
//!
//! * Vec<T> is a compact-size prefixed set
//! * usize is a compact-size
//! * String is a compact-size prefixed UTF-8 string
//! * [u8;32] is a hash
//! * u*nn*/i*nn* is a little endian integer
//!
//! This crate can be used in combination with [encode-derive][1] to enable
//! network encoding attributes `#[derive(Encode,Decode)]`
//!
//!
//!
//! [1]: ../encode_derive/index.html

#![feature(trace_macros)]


extern crate byteorder;
extern crate ring;

mod encode;
mod decode;
mod hash;

pub use crate::encode::Encode;
pub use crate::decode::Decode;
pub use crate::hash::Hash;
pub use crate::decode::WithHash;

use std::{io,mem};



/// Decodes the given buffer into any type which implements the Decode trait
///
/// # Errors
///
/// Returns Err(io::Error) if the buffer is too short
///
/// # Examples
///
/// ```rust
/// extern crate network_encoding;
/// use network_encoding::*;
///
/// let a = &[5u8,0,0,0];
/// let b:u32 = decode_buf(a).unwrap();
/// assert_eq!(b, 5);
/// ```
///
#[inline(always)]
pub fn decode_buf<T: Decode>(buf: &[u8]) -> Result<T, io::Error> {
    T::decode(&mut io::Cursor::new(buf))
}



/// Decodes from the given reader into any type which implements the Decode trait
///
/// # Errors
///
/// Returns Err(io::Error) only if the reader returns one.
///
/// # Examples
///
/// ```rust
/// extern crate network_encoding;
/// use std::io;
/// use network_encoding::*;
///
/// let a = &[5u8,0,0,0];
/// let mut cur = io::Cursor::new(a);
/// let b:u32 = decode(&mut cur).unwrap();
/// assert_eq!(b,5);
/// ```
#[inline(always)]
pub fn decode<R: io::Read, T: Decode>(r: &mut R) -> Result<T, io::Error> {
    T::decode(r)
}

/// Encodes any type with the Encode trait into a Vec<u8> buffer
///
/// # Errors
///
/// Return io::Error if writing does. This normally cannot happen as we allocate
/// the buffer as needed
///
/// # Examples
///
/// ```rust
/// use network_encoding::*;
///
/// let data = vec![1u32, 2u32];
/// let encoded = encode_buf(&data);
///
/// // vec is encoded as compact size prefixed
/// assert_eq!(&encoded, &[2u8, 1u8,0u8,0u8,0u8, 2u8,0u8,0u8,0u8]);
/// ```
#[inline(always)]
pub fn encode_buf<T: Encode>(obj: &T) -> Vec<u8> {
    let mut buf = Vec::with_capacity(crate::mem::size_of::<T>());

    // We can safely unwrap as our writer is memory
    obj.encode(&mut buf)
        .expect("Encoding failed");

    buf
}

/// Encodes any type with the Encode trait into a writer
///
/// # Errors
///
/// Return Err(io::Error) if the writing does.
///
/// # Examples
///
/// ```rust
/// use std::io::Write;
/// use network_encoding::*;
///
/// let data = vec![1u32, 2u32];
/// let mut writer: Vec<u8> = Vec::new();
/// encode(&data, &mut writer).unwrap();
///
/// // vec is encoded as compact size prefixed
/// assert_eq!(&writer, &[2u8, 1u8,0u8,0u8,0u8, 2u8,0u8,0u8,0u8]);
/// ```
#[inline(always)]
pub fn encode<W: io::Write, T: Encode>(obj: &T, w: &mut W) -> Result<(), io::Error> {
    obj.encode(w)
}
