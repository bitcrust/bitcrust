use std::io;
use std::net::Ipv6Addr;
use byteorder::{LittleEndian, BigEndian, WriteBytesExt};

/// Allows a type to be encoded to a byte-stream in Bitcoin network-format
pub trait Encode {
    fn encode<W: io::Write>(&self, _: &mut W) -> Result<(), io::Error>;
}

impl Encode for u8 {
    #[inline(always)]
    fn encode<W: io::Write>(&self, w: &mut W) -> Result<(), io::Error> {
        w.write_u8(*self)
    }
}

impl Encode for u16 {
    #[inline(always)]
    fn encode<W: io::Write>(&self, w: &mut W) -> Result<(), io::Error> {
        w.write_u16::<BigEndian>(*self)
    }
}

impl Encode for u32 {
    #[inline(always)]
    fn encode<W: io::Write>(&self, w: &mut W) -> Result<(), io::Error> {
        w.write_u32::<LittleEndian>(*self)
    }
}

impl Encode for u64 {
    #[inline(always)]
    fn encode<W: io::Write>(&self, w: &mut W) -> Result<(), io::Error> {
        w.write_u64::<LittleEndian>(*self)
    }
}

impl Encode for i8 {
    #[inline(always)]
    fn encode<W: io::Write>(&self, w: &mut W) -> Result<(), io::Error> {
        w.write_i8(*self)
    }
}

impl Encode for i16 {
    #[inline(always)]
    fn encode<W: io::Write>(&self, w: &mut W) -> Result<(), io::Error> {
        w.write_i16::<LittleEndian>(*self)
    }
}

impl Encode for i32 {
    #[inline(always)]
    fn encode<W: io::Write>(&self, w: &mut W) -> Result<(), io::Error> {
        w.write_i32::<LittleEndian>(*self)
    }
}

impl Encode for i64 {
    #[inline(always)]
    fn encode<W: io::Write>(&self, w: &mut W) -> Result<(), io::Error> {
        w.write_i64::<LittleEndian>(*self)
    }
}

// usize is encoded as compact varint
impl Encode for usize {
    #[inline(always)]
    fn encode<W: io::Write>(&self, w: &mut W) -> Result<(), io::Error> {
        if *self < 0xFD {
            w.write_u8(*self as u8)?;
        }
        else if *self < 0xFFFF {
            w.write_u8(0xFD)?;
            w.write_u16::<LittleEndian>(*self as u16)?;
        }
        else if *self < 0xFFFF_FFFF {
            w.write_u8(0xFE)?;
            w.write_u32::<LittleEndian>(*self as u32)?;
        }
        else {
            w.write_u8(0xFF)?;
            w.write_u64::<LittleEndian>(*self as u64)?;
        }
        Ok(())
    }
}


impl Encode for bool {
    #[inline(always)]
    fn encode<W: io::Write>(&self, w: &mut W) -> Result<(), io::Error> {
        match *self {
            true => w.write_u8(1),
            false => w.write_u8(0),
        }
    }
}

impl Encode for Ipv6Addr {
    // write IP
    #[inline(always)]
    fn encode<W: io::Write>(&self, w: &mut W) -> Result<(), io::Error> {
        for octet in self.segments().iter() {
            w.write_u16::<BigEndian>(*octet)?;
        }
        Ok(())
    }
}

impl Encode for [u8] {
    #[inline(always)]
    fn encode<W: io::Write>(&self, w: &mut W) -> Result<(), io::Error> {
        for byte in self {
            w.write_u8(*byte)?;
        }
        Ok(())
    }
}

impl<T> Encode for [T; 4] where T: Encode {
    #[inline(always)]
    fn encode<W: io::Write>(&self, w: &mut W) -> Result<(), io::Error> {
        self[0].encode(w)?;
        self[1].encode(w)?;
        self[2].encode(w)?;
        self[3].encode(w)?;
        Ok(())
    }
}

impl<T> Encode for [T; 6] where T: Encode {
    #[inline(always)]
    fn encode<W: io::Write>(&self, w: &mut W) -> Result<(), io::Error> {
        self[0].encode(w)?;
        self[1].encode(w)?;
        self[2].encode(w)?;
        self[3].encode(w)?;
        self[4].encode(w)?;
        self[5].encode(w)?;
        Ok(())
    }
}
impl<T> Encode for [T; 8] where T: Encode {
    #[inline(always)]
    fn encode<W: io::Write>(&self, w: &mut W) -> Result<(), io::Error> {
        self[0].encode(w)?;
        self[1].encode(w)?;
        self[2].encode(w)?;
        self[3].encode(w)?;
        self[4].encode(w)?;
        self[5].encode(w)?;
        self[6].encode(w)?;
        self[7].encode(w)?;
        Ok(())
    }
}

impl Encode for [u8; 32] {
    #[inline(always)]
    fn encode<W: io::Write>(&self, w: &mut W) -> Result<(), io::Error> {
        for byte in self {
            w.write_u8(*byte)?;
        }
        Ok(())
    }
}


impl Encode for String {
    #[inline(always)]
    fn encode<W: io::Write>(&self, w: &mut W) -> Result<(), io::Error> {
        let bytes = self.as_bytes();
        bytes.len().encode(w)?;
        for byte in bytes {
            w.write_u8(*byte)?;
        }
        Ok(())
    }
}


impl<T> Encode for Vec<T> 
    where T: Encode {
    #[inline(always)]
    fn encode<W: io::Write>(&self, w: &mut W) -> Result<(), io::Error> {
        let size: usize = self.len();
        size.encode(w)?;
        for item in self {
            item.encode(w)?;
        }
        Ok(())
    }
}

