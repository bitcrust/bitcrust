//! Implements the Encode / Decode derive attributes
//!
//! This generates code that invokes network-serialization
//!
//! # Examples
//!
//! ```rust
//! #[macro_use]
//! extern crate encode_derive;
//! extern crate network_encoding;
//! use network_encoding::*;
//!
//! #[derive(Encode, Decode)]
//! struct Foo {
//!    a: u32,
//!    b: Vec<u32>,
//!
//! }
//!
//! fn main() {
//!     let x = Foo { a: 1, b: vec![2,3] };
//!     let z = encode_buf(&x);
//!     assert_eq!(z, &[
//!         1,0,0,0, // a as little-endian
//!         2,       // compact size of b
//!         2,0,0,0,
//!         3,0,0,0, // elements of b
//!     ]);
//! }
//! ```



extern crate proc_macro;
extern crate syn;
#[macro_use]
extern crate quote;
extern crate byteorder;


use proc_macro::TokenStream;




#[proc_macro_derive(Encode, attributes(count))]
pub fn derive_encode(input: TokenStream) -> TokenStream {
    // Construct a string representation of the type definition
    let s = input.to_string();
    
    // Parse the string representation
    let ast = syn::parse_derive_input(&s).unwrap();

    // Build the impl
    let gen = impl_encode(&ast);
    
    // Return the generated impl
    gen.parse().unwrap()
}

fn impl_encode(ast: &syn::DeriveInput) -> quote::Tokens {
    let name = &ast.ident;
    let fields = match ast.body {
        syn::Body::Struct(ref data) => data.fields(),
        syn::Body::Enum(_) => panic!("#[derive(Encode)] can only be used with structs"),
    };
    let fields = generate_fields_encode(&fields);
    quote! {
        impl Encode for #name {
            fn encode<W: ::std::io::Write>(&self, w: &mut W) -> Result<(), ::std::io::Error> {
                #(#fields)*
                Ok(())
            }
        }
    }
    
}

fn generate_fields_encode(fields: &[syn::Field]) -> Vec<quote::Tokens> {
    let mut result = Vec::new();
    for field in fields {
        let ident = &field.ident;
        result.push(quote!{
            self.#ident.encode(w)?;
        });
    }
    result
}


#[proc_macro_derive(Decode, attributes(count))]
pub fn derive_decode(input: TokenStream) -> TokenStream {
    // Construct a string representation of the type definition
    let s = input.to_string();

    // Parse the string representation
    let ast = syn::parse_derive_input(&s).unwrap();

    // Build the impl
    let gen = impl_decode(&ast);

    // Return the generated impl
    gen.parse().unwrap()
}

fn impl_decode(ast: &syn::DeriveInput) -> quote::Tokens {
    let name = &ast.ident;
    let fields = match ast.body {
        syn::Body::Struct(ref data) => data.fields(),
        _ => panic!("#[derive(Decode)] can only be used with structs"),
    };
    if fields.len() == 1 && fields[0].ident.is_none() {
        panic!("Tuple struct found");
    }
    let fields = generate_fields_decode(&fields);
    quote! {
        impl Decode for #name {
            fn decode<R: ::std::io::Read>(r: &mut R) -> Result<Self, ::std::io::Error> {
                Ok( #name {
                    #(#fields)*
                })
            }
        }
    }
}

fn generate_fields_decode(fields: &[syn::Field]) -> Vec<quote::Tokens> {
    let mut result = Vec::new();
    for field in fields {
        let ident = &field.ident;
        let typ =   &field.ty;
        result.push(quote!{
            #ident : <#typ>::decode(r)?,
        });
    }
    result
}
