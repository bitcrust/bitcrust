
#[macro_use]
extern crate encode_derive;

extern crate network_encoding;

use network_encoding::{Encode,Decode};
use std::io::Cursor;

#[derive(Encode)]
struct TestStructWithCount {
    id: u32,
    data: Vec<u8>
}

#[derive(Encode, Decode)]
struct TestStruct {
    id: u32,
    data: [u8; 32]
}

#[test]
fn it_encodes() {
    let t = TestStruct {
        id: 12,
        data: [0x00; 32],
    };
    let mut buff = Cursor::new(vec![0; 15]);

    t.encode(&mut buff).unwrap();
    let res = buff.get_ref();
    assert_eq!(*res, vec![
        // id
        12, 0, 0, 0,
        // data
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]);

    let decoded = TestStruct::decode(&mut Cursor::new(res)).unwrap();
    assert_eq!(decoded.id, 12);
}

#[test]
fn it_encodes_with_count() {
    let t = TestStructWithCount {
        id: 13,
        data: vec![0x00],
    };
    let mut encoded = vec![];
    let _ = t.encode(&mut encoded);
    assert_eq!(encoded, vec![
        // id
        13, 0, 0, 0,
        // len
        1,
        // data
        0]);
}
