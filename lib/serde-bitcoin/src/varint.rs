//!
//! Bitcoin's varint encoding and decoding
//!

use std::io;

#[inline]
pub fn read<R : io::Read>(rdr: &mut R) -> Result<usize, io::Error> {

    let mut byte1 = [0u8];
    rdr.read_exact(&mut byte1)?;
    Ok(match byte1[0] {
        0xff => {
            let mut buf = [0u8; 8];
            rdr.read_exact(&mut buf)?;
            u64::from_le_bytes(buf) as usize
        },
        0xfe => {
            let mut buf = [0u8; 4];
            rdr.read_exact(&mut buf)?;
            u32::from_le_bytes(buf) as usize
        },
        0xfd => {
            let mut buf = [0u8; 2];
            rdr.read_exact(&mut buf)?;
            u16::from_le_bytes(buf) as usize
        },
        _ => byte1[0] as usize
    })
}

#[inline]
pub fn write<W: io::Write>(w: &mut W, v: usize) -> Result<(), io::Error> {
    let buf: Vec<u8> = match v {
        0..=0xfc => {
            vec![v as u8]
        }
        0xfd..=0xffff => {
            [0xfd].iter()
                .chain(u16::to_le_bytes(v as u16).iter())
                .cloned().collect()
        }
        0x10000..=0xffff_ffff => {
            [0xfe].iter()
                .chain(u32::to_le_bytes(v as u32).iter())
                .cloned().collect()
        }
        _ => {
            [0xff].iter()
                .chain(u64::to_le_bytes(v as u64).iter())
                .cloned().collect()
        }
    };
    w.write_all(&buf)
}

#[inline]
pub fn to_bytes(v: usize) -> Vec<u8> {
    let mut buf = Vec::with_capacity(9);
    write(&mut buf, v).unwrap(); 
    buf
}

/*
    We might want to add something like this to allow 
    the attribute #[serde_with="serde_bitcoin::varint"]

pub fn serialize<T,S>(v: &T, serializer: S) -> Result<S::Ok, S::Error>
    where T: Into<u64>, S: serde::Serializer
{
    let v:u64 = v.into();

}
*/
