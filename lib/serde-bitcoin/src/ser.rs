use std::io::{self, Write};

use serde;
use serde::serde_if_integer128;

use crate::error::{Error, Result};
use byteorder::{LittleEndian, BigEndian, WriteBytesExt};

/// An Serializer that encodes values directly into a Writer.
///
pub struct Serializer<W> {
    writer: W,
}

pub fn to_bytes<T: serde::Serialize>(obj: &T) -> Vec<u8> {
    let mut buf = Vec::with_capacity(std::mem::size_of::<T>());

    // We can safely unwrap as our writer is memory
    let mut ser = Serializer { writer: &mut buf };
    obj.serialize(&mut ser)
        .expect("Encoding failed");

    buf
}

pub fn to_writer<T: serde::Serialize, W: io::Write>(obj: &T, w: &mut W) -> Result<()> {
    // We can safely unwrap as our writer is memory
    let mut ser = Serializer { writer: w };
    obj.serialize(&mut ser)
}

fn serialize_varint<W: Write>(w: &mut W, v: usize) -> Result<()> {
    match v {
        0..=0xfc => {
            w.write_u8(v as u8)?;
        }
        0xfd..=0xffff => {
            w.write_u8(0xFD)?;
            w.write_u16::<LittleEndian>(v as u16)?;
        }
        0x10000..=0xffff_ffff => {
            w.write_u8(0xFE)?;
            w.write_u32::<LittleEndian>(v as u32)?;
        }
        _ => {
            w.write_u8(0xFF)?;
            w.write_u64::<LittleEndian>(v as u64)?;
        }
    };
    Ok(())
}

impl<'a, W: Write> serde::Serializer for &'a mut Serializer<W> {
    type Ok = ();
    type Error = Error;
    type SerializeSeq = Self;
    type SerializeTuple = Self;
    type SerializeTupleStruct = Self;
    type SerializeTupleVariant = Self;
    type SerializeMap = Self;
    type SerializeStruct = Self;
    type SerializeStructVariant = Self;

    fn serialize_unit(self) -> Result<()> {
        Ok(())
    }

    fn serialize_unit_struct(self, _: &'static str) -> Result<()> {
        Ok(())
    }

    fn serialize_bool(self, v: bool) -> Result<()> {
        Ok(self.writer.write_u8(if v { 1 } else { 0 })?)
    }

    fn serialize_u8(self, v: u8) -> Result<()> {
        Ok(self.writer.write_u8(v)?)
    }

    fn serialize_u16(self, v: u16) -> Result<()> {
        Ok(self.writer.write_u16::<LittleEndian>(v)?)
    }

    fn serialize_u32(self, v: u32) -> Result<()> {
        Ok(self.writer.write_u32::<LittleEndian>(v)?)
    }

    fn serialize_u64(self, v: u64) -> Result<()> {
        Ok(self.writer.write_u64::<LittleEndian>(v)?)
    }

    fn serialize_i8(self, v: i8) -> Result<()> {
        Ok(self.writer.write_i8(v)?)
    }

    fn serialize_i16(self, v: i16) -> Result<()> {
        Ok(self.writer.write_i16::<LittleEndian>(v)?)
    }

    fn serialize_i32(self, v: i32) -> Result<()> {
        Ok(self.writer.write_i32::<LittleEndian>(v)?)
    }

    fn serialize_i64(self, v: i64) -> Result<()> {
        Ok(self.writer.write_i64::<LittleEndian>(v)?)
    }

    serde_if_integer128! {
        fn serialize_u128(self, v: u128) -> Result<()> {
            Ok(self.writer.write_u128::<BigEndian>(v)?)
        }

        fn serialize_i128(self, v: i128) -> Result<()> {
            Ok(self.writer.write_i128::<LittleEndian>(v)?)
        }
    }

    fn serialize_f32(self, v: f32) -> Result<()> {
        Ok(self.writer.write_f32::<LittleEndian>(v)?)
    }

    fn serialize_f64(self, v: f64) -> Result<()> {
        Ok(self.writer.write_f64::<LittleEndian>(v)?)
    }

    fn serialize_str(self, v: &str) -> Result<()> {
        let v = v.as_bytes();
        self.serialize_bytes(&v)
    }

    fn serialize_char(self, _: char) -> Result<()> {
        unimplemented!()
    }

    fn serialize_bytes(self, v: &[u8]) -> Result<()> {
        serialize_varint(&mut self.writer, v.len())?;
        Ok(self.writer.write_all(v)?)
    }

    fn serialize_none(self) -> Result<()> {
        Ok(self.writer.write_u8(0)?)
    }

    fn serialize_some<T: ?Sized>(self, v: &T) -> Result<()>
    where
        T: serde::Serialize,
    {
        self.writer.write_u8(1)?;
        v.serialize(self)
    }

    fn serialize_seq(self, len: Option<usize>) -> Result<Self::SerializeSeq> {
        let len = len.ok_or(Error::SequenceMustHaveLength)?;
        serialize_varint(&mut self.writer, len)?;
        Ok(self)
    }

    fn serialize_tuple(self, _len: usize) -> Result<Self::SerializeTuple> {
        Ok(self)
    }

    fn serialize_tuple_struct(
        self,
        _name: &'static str,
        _len: usize,
    ) -> Result<Self::SerializeTupleStruct> {
        Ok(self)
    }

    fn serialize_tuple_variant(
        self,
        _name: &'static str,
        variant_index: u32,
        _variant: &'static str,
        _len: usize,
    ) -> Result<Self::SerializeTupleVariant> {
        serialize_varint(&mut self.writer, variant_index as usize)?;
        Ok(self)
    }

    fn serialize_map(self, len: Option<usize>) -> Result<Self::SerializeMap> {
        let len = len.ok_or(Error::SequenceMustHaveLength)?;
        serialize_varint(&mut self.writer, len)?;
        Ok(self)
    }

    fn serialize_struct(self, _name: &'static str, _len: usize) -> Result<Self::SerializeStruct> {
        Ok(self)
    }

    fn serialize_struct_variant(
        self,
        _name: &'static str,
        variant_index: u32,
        _variant: &'static str,
        _len: usize,
    ) -> Result<Self::SerializeStructVariant> {
        serialize_varint(&mut self.writer, variant_index as usize)?;
        Ok(self)
    }

    fn serialize_newtype_struct<T: ?Sized>(self, _name: &'static str, value: &T) -> Result<()>
    where
        T: serde::ser::Serialize,
    {
        value.serialize(self)
    }

    fn serialize_newtype_variant<T: ?Sized>(
        self,
        _name: &'static str,
        variant_index: u32,
        _variant: &'static str,
        value: &T,
    ) -> Result<()>
    where
        T: serde::ser::Serialize,
    {
        serialize_varint(&mut self.writer, variant_index as usize)?;
        value.serialize(self)
    }

    fn serialize_unit_variant(
        self,
        _name: &'static str,
        variant_index: u32,
        _variant: &'static str,
    ) -> Result<()> {
        serialize_varint(&mut self.writer, variant_index as usize)
    }

    fn is_human_readable(&self) -> bool {
        false
    }
}

impl<'a, W> serde::ser::SerializeSeq for &mut Serializer<W>
where
    W: Write,
{
    type Ok = ();
    type Error = Error;

    #[inline]
    fn serialize_element<T: ?Sized>(&mut self, value: &T) -> Result<()>
    where
        T: serde::ser::Serialize,
    {
        value.serialize(&mut **self)
    }

    #[inline]
    fn end(self) -> Result<()> {
        Ok(())
    }
}

impl<'a, W> serde::ser::SerializeTuple for &'a mut Serializer<W>
where
    W: Write,
{
    type Ok = ();
    type Error = Error;

    #[inline]
    fn serialize_element<T: ?Sized>(&mut self, value: &T) -> Result<()>
    where
        T: serde::ser::Serialize,
    {
        value.serialize(&mut **self)
    }

    #[inline]
    fn end(self) -> Result<()> {
        Ok(())
    }
}

impl<'a, W> serde::ser::SerializeTupleStruct for &'a mut Serializer<W>
where
    W: Write,
{
    type Ok = ();
    type Error = Error;

    #[inline]
    fn serialize_field<T: ?Sized>(&mut self, value: &T) -> Result<()>
    where
        T: serde::ser::Serialize,
    {
        value.serialize(&mut **self)
    }

    #[inline]
    fn end(self) -> Result<()> {
        Ok(())
    }
}

impl<'a, W> serde::ser::SerializeTupleVariant for &'a mut Serializer<W>
where
    W: Write,
{
    type Ok = ();
    type Error = Error;

    #[inline]
    fn serialize_field<T: ?Sized>(&mut self, value: &T) -> Result<()>
    where
        T: serde::ser::Serialize,
    {
        value.serialize(&mut **self)
    }

    #[inline]
    fn end(self) -> Result<()> {
        Ok(())
    }
}

impl<'a, W> serde::ser::SerializeMap for &'a mut Serializer<W>
where
    W: Write,
{
    type Ok = ();
    type Error = Error;

    #[inline]
    fn serialize_key<K: ?Sized>(&mut self, value: &K) -> Result<()>
    where
        K: serde::ser::Serialize,
    {
        value.serialize(&mut **self)
    }

    #[inline]
    fn serialize_value<V: ?Sized>(&mut self, value: &V) -> Result<()>
    where
        V: serde::ser::Serialize,
    {
        value.serialize(&mut **self)
    }

    #[inline]
    fn end(self) -> Result<()> {
        Ok(())
    }
}

impl<'a, W> serde::ser::SerializeStruct for &'a mut Serializer<W>
where
    W: Write,
{
    type Ok = ();
    type Error = Error;

    #[inline]
    fn serialize_field<T: ?Sized>(&mut self, _key: &'static str, value: &T) -> Result<()>
    where
        T: serde::ser::Serialize,
    {
        value.serialize(&mut **self)
    }

    #[inline]
    fn end(self) -> Result<()> {
        Ok(())
    }
}

impl<'a, W> serde::ser::SerializeStructVariant for &mut Serializer<W>
where
    W: Write,
{
    type Ok = ();
    type Error = Error;

    #[inline]
    fn serialize_field<T: ?Sized>(&mut self, _key: &'static str, value: &T) -> Result<()>
    where
        T: serde::ser::Serialize,
    {
        value.serialize(&mut **self)
    }

    #[inline]
    fn end(self) -> Result<()> {
        Ok(())
    }
}
