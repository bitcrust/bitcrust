/// Serializes/Deserializes `SocketAddr` in bitcoin network format
///
/// This uses 16 bytes for the ip address (mapping IPv4 addresses to IPv6),
/// and 2 bytes *big-endian* for the port
///
/// This module can be referenced using `#[serde(with = "serde_bitcoin::net_addr")]`
///

use std::net::{self,SocketAddr};

use serde::{Serialize, Deserialize, Serializer, Deserializer};

/// Deserializes a SocketAddr from Bitcoin network format
pub fn deserialize<'de, D>(deserializer: D) -> Result<SocketAddr, D::Error>
where
    D: Deserializer<'de>,
{
    // Deserialize 18 bytes as SocketAddr
    let packed: [u8;18]  = Deserialize::deserialize(deserializer)?;
    let mut ipv6 = [0u8;16];
    ipv6.clone_from_slice(&packed[0..16]);
    let port:u16 = ((packed[16] as u16) << 8) | (packed[17] as u16);
    Ok(SocketAddr::new(net::IpAddr::from(ipv6), port))
}

/// Serializes a SocketAddr in Bitcoin network format
pub fn serialize<S>(addr: &SocketAddr, serializer: S) -> Result<S::Ok, S::Error>
where S: Serializer
{
    // mao ipv4 to ipv6
    let ipv6: net::Ipv6Addr = match addr.ip() {
        net::IpAddr::V4(ip) => ip.to_ipv6_mapped(),
        net::IpAddr::V6(ip) => ip,
    };
    let mut packed = [0u8;18];
    packed[..16].clone_from_slice(&ipv6.octets());
    packed[16] = (addr.port() >> 8) as u8;
    packed[17] = addr.port() as u8;
    packed.serialize(serializer)
}

