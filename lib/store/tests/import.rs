
extern crate bitcrust_store as store;
extern crate network_encoding;

use std::time::Instant;
mod util;
mod blk_file;

use network_encoding::*;

#[test]
#[ignore]
fn test_import() {

    let db = store::init_empty("tst-import").unwrap();
    let mut orphans = std::collections::HashMap::new();
    let now = Instant::now();
    let mut blocks = 0;

    for blk in blk_file::read_blocks() {

        let header = &blk[0..80];
        let txs = &blk[80..];
        let add_result = store::Header::add(&db, header).unwrap();
        match add_result {
            store::AddResult::Orphan { parent: prev_hash, .. } => {
                assert!(orphans.insert(prev_hash, (header.to_vec(),txs.to_vec())).is_none());
            },
            store::AddResult::Exists { .. }=> {
                println!("Header already exists");
            },
            store::AddResult::New (header) => {

                header.add_transactions(&db, txs).unwrap();
                blocks += 1;
                let mut h = header.hash;
                while let Some((orphan_header, txs)) = orphans.remove(&h) {
                    //println!("Adding decendent {} of {}", util::to_hex_rev(&orphan_header.hash[..]), util::to_hex_rev(&hash[..]));
                    if let store::AddResult::New (header) = store::Header::add(&db, &orphan_header).unwrap() {
                        header.add_transactions(&db, &txs).unwrap();
                        blocks += 1;

                        h = header.hash;
                    }
                    else {
                        panic!("insert failed");
                    }

                }


                if blocks == 300000 { break; }
            }
        }


    }

    let elapsed = now.elapsed().as_secs() * 1000 + now.elapsed().subsec_nanos() as u64 / 1000_000;
    let ms_block = elapsed / blocks;
    println!("Processed {} in {} ms,  {} ms/block", blocks, elapsed, ms_block);

    let _ = store::Header::get(&db,
                                &Hash::from("000000000000034a7dedef4a161fa058a2d67a173a90155f3a2fe6fc132e0ebf"))
        .unwrap();


}
