
extern crate network_encoding;

extern crate bitcrust_store as store;
mod util;


#[test]
fn test_empty() {
    use network_encoding::*;
    let mut db = store::init_empty("tst-empty").unwrap();

    // get genesis coinbase tx
    let tx = store::transaction_get(&mut db, &Hash::from(
        "4a5e1e4baab89f3a32518a88c31bc87f618f76673e2cc77ab2127b7afdeda33b")
    ).unwrap().unwrap();

    println!("{:?}", tx);
    assert_eq!(tx.tx.version, 1);
    assert_eq!(tx.tx.outputs[0].value, 50 * 100_000_000);
}

//10mb/sec => 13000