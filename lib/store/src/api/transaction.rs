
use crate::db::*;
use network_encoding::*;
use crate::StoreError;

pub fn transaction_get(db: &Db, hash: &Hash) -> Result<Option<transaction::Transaction>, StoreError> {
    Ok(transaction::Transaction::find(db, hash)?)
}
