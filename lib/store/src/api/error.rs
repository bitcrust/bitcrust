
use crate::verify;
use crate::db;

pub type Result<T> = ::std::result::Result<T, Error>;

#[derive(Debug)]
pub enum Error {

    /* The input data isn't valid */
    VerifyError(verify::VerifyError),

    /* Something is wrong with the store */
    DbError(db::DbError)
}

impl Error {
    pub fn is_verify_error(&self) -> bool {
        match self {
            Error::VerifyError(_) => true,
            _ => false
        }
    }
}

/// Extension for `Result<_,StoreError>` to unwrap errors that are likely unrecoverable
///
/// `StoreError`s wrap either `DbError`s or `VerifyError`s. `DbError`s indicate a bug or data corruption
/// while `VerifyError`s indicate invalid input.
///
/// Importing `ResultExt` allows you to use `unwrap_dberr` to panic only on unrecoverable errors
pub trait ResultExt<T> {
    fn unwrap_dberr(self) -> ::std::result::Result<T, verify::VerifyError>;
}

impl<T> ResultExt<T> for Result<T> {
    fn unwrap_dberr(self) -> ::std::result::Result<T, verify::VerifyError> {
        match self {
            Err(Error::DbError(x)) => panic!(x),
            Err(Error::VerifyError(x)) => Err(x),
            Ok(x) => Ok(x)

        }
    }
}

impl From<db::DbError> for Error {
    fn from(err: db::DbError) -> Error {
        Error::DbError(err)
    }
}


impl From<::std::io::Error> for Error {
    fn from(err: ::std::io::Error) -> Error {
        Error::DbError(db::DbError::from(err))
    }
}


impl From<verify::VerifyError> for Error {
    fn from(err: verify::VerifyError) -> Error {
        Error::VerifyError(err)
    }
}
