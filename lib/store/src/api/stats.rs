//!
//! Database statistics
//!

use std::collections::HashMap;

use crate::db::*;
use crate::StoreError;


/// Retrieves database statistcs.
pub fn stats_get(db: &Db) -> Result<HashMap<String, u64>, StoreError> {
    Ok(db_stats::stats_get(db)?)
}
