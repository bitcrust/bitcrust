

pub mod transaction;

pub mod stats;

pub mod error;

use crate::Hash;

/// Result of adding something to the store
#[derive(Debug)]
pub enum AddResult<T> {
    Exists { hash: Hash },
    Orphan { hash: Hash, parent: Hash },
    New (T),
}

impl<T : ::std::fmt::Debug> AddResult<T>{

    /// Panics if the addresult is not `AddResult::New(T)` and return `T` otherwise
    pub fn expect_new(self) -> T {
        match self {
            AddResult::New (h) => h,
            x => panic!("Unexpected AddResult {:?}", x),
        }
    }
}
