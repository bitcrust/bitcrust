use network_encoding::*;

use crate::{Db,StoreError, AddResult};

use crate::chain;
use crate::db::*;
use crate::verify::*;

use crate::timings::Timings;
use super::header::*;


impl Header {
    /// Add the given set of transactions to the header
    ///
    /// The transactions should be in network-format. They will be decoded and verified against
    /// the merke root of the header.
    ///
    /// If
    pub fn add_transactions(&self, db: &Db, transactions: &[u8]) -> Result<AddResult<()>,StoreError> {

        debug!("Starting add_transactions {}", self.hash);

        let mut timings = Timings::new();

        if self.has_transactions() {
            warn!("Transactions already added");
            return Ok(AddResult::Exists { hash: self.hash } );
        }

        timings.next();
        // Decode the transactions
        let mut txs = transaction::Transaction::decode_set(transactions)?;
        timings.next();

        verify_merkle_root(self, &txs)?;
        timings.next();

        transaction::Transaction::store_set(db, &mut txs)?;
        timings.next();

        let block: Vec<_> = txs.iter().map(|tx| tx.tx_ptr.unwrap()).collect();
        let block_ptr = db.blk.add(&block)?;
        timings.next();

        info!("Block   {:6} with {:5} transactions in {}", self.height, txs.len(), timings.end());

        if !self.parent(db)?.is_connected() {
            self.set_state(db, None, Some(block_ptr), None);
            return Ok(AddResult::Orphan { hash: self.hash, parent: self.parent(db)?.hash } );
        }

        match self.do_connect(db, &block) {
            Err(StoreError::VerifyError(v)) => {

                self.set_state(db, Some(false), None, None);
                Err(StoreError::VerifyError(v))
            },
            Err(e) => Err(e),
            Ok(spend_index_root) => {
                self.set_state(db, Some(true), Some(block_ptr), Some(spend_index_root));
                Ok(AddResult::New(()))
            }
        }
    }
}
