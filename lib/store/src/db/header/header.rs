use std::fmt;
use atomic::{Atomic,Ordering};

use network_encoding::*;
use super::*;
use crate::{Hash, Db, DbError,StoreError};
use crate::verify::*;
use crate::db::{spend_index, block};
use crate::pow::{self,U256};
use crate::chain;
use crate::api::AddResult;




/// The normative fields of the header.
///
/// This is the block-header as used in network propagation
#[derive(Encode,Decode,Debug, Clone, Default)]
pub struct HeaderNorm {
    pub version:     i32,
    pub prev_hash:   Hash,
    pub merkle_root: Hash,
    pub time:        u32,
    pub bits:        u32,
    pub nonce:       u32,
}
pub const FLAG_ASSUME_VALID: u64 = 1;

/// The steps in which we index jumping backwards
const SKIP_STEPS: [u32;4] = [1, 10, 256, 2016];


/// Header is a connected block-header
///
/// After decoding and storing a network with `Header::add()`, headers are
/// always referred to as an immutable reference directly to the memory map in
/// the header file.
///
/// After adding, the header structure is update at most twice.
/// First, the transactions are associated with the header with `Header::add_transactions()`
///
#[derive(Debug, Default)]
#[repr(C)]
pub struct Header {

    pub hash: Hash,

    // Index of the previous header whose hash collides in the hash index
    // (todo: this does not need to be atomic)
    pub(crate) prev_with_hash: Atomic<Option<HeaderIndex>>,

    // Current state of the header
    pub(crate) is_valid: Atomic<Option<bool>>,

    /// Pointer to the Vector of TxPtr's in the `block` file
    /// These are pointers to the transactions contained in the block
    /// None if the transactions haven't been added yet
    block: Atomic<Option<block::BlockPtr>>,

    /// The root page number in the spendindex
    /// None if the block isn't connected yet
    spend_index_root: Atomic<Option<spend_index::PageNumber>>,

    /// Flags defined above FLAGS_*
    flags: u64,

    /// Previous_ptr point to the header at height
    /// 0 => h-1
    /// 1 => (h-1) % 10
    /// 2 => (h-1) % 256
    /// 3 => (h-1) % 2016
    ancestors: [Option<HeaderIndex>; 4],

    // Time the header itself arrived
    pub(crate) network_time: u32,

    /// The height of the header
    pub height: u32,

    // Total amount of work in this block and its ancestors
    pub(crate) acc_work: U256,

    /// Main normative fields used in network propagation
    pub hdr: HeaderNorm,

}

impl Header {

    /// Adds a header
    ///
    /// The header will first be parsed from network format. Then it is validated and connected
    /// to its parent and stored in the database. A reference to the header in the database
    /// is returned.
    ///
    /// If the parent header is not present adding fails and the function returns `AddResult::Orphan`.
    ///
    /// # Examples
    ///
    /// ```rust
    /// use bitcrust_store::*;
    /// use bitcrust_store::builder::*;
    ///
    /// let db = init_empty("test-data/test-add-header").unwrap();
    ///
    /// let hdr1 = Header::add(&db, &header_on(GENESIS)).unwrap().expect_new();
    /// ```
    pub fn add<'a>(db: &'a Db, raw_header: &[u8]) -> Result<AddResult<&'a Header>, StoreError> {

        // See if it's already in
        let hash = Hash::from_data(&raw_header[..]);
        if db.hdr.find(&hash).is_some() {
            return Ok(AddResult::Exists { hash });
        }

        // Parse network-header input
        let hdr: HeaderNorm = decode_buf(raw_header)?;

        // Find parent, or bail if orphan
        let parent = match db.hdr.find(&hdr.prev_hash) {
            Some(p) => p,
            None => return Ok(AddResult::Orphan { hash: hash, parent: hdr.prev_hash})
        };

        // Set skip-list pointers for height based seeking
        let parent_ptr = db.hdr.index_of(parent);
        let previous_ptr = [
            Some(parent_ptr),
            if (parent.height % SKIP_STEPS[1]) == 0 { Some(parent_ptr) } else { parent.ancestors[1] },
            if (parent.height % SKIP_STEPS[2]) == 0 { Some(parent_ptr) } else { parent.ancestors[2] },
            if (parent.height % SKIP_STEPS[3]) == 0 { Some(parent_ptr) } else { parent.ancestors[3] }
        ];

        // Calculate accumulated work
        let target   = pow::from_compact(hdr.bits);
        let work     = pow::difficulty_target_to_work(target);

        // Construct
        let header = Header {
            hash          : hash,
            ancestors     : previous_ptr,
            height        : parent.height + 1,
            network_time  : get_time_as_u32(),
            acc_work      : parent.acc_work + work,
            hdr,
            flags: FLAG_ASSUME_VALID,
            ..Default::default()
        };

        // Verification
        verify_proof_of_work(&header)?;
        verify_work_target(db, &header)?;
        verify_network_time(&header)?;
        verify_version(&header)?;
        verify_median_time(db, &header)?;

        // Store and switch to reference of stored object
        let header = db.hdr.add(header)?;

        Ok(AddResult::New(header))
    }

    /// Returns a new genesis header
    pub(crate) fn new_genesis() -> Header {

        Header {
            hash: Hash::from_data(chain::GENESIS),
            spend_index_root: Atomic::new(Some(spend_index::GENESIS_ROOT_PAGE)),
            block: Atomic::new(Some(block::BlockPtr::GENESIS)),
            hdr: decode_buf(chain::GENESIS).expect("Invalid hardcoded genesis"),
            acc_work: U256::from(0x0000_0001_0001_0001u64),
            ..Default::default()
        }
    }

    /// Find and return the header with the given hash, or return `None` if it doesn't exist
    pub fn get<'a>(db: &'a Db, hash: &Hash) -> Option<&'a Header> {
        db.hdr.find(hash)
    }

    /// Retuns the genesis header
    pub fn get_genesis(db: &Db) -> &Header {
        db.hdr.get(HeaderIndex::GENESIS)
    }

    /// Retuns the fully validated header with the highest amount of accumulated work
    pub fn get_best(db: &Db) -> &Header {
        db.hdr.get_best()
    }

    /// Returns the header with the most accumulated work, considering headers without transactions
    /// as well
    pub fn get_best_non_validated(db: &Db) -> &Header {
        db.hdr.get_best_non_validated()
    }

    /// Returns the main header fields in network format
    pub fn as_raw(&self) -> Vec<u8> {
        encode_buf(&self.hdr)
    }

    /// Returns the child header leading towards the header with the most accumulated work
    /// None if this isn't the main branch, or if this is the tip
    pub(crate) fn best_child(&self, db: &Db) -> Option<&Header> {
        todo!()
        /*match self.state.load(Ordering::Relaxed) {
            HeaderState::MainBranch { next: Some(idx) } => Some(db.hdr.get(idx)),
            _ => None,
        }*/
    }

    /// Returns the parent or None if self is genesis
    pub(crate) fn try_parent<'a,D>(&self, db: D) -> Option<&'a Header>
        where D :  Into<&'a HeaderFile>
    {
        self.ancestors[0].map(|idx| db.into().get(idx))
    }

    /// Returns the parent or a db error if self is genesis
    pub fn parent<'a,F>(&self, file: F) -> Result<&'a Header, DbError>
        where F :  Into<&'a HeaderFile>
    {
        self.try_parent(file).ok_or(DbError::ParentNotFound)
    }

    /// Returns the header that is an ancestor of `self` at the given height.
    ///
    /// Returns None if the requested height is too large
    ///
    /// # Examples
    ///
    /// ```rust
    ///
    /// use bitcrust_store::*;
    /// use bitcrust_store::builder;
    ///
    /// let db = init_empty("test-data/test-header_get_by_height").unwrap();
    ///
    /// let genesis = Header::get_best(&db);
    /// assert!(genesis.ancestor_at(&db,0).is_some());
    /// assert!(genesis.ancestor_at(&db,1).is_none());
    ///
    /// ```
    pub fn ancestor_at(&self, db: &Db, height: u32) -> Option<&Header> {
        if height == self.height {
            return Some(self);
        } else if height > self.height {
            return None;
        }

        let mut current = self;
        let mut skip = SKIP_STEPS.len()-1; // Start with large jumps
        loop {
            let jump_height = (current.height - 1) - ((current.height -1) % SKIP_STEPS[skip]);
            if jump_height > height {
                // jump SKIP_STEP[skip] backwards
                current = db.hdr.get(current.ancestors[skip].unwrap());
            }
            else if jump_height < height {
                debug_assert!(skip!=0);
                skip-=1;
            } else {
                // found target
                current = db.hdr.get(current.ancestors[skip].unwrap());
                debug_assert_eq!(current.height, height);
                return Some(current);
            }
        }
    }

    /// Returns the next header on insertion order
    pub(crate) fn next_inserted<'a,F>(&self, file: F) -> Option<&'a Header>
        where F :  Into<&'a HeaderFile>
    {
        file.into().next_inserted(self)
    }

    /// Iterate over the ancestors, starting with self, ending with genesis
    pub fn iter_ancestors<'a>(&'a self, db: &'a Db) -> impl Iterator<Item = &'a Header> {
        HeaderIter {
            db,
            current: Some(self),
            direction: HeaderIteratorDirection::Backward,
        }
    }

    /// Iterate over headers that do not yet have their transactions loaded
    pub fn iter_needs_transactions<'a>(db: &'a Db) -> impl Iterator<Item = &'a Header> {

        db.hdr.get_last_with_txs(db)
            .iter_next_inserted(db)
            .filter(|hdr| !hdr.has_transactions())
    }

    /// Iterate over headers that need to be connected
    /// This can include headers that also need to be downloaded
    ///
    /// Note that when looking for a header to connect, one needs to do a lookahead,
    /// to prevent one (no-transactions) sidechain block from blocking further connecting
    pub fn iter_needs_connection<'a>(db: &'a Db) -> impl Iterator<Item = &'a Header> {

        db.hdr.get_last_connected(db)
            .iter_next_inserted(db)
            .filter(|hdr| !hdr.is_connected() && !hdr.is_invalid())
    }

    /// Iterate forward, starting with self, in insertion order
    ///
    /// Insertion order guarantees that a header is preceeded by all ancestors,
    /// but it does not guarantee that a header is followed by its child; it may also
    /// be followed by (side-branch) siblings
    pub fn iter_next_inserted<'a>(&'a self, db: &'a Db) -> impl Iterator<Item = &'a Header> {
        HeaderIter {
            db,
            current: Some(self),
            direction: HeaderIteratorDirection::ForwardInserted,
        }
    }

    /// Returns the median of the block times of *n* last blocks (including self)
    pub fn median_time(&self, db: &Db, n: u32) -> u32 {

        // Take previous n hdr.times
        let mut times: Vec<_> = self.iter_ancestors(db)
                .take(n as usize)
                .map(|hdr| hdr.hdr.time)
                .collect();

        // Sort and take median
        times.sort_unstable();
        debug_assert!(times.len()>0);
        if times.len() == 1 { let t = times[0]; times.push(t); };
        (times[times.len()/2] + times[(times.len()-1)/2]) / 2

    }

    /// Constructs a locator object for this header
    ///
    /// This consists of the block hash and at most 32 hashes ancestor hashes,
    /// ending in Genesis, starting dense but more sparse near genesis
    ///
    /// This is used so that peers can provide the headers towards the best chain,
    /// even if we are only aware of a side chain. This set can locate the most recent
    /// common ancestor
    pub fn locator(&self, db: &Db) -> Vec<Hash> {

        let mut hashes = vec![self.hash];
        let mut step_size = 1;
        let mut hdr = self;
        while hashes.len() < 32 && hdr.height > step_size {

            hdr = hdr.ancestor_at(db, hdr.height - step_size).unwrap();
            hashes.push(hdr.hash);
            if hashes.len() > 10 {
                step_size *= 2;
            }
        }
        hashes.push(Hash::from_data(chain::GENESIS));
        hashes
    }


    /// Returns the block ptr containing all `TxPtr`s in the block
    /// or returns a `DbError` if the block isn't loaded yet
    pub(crate) fn get_block_ptr(&self) -> Result<block::BlockPtr, DbError> {
        self.block.load(Ordering::Relaxed).ok_or(DbError::ObjectNotFound)
    }

    pub(crate) fn get_spend_index_root(&self) -> Option<spend_index::PageNumber> {
        self.spend_index_root.load(Ordering::Relaxed)
    }

    /// Returns true if the transactions of the header are loaded
    ///
    /// This does not mean the header is *connected* yet, as the transactions may be loaded
    /// out of order
    pub fn has_transactions(&self) -> bool {
        self.block.load(Ordering::Relaxed).is_some()
    }

    /// Returns true if the transactions of the header are loaded and verified to be valid at
    /// this point in the blockchain
    pub fn is_connected(&self) -> bool {
        self.spend_index_root.load(Ordering::Relaxed).is_some()
    }

    /// Returns true if the transactions of this header are invalid. If this returns false,
    /// the transactions are either valid or still not verified
    pub fn is_invalid(&self) -> bool {
        !self.is_valid.load(Ordering::Relaxed).unwrap_or(true)
    }

    /// Returns the set of transactions of this header as locally valid `TxPtr`s, or `None` if
    /// the transactions are not yet loaded
    pub fn get_transactions(&self, db: &Db) -> Result<Option<Vec<crate::db::transaction::TxPtr>>,DbError> {
        match self.block.load(Ordering::Relaxed) {
            None => Ok(None),
            Some(block_ptr) => {
                Ok(Some(db.blk.get(block_ptr)?))
            }
        }
    }

    /// Returns `true` if this header is known to be valid at compile time, and therefor has not been
    /// and will not be validated.
    pub fn assume_valid(&self) -> bool {
        self.flags & FLAG_ASSUME_VALID == FLAG_ASSUME_VALID
    }

    pub fn accumulated_work(&self) -> U256 {
        self.acc_work
    }


    pub(crate) fn has_more_acc_work(&self, other: &Header) -> bool {
        self.acc_work > other.acc_work ||
            (self.acc_work == other.acc_work && self.network_time < other.network_time)
    }


    pub(crate) fn set_state(&self, db: &Db, is_valid: Option<bool>,
                     block_ptr: Option<block::BlockPtr>,
                     spend_index_root: Option<spend_index::PageNumber>)
    {
        if self.is_valid.compare_exchange_weak(None, is_valid, Ordering::Relaxed, Ordering::Relaxed).is_err() {
            warn!("Is valid is already set for {}", self.hash);
        }
        if !is_valid.unwrap_or(true)  {
            return;
        }

        if block_ptr.is_some() {

            if self.block.compare_exchange_weak(None, block_ptr, Ordering::Relaxed, Ordering::Relaxed).is_err() {
                warn!("Block-ptr already set for header {}", self.hash);
            }
            db.hdr.update_has_txs();
        }

        if spend_index_root.is_some() {

            db.hdr.update_best_block(self);
            if self.spend_index_root.compare_exchange_weak(None, spend_index_root, Ordering::Relaxed, Ordering::Relaxed).is_err() {
                warn!("Spend-index already set for header {}", self.hash);
            }
        }
    }

}



/// Implements an iterator over headers
pub(crate) struct HeaderIter<'a> {
    db: &'a Db,
    current: Option<&'a Header>,
    direction: HeaderIteratorDirection,
}

enum HeaderIteratorDirection {
    _ForwardToTip,
    Backward,
    ForwardInserted,
}

impl<'a> Iterator for HeaderIter<'a> {
    type Item = &'a Header;

    fn next(&mut self) -> Option<&'a Header> {
        let current = self.current;
        self.current = self.current.and_then(|header| match self.direction {
            HeaderIteratorDirection::Backward => header.try_parent(self.db),
            HeaderIteratorDirection::_ForwardToTip => unimplemented!(),  //header.best_child(self.db),
            HeaderIteratorDirection::ForwardInserted => header.next_inserted(self.db),
        });

        current
    }
}

impl fmt::Display for Header {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "HDR @ {} [{}]", self.height, self.hash)
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use std::mem;

    #[test]
    fn test_size() {
        assert!(mem::size_of::<Header>() % 8 == 0);
        assert_eq!(mem::size_of::<Result<Option<HeaderIndex>,Option<HeaderIndex>>>(), 8);
    }


}
