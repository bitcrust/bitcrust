
mod header;
mod header_file;
mod header_connect;
mod header_transactions;
mod header_iter;

#[cfg(test)]
mod header_tests;


pub use self::header::{Header,HeaderNorm};
pub use self::header_file::{HeaderFile,HeaderIndex};



fn get_time_as_u32() -> u32 {
    let network_time = ::std::time::SystemTime::now()
        .duration_since(::std::time::UNIX_EPOCH)
        .expect("System time error");

    if  network_time.as_secs() + 60*60*60 > u32::max_value() as u64 {
        panic!("System time overflows 32-bit")
    } else {
        network_time.as_secs() as u32
    }
}
