/// The header file stores all headers in an append only list.
///
/// It uses a root hash table to access by hash, and simple "prev_with_hash" pointers to resolve
/// collisions
///
/// The file is memory mapped for fast access


use std::{path,fs,mem};
use std::collections::HashMap;
use std::num::NonZeroU32;
use memmap;
use atomic::{Atomic,Ordering};
use network_encoding::Hash;
use crate::{Db,DbError};
use super::header::Header;
use super::super::data_file::*;

const FILE_HEADER_SIZE: u64 = mem::size_of::<DataFileHeader>() as u64;
const INDEX_SIZE: u64       = mem::size_of::<HeaderIndex>() as u64 * ( 1<< HASH_INDEX_BITS);


/// Persistent identifier for a stored header
/// This is not the height, although it coincides during IBD
#[derive(Debug,Copy,Clone,PartialEq,Eq)]
pub struct HeaderIndex(NonZeroU32);

impl HeaderIndex {
    pub const GENESIS: Self = HeaderIndex(unsafe { NonZeroU32::new_unchecked(1) });

    /// Returns the next sequentual index or None if we are at the end
    pub fn next(&self, write_pos: u64) -> Option<HeaderIndex> {
        let next = HeaderIndex(unsafe { NonZeroU32::new_unchecked(self.0.get() + 1) });
        if next.to_file_pos() >= write_pos {
            None
        } else {
            Some(next)
        }
    }

    fn to_file_pos(&self) -> u64 {

        self.0.get() as u64 * mem::size_of::<Header>() as u64
            + FILE_HEADER_SIZE + INDEX_SIZE
    }

    fn from_file_pos(file_offset: u64) -> HeaderIndex {
        // convert the file offset to an index in self.headers

        const FILE_HEADER_SIZE: u64 = mem::size_of::<DataFileHeader>() as u64;
        const INDEX_SIZE: u64       = mem::size_of::<HeaderIndex>() as u64 * ( 1<< HASH_INDEX_BITS);
        const HEADER_SIZE: u64      = mem::size_of::<Header>() as u64;

        let offset_in_headers = file_offset - FILE_HEADER_SIZE - INDEX_SIZE;
        debug_assert!(offset_in_headers % HEADER_SIZE == 0);

        let header_index = offset_in_headers / HEADER_SIZE;
        HeaderIndex(unsafe { NonZeroU32::new_unchecked(header_index as u32) })
    }
}

impl From<usize> for HeaderIndex {
    fn from(n: usize) -> Self { HeaderIndex(unsafe { NonZeroU32::new_unchecked(n as u32) })}
}

impl Into<u32> for HeaderIndex {
    fn into(self) -> u32 { self.0.get() }
}

impl From<HeaderIndex> for usize {
    fn from(n: HeaderIndex) -> usize { n.0.get() as usize }
}



// The number of bits used for the hash index; giving 1<<20 index entries
const HASH_INDEX_BITS: usize = 20;

/// The `HeaderFile` contains all the headers and its hash-index
pub struct HeaderFile {

    rw_file:     fs::File,
    data_file_header: &'static DataFileHeader,

    _mmap:             memmap::Mmap,
    _mmap_file_header: memmap::Mmap,

    hash_index:  &'static [Atomic<Option<HeaderIndex>>],
    headers:     &'static [Header],

    // aggregates

    // Total headers
    count:         &'static Atomic<u64>,

    // Pointer to the header with validated transactions with the most accumulated work
    best_block:    &'static Atomic<HeaderIndex>,

    // Pointer with the header with the most accumulated work; could be without validated txs
    best_header:   &'static Atomic<HeaderIndex>,

    validated:     &'static Atomic<u64>,
    has_txs:       &'static Atomic<u64>,
    last_with_txs: &'static Atomic<HeaderIndex>,

    last_connected: &'static Atomic<HeaderIndex>,

}

// We fix the maximum header count to prevent growing memmaps
const MAX_HEADER_COUNT: usize = 2_000_000;


impl DataFile for HeaderFile {

    /// Opens or creates the header file at the given location
    fn new<P : AsRef<path::Path>>(file_name: P) -> Result<Self, DbError>
    {

        const FILE_HEADER_SIZE: usize = mem::size_of::<DataFileHeader>();
        const INDEX_SIZE: usize = mem::size_of::<HeaderIndex>() * ( 1<< HASH_INDEX_BITS);
        const DATA_SIZE: usize = mem::size_of::<Header>() * MAX_HEADER_COUNT;
        const HEADER_SIZE: usize = mem::size_of::<Header>();

        let file_name = file_name.as_ref();
        let is_new = !file_name.exists();
        if  is_new {
            // We initialize with space for the index and one header
            // This is to ensure the first header gets written at index 1, as HeaderIndex is NonZero
            init_file(file_name, INDEX_SIZE + HEADER_SIZE)?;
        }

        let rw_file = fs::OpenOptions::new().read(true).write(true).open(&file_name)?;

        // Setup memory maps

        let (_mmap_file_header, data_file_header) = map_header(&rw_file)?;

        let _mmap = memmap::Mmap::open_with_offset(&rw_file,
                memmap::Protection::ReadWrite,
                FILE_HEADER_SIZE,
                DATA_SIZE + INDEX_SIZE        )?;

        let index_ptr =  _mmap.ptr() as *const Atomic<Option<HeaderIndex>>;
        let hash_index = unsafe { ::std::slice::from_raw_parts(index_ptr, 1 << HASH_INDEX_BITS) };

        let data_ptr =  unsafe { _mmap.ptr().offset(INDEX_SIZE as isize) as *const Header };
        let headers = unsafe { ::std::slice::from_raw_parts(data_ptr, MAX_HEADER_COUNT) };

        // Setup and initialize aggregates
        let count = data_file_header.init_aggregate(0, 0);
        let validated = data_file_header.init_aggregate(1, 0);
        let has_txs = data_file_header.init_aggregate(2, 1);
        let best_block = data_file_header.init_aggregate32(0, HeaderIndex::GENESIS);
        let best_header = data_file_header.init_aggregate32(1, HeaderIndex::GENESIS);
        let last_with_txs = data_file_header.init_aggregate32(2, HeaderIndex::GENESIS);
        let last_connected = data_file_header.init_aggregate32(3, HeaderIndex::GENESIS);

        let result = HeaderFile {
            rw_file,
            _mmap, _mmap_file_header,
            data_file_header, hash_index, headers,
            count, best_block, best_header, validated
            ,has_txs, last_with_txs, last_connected
        };

        if is_new {
            let genesis = result.add(Header::new_genesis())?;

            assert_eq!(result.index_of(genesis), HeaderIndex::GENESIS);

            let _ = result.validated.fetch_add(1, Ordering::Relaxed);
        }
        Ok(result)
    }

    fn stats(&self) -> HashMap<String,u64> {

        vec![
            ("count".to_owned(),       self.count.load(Ordering::Relaxed) as u64),
            ("validated".to_owned(),   self.validated.load(Ordering::Relaxed) as u64),
            ("best_block".to_owned(),  usize::from(self.best_block.load(Ordering::Relaxed)) as u64),
            ("best_header".to_owned(),  usize::from(self.best_header.load(Ordering::Relaxed)) as u64),
            ("has_txs".to_owned(),     self.has_txs.load(Ordering::Relaxed) as u64),
            ("last_with_txs".to_owned(),usize::from(self.last_with_txs.load(Ordering::Relaxed)) as u64),
        ].into_iter().collect()

    }

}


impl HeaderFile {

    /// Stores the header in the header file and adds it to the hash_index
    pub fn add(&self, header: Header) -> Result<&Header, DbError> {

        let file_offset = write_object(&self.rw_file, &header, &self.data_file_header)?;
        let header_index = HeaderIndex::from_file_pos(file_offset);

        self.add_to_hash_index(header_index);

        self.count.fetch_add(1, Ordering::Relaxed);

        // We get a new reference to the header as stored
        let result = self.get(header_index);
        self.update_best_header(result);
        Ok(result)
    }

    /// Returns the index of the passed header.
    /// Note that this is NOT the height
    pub fn index_of(&self, header: &Header) -> HeaderIndex {
        let index = (header as *const _ as usize - self.headers.as_ptr() as usize)
            / std::mem::size_of::<Header>();
        HeaderIndex::from(index)
    }

    /// Returs the header directly following the given header
    ///
    /// This may not me a child of `header`, but it is garanteed not to be an ancestor
    pub fn next_inserted(&self, header: &Header) -> Option<&Header> {

        let candidate = self.index_of(header)
            .next(self.data_file_header.write_pos.load(Ordering::Relaxed))
            .map(|idx| self.get(idx))?;

        // We check here if we have the correct one;
        // It could be that two headers were added simultaniously, and we'll need the index one
        if let Some(c) = self.find(&candidate.hash) {
            if self.index_of(c) == self.index_of(candidate) {
                return Some(candidate);
            }
        }
        warn!("Skipping unindexed header");
        self.next_inserted(candidate)
    }

    /// Find a header by its hash
    pub fn find(&self, hash: &Hash) -> Option<&Header> {

        let idx = get_hash_index(hash);
        let mut header_index = self.hash_index[idx].load(Ordering::Relaxed);

        // loop over linked list of value-objects at idx
        while let Some(h) = header_index {
            let header = self.get(h);
            if header.hash == *hash {
                return Some(header);
            }

            header_index = header.prev_with_hash.load(Ordering::Relaxed);
        }
        None
    }

    /// Returns the header with the most accumulated work
    pub fn get_best(&self) -> &'static Header {

        self.get(self.best_block.load(Ordering::Relaxed))
    }

    /// Returns the header with the most accumulated work, considering headers without transactions
    /// as well
    pub fn get_best_non_validated(&self) -> &'static Header {

        self.get(self.best_header.load(Ordering::Relaxed))
    }

    /// Returns the first header (sequentially) that is not yet connected
    pub fn get_last_connected<'a,'b>(&'a self, db: &'a Db) -> &'a Header {

        // update it first
        let new_connected = self.get(self.last_connected.load(Ordering::Relaxed))
            .iter_next_inserted(db)
            .take_while(|hdr| hdr.is_connected() || hdr.is_invalid())
            .last()
            // can't happen because of GENESIS; just panic
            .expect("No connected headers");

        self.last_connected.store(self.index_of(new_connected), Ordering::Relaxed);
        new_connected
    }


    /// Returns the first header (sequentially) that does not yet have txs
    pub fn get_last_with_txs<'a,'b>(&'a self, db: &'a Db) -> &'a Header {

        // update it first
        let new_last_with_txs = self.get(self.last_with_txs.load(Ordering::Relaxed))
            .iter_next_inserted(db)
            .take_while(|hdr| hdr.has_transactions())
            .last()
            // can't happen because of GENESIS; just panic
            .expect("No headers with loaded transactions");

        self.last_with_txs.store(self.index_of(new_last_with_txs), Ordering::Relaxed);
        new_last_with_txs
    }


    /// Gets a header by its index
    ///
    /// Panics if the index is out-of-bounds
    pub fn get(&self, index: HeaderIndex) -> &'static Header {
        let index: usize = index.into();
        assert!(index < self.headers.len(),
                format!("Header file overflow. Seeking {} len={}", index, self.headers.len()));

        &self.headers[index]
    }


    /// Update aggregates to reflect that the given header has transactions
    pub fn update_has_txs(&self) {
        self.has_txs.fetch_add(1, Ordering::Relaxed);
    }

    fn update_next_pointers(&self, _best_tip: &Header, _not_best_tip: &Header) {
        // TODO (needed?)

    }
    
    /// Check if the given header is new best (most accumulated work), and update accordingly
    pub fn update_best_header(&self, header: &Header) {
        loop {
            let old_best_idx = self.best_header.load(Ordering::Relaxed);
            let old_best = self.get(old_best_idx);
            if old_best.has_more_acc_work(header) {
                return;
            }
            if self.best_header.compare_exchange(old_best_idx, self.index_of(header), Ordering::Relaxed, Ordering::Relaxed).is_ok() {
                return;
            }
        }
    }

    /// Check if the given header is new best (most accumulated work), and update accordingly
    pub fn update_best_block(&self, header: &Header) {

        let mut maybe_best = header;

        // Compare-and-swap loop
        // A bit special one; as we are updating the chain of next pointers,
        // we must make sure that if our original best is updated during the loop,
        // we must update these next pointers *even* if our new maybe_best isn't good enough!
        // This way we ensure that next pointers that are no longer of the main chain, are also update
        loop {
            let old_best_idx = self.best_block.load(Ordering::Relaxed);
            let old_best = self.get(old_best_idx);

            let (new_best, new_not_best) = if maybe_best.has_more_acc_work(old_best) {
                (maybe_best,old_best)
            } else {
                (old_best,maybe_best)
            };

            self.update_next_pointers(new_best, new_not_best);

            if self.best_block.compare_exchange(old_best_idx, self.index_of(new_best), Ordering::Relaxed, Ordering::Relaxed).is_ok() {
                return;
            } else {
                maybe_best = old_best;
            }
        }
    }

    fn add_to_hash_index(&self, header_idx: HeaderIndex) {
        let header = self.get(header_idx);
        let root_index = get_hash_index(&header.hash);

        // Compare and swap loop to update the hash table without locks
        loop {
            let old_header_idx = self.hash_index[root_index].load(Ordering::Relaxed);

            header.prev_with_hash.store(old_header_idx, Ordering::Relaxed);

            if self.hash_index[root_index].compare_exchange_weak
                (old_header_idx, Some(header_idx), Ordering::Relaxed, Ordering::Relaxed).is_ok()
            {
                return;
            }
        }
    }
}


// Returns the index of the key in the hash index
fn get_hash_index(key: &Hash) -> usize {
    let key = key.into_inner();
    let bits32 = ((key[0] as usize) << 24) |
        ((key[1] as usize) << 16) |
        ((key[2] as usize) << 8) |
        ((key[3] as usize));

    bits32 >> (32 - HASH_INDEX_BITS)
}
