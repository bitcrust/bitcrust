//! Block connect procedure
//!
//! This is where it is verified if the transactions of a header are valid at the header's
//! location in the blockchain, by adding them to the spend index

use network_encoding::*;
use std::sync::atomic;
use std::mem;
use network_encoding::*;
use rayon::prelude::*;


use crate::{Db,DbError,StoreError, AddResult};
use crate::db::transaction::{Transaction,TxPtr,TxCached};
use crate::db::header::Header;
use crate::db::spend_index;
use crate::verify::*;
use crate::timings::Timings;
use super::header::*;


impl Header {
    /// Connects the transactions of the header to the blockchain
    ///
    /// This normally happens on `Header::add_transactions()`, but during catch up or initial block
    /// download, these may happen out-of-order and return `AddResult::Orphan`. For these headers,
    /// this method needs to be called when the parent header is connected.
    pub fn connect(&self, db: &Db) -> Result<(), StoreError> {
        let txs: &[TxPtr] = &self.get_transactions(db)?.ok_or(StoreError::from(DbError::ObjectNotFound))?;

        if !self.parent(db)?.is_connected() {
            return Err(DbError::ParentNotFound.into());
        }

        match self.do_connect(db, &txs) {
            Err(StoreError::VerifyError(v)) => {
                error!("VerifyError {:?} on block {}", v, self.height);
                self.set_state(db, Some(false), None, None);
                Err(StoreError::VerifyError(v))
            },
            Err(e) => Err(e),
            Ok(spend_index_root) => {
                
                self.set_state(db, Some(true), None, Some(spend_index_root));
                Ok(())
            }
        }
    }

    /// This is the actual connect procedure that connects two blocks
    pub(crate) fn do_connect(&self, db: &Db, txs: &[TxPtr]) -> Result<spend_index::PageNumber, StoreError> {

        debug!("Starting connect of {}", self.height);

        let assume_valid = self.assume_valid();
        let mut timings = Timings::new_at(5);

        let tx_count = txs.len();

        let txs = &mut Transaction::index_set(db, txs)?;
        if !assume_valid {
            verify_block_order(txs)?;
        }
        timings.next();

        // add pointers to spend outputs
        let tx_cached = if assume_valid {
            fetch_spends_no_verify(db, txs)?
        } else {
            fetch_spends(db, self, txs)?
        };

        timings.next();

        // Update and verify spend index
        let (spend_bits_store, spend_bits_check): (Vec<u64>, Vec<u64>) = get_spend_bits(&tx_cached, get_matured_coinbase(db, self)?);
        let spend_bits_check = if assume_valid { vec![] } else { spend_bits_check };
        timings.next();

        let prev_root = self.parent(db)?.get_spend_index_root().unwrap();
        let spend_index_root = db.spn.add_block(&spend_bits_store, &spend_bits_check, prev_root)?
            .ok_or(VerifyError::BlockDoubleSpent)?;

        timings.next();
        if timings.get_ms() > 1000 {
            warn!("Connect SLOW {:6} with {:5} transactions in {}", self.height, tx_count, timings.end());
        }
        else {
            info!("Connect {:6} with {:5} transactions in {}", self.height, tx_count, timings.end());
        }

        Ok(spend_index_root)
    }
}



/// Fetch the spend outputs for each transaction, but does not verify the scripts
fn fetch_spends_no_verify(db: &Db, txs: &[Transaction]) -> Result<Vec<TxCached>, StoreError> {

    let txs: Result<Vec<TxCached>, StoreError> = txs
        .par_iter()
        .skip(1) // skip coinbase
        .map(|tx| {
            // find outputs only
            tx.resolve_spends(db, true)?
                .ok_or(StoreError::from(VerifyError::PrevOutNotFound))

        })
        .collect();

    txs
}


/// Fetch spend outputs from the db, and fully verify
fn fetch_spends(db: &Db, hdr: &Header, txs: &mut [Transaction]) -> Result<Vec<TxCached>, StoreError>  {

    // We gather some aggregate data in the process
    let min_script_flags = atomic::AtomicU32::new(crate::ScriptVerifyFlags::verify_max().raw());
    let sum_fees = atomic::AtomicI64::new(0);
    let sum_size = atomic::AtomicUsize::new(0);
    let sum_sig_ops = atomic::AtomicUsize::new(0);


    let txs_cached: Result<Vec<TxCached>, StoreError> = txs
        .par_iter()
        // skip coinbase
        .skip(1)
        .map(|tx| -> Result<TxCached, StoreError> {

            // Output fetching and script verification
            let tx_cached = tx.resolve_spends(db, false)?
                .ok_or(StoreError::from(VerifyError::PrevOutNotFound))?;

            verify_transaction_fee(tx_cached.fee)?;
            verify_transaction(tx)?;

            verify_locktime(db, &tx_cached, hdr)?;
            verify_sequence(db, &tx_cached, hdr)?;

            sum_fees.fetch_add(tx_cached.fee, atomic::Ordering::Relaxed);
            sum_size.fetch_add(tx.meta.size as usize, atomic::Ordering::Relaxed);
            sum_sig_ops.fetch_add(tx_cached.sig_ops as usize, atomic::Ordering::Relaxed);
            min_script_flags.fetch_and(tx_cached.min_verify, atomic::Ordering::Relaxed);

            Ok(tx_cached)
        })

        .collect();

    let txs_cached = txs_cached?;

    // block-level tx validation

    let min_verify_flags = crate::ScriptVerifyFlags::from_raw(min_script_flags.into_inner());
    verify_min_script_flags(&hdr, min_verify_flags)?;

    verify_coinbase(&hdr, &txs[0], sum_fees.into_inner())?;

    // Block size = compact_size(tx_count) + sum(tx_size) + header_size
    verify_block_size(encode_buf(&txs.len()).len() + sum_size.into_inner() + mem::size_of::<crate::db::header::HeaderNorm>())?;

    Ok(txs_cached)
}



/// Gets the coinbase that is becoming spendable at this header
fn get_matured_coinbase(db: &Db, header: &Header) -> Result<Option<TxPtr>, DbError> {

    if header.height > 100 {
        // Here header.height>99 would logical, but we must skip block #0 as it is unspendable
        let matured_block = header.ancestor_at(db, header.height - 100).unwrap();
        Ok(Some(db.blk.get_first(matured_block.get_block_ptr()?)?))
    }
    else {
        Ok(None)
    }
}


/// Get the spend bits from the transaction set
///
/// Returns two vectors; the first one needs to be stored in the spend_index,
/// the second one only needs to be checked for existence
fn get_spend_bits(txs: &[TxCached], matured_coinbase: Option<TxPtr>) -> (Vec<u64>, Vec<u64>) {

    // Spend bits that are stored in the spend index, failing if they already exist
    // these represent the new transactions in the block and the outputs that are spend
    let mut spend_bits_store: Vec<u64> = Vec::new();

    // Spend bits that are verified for existence. These are the transactions for the outputs being spend
    let mut spend_bits_check: Vec<u64> = Vec::new();

    if let Some(ptr) = matured_coinbase {
        spend_bits_store.push(ptr.as_spend_bit_transaction())
    }

    // Might want to rewrite as par_iter; fast enough for now
    for tx in txs.iter() {
        spend_bits_store.push(tx.tx_ptr.unwrap().as_spend_bit_transaction());
        debug_assert_ne!(tx.inputs.len(), 0);
        for inp in tx.inputs.iter() {

            spend_bits_store.push(inp.out_ptr.as_spend_bit_output(inp.out_idx));
            spend_bits_check.push(inp.out_ptr.as_spend_bit_transaction());
        }
    }
    debug!("store spendbits={}", spend_bits_store.len());

    debug_assert!(spend_bits_store.len() >= txs.len()*2);

    (spend_bits_store, spend_bits_check)
}
