use super::*;
use atomic::Atomic;
use crate::builder::*;
use crate::Db;


#[test]
fn test_sizes() {
    // Check if nonzero nicely wrappes around to option
    struct M {
        _f: Atomic<Option<HeaderIndex>>,
    }
    assert_eq!(::std::mem::size_of::<M>(), 4);
}

fn add_header<'a>(db: &'a Db, header: &[u8]) -> &'a Header {
    let hdr = Header::add(&db, header).unwrap().expect_new();
    hdr
}

#[test]
fn test_header_genesis() {
    let db = crate::db::init_empty("test-data/test_header_genesis").unwrap();
    let _hdr1 = add_header(&db, &header_on(crate::chain::GENESIS));
}


#[test]
fn test_iter() {

    let db = crate::db::init_empty("test-data/test_iter2").unwrap();

    let hdr1 = add_header(&db, &header_on(crate::chain::GENESIS));
    let hdr2 = add_header(&db, &header_on(&hdr1.as_raw()));
    let hdr3 = add_header(&db, &header_on(&hdr2.as_raw()));

    assert_eq!(hdr3.iter_ancestors(&db).skip(1).next().unwrap().hash, hdr2.hash);
    assert_eq!(hdr3.iter_ancestors(&db).skip(2).next().unwrap().hash, hdr1.hash);
/*
    let back2 = backwards(&db, hdr3.get_prev_ptr()).skip(1).next().unwrap().unwrap();
    assert_eq!(forwards(&db, back2.0).skip(1).next().unwrap().unwrap().1.get_hash(),
               hdr2.get_hash());
*/
}

#[test]
fn test_height() {
    let db = crate::init_empty("test-data/test_height").unwrap();
    let hdr1 = add_header(&db, &header_on(crate::chain::GENESIS));
    let hdr2 = add_header(&db, &header_on(&hdr1.as_raw()));
    let hdr3a = add_header(&db, &header_on(&hdr2.as_raw()));
    let hdr3b = add_header(&db, &header_on(&hdr2.as_raw()));

    assert_eq!(hdr1.height, 1);
    assert_eq!(hdr2.height, 2);
    assert_eq!(hdr3a.height, 3);
    assert_eq!(hdr3b.height, 3);
}


/*
#[test]
fn test_median() {
    let db = ::init_empty("test-data/test_median").unwrap();
    let hdr1 = ::Header::add(&db, &header_on(::chain::GENESIS)).unwrap().unwrap();
    let hdr2 = ::Header::add(&db, &header_on_stored(&hdr1)).unwrap().unwrap();
    let hdr3 = ::Header::add(&db, &header_on_stored(&hdr2)).unwrap().unwrap();

    assert_eq!(hdr3.median_time(&db, 3).unwrap(), hdr2.get_time(), "Median of 3 should be #2");

    assert_eq!(hdr2.median_time(&db, 3).unwrap(), hdr1.get_time(), "Median of 3 from #2 should still be #1");
    assert_eq!(hdr2.median_time(&db, 11).unwrap(), hdr1.get_time(), "Median of 11 from #2 should still be #1");
}

*/
