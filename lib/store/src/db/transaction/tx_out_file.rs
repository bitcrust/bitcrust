use crate::{DbError};
use network_encoding::*;
use std::collections::HashMap;
use std::{fs,path};
use crate::chain;
use super::super::data_file::*;
use atomic::{Atomic,Ordering};
use super::*;

// The number of bits used for the hash index; giving 1<<26 index entries
const HASH_INDEX_BITS: usize = 26;

const FILE_HEADER_SIZE: usize = mem::size_of::<DataFileHeader>();
const INDEX_SIZE: usize = mem::size_of::<TxPtr>() * ( 1<< HASH_INDEX_BITS);


pub struct TxOutFile {
    rw_file:     fs::File,
    data_file_header: &'static DataFileHeader,

    _mmap:             memmap::Mmap,
    _mmap_file_header: memmap::Mmap,

    hash_index:  &'static [Atomic<Option<TxPtr>>],

    // aggregators
    transactions: &'static Atomic<u64>,
    outputs:      &'static Atomic<u64>,
    pages:        &'static Atomic<u64>,
}


/// Persistent pointer to a transaction
///
/// This points to the tx_out_file; in there a pointer to the corresponding tx_in_file record is found
#[derive(Debug,PartialEq,Eq,Copy,Clone)]
pub struct TxPtr(NonZeroU64);

impl TxPtr {
    pub fn from_file_pos(file_pos: u64) -> Self {
        const START_OF_DATA: usize = INDEX_SIZE + FILE_HEADER_SIZE;
        unsafe {
            TxPtr(NonZeroU64::new_unchecked(
                ((file_pos - START_OF_DATA as u64) / 32) + 1
            ))
        }
    }

    pub fn to_file_pos(self) -> u64 {
        const START_OF_DATA: usize = INDEX_SIZE + FILE_HEADER_SIZE;
        (self.0.get() - 1) * 32 + START_OF_DATA as u64
    }

    pub const GENESIS: Self = TxPtr(unsafe { NonZeroU64::new_unchecked(1) });

    /// Convert to a small unique number to store in the spend index
    pub fn as_spend_bit_transaction(self) -> u64 {
        self.0.get()
    }

    /// Convert to a small unique number to store in the spend index
    pub fn as_spend_bit_output(self, index: u32) -> u64 {
        self.0.get() + 1 + index as u64
    }
}


impl From<NonZeroU64> for TxPtr {
    fn from(n: NonZeroU64) -> Self {
        TxPtr(n)
    }
}
impl Into<NonZeroU64> for TxPtr {
    fn into(self) -> NonZeroU64{
        self.0
    }
}

impl DataFile for TxOutFile {

    fn new<P : AsRef<path::Path>>(file_name: P) -> Result<Self, DbError> {
        let file_name = file_name.as_ref();
        let is_new = !file_name.exists();

        const FILE_HEADER_SIZE: usize = mem::size_of::<DataFileHeader>();
        const INDEX_SIZE: usize = mem::size_of::<TxPtr>() * ( 1<< HASH_INDEX_BITS);

        if  is_new {
            init_file(file_name, INDEX_SIZE)?;
        }

        let rw_file = fs::OpenOptions::new().read(true).write(true).open(&file_name)?;

        // Setup memory map to header
        let (_mmap_file_header, data_file_header) = map_header(&rw_file)?;

        let _mmap = memmap::Mmap::open_with_offset(&rw_file,
                   memmap::Protection::ReadWrite,
                   FILE_HEADER_SIZE,
                   INDEX_SIZE
        )?;

        // .. and to index
        let index_ptr =  _mmap.ptr() as *const Atomic<Option<TxPtr>>;
        let hash_index = unsafe { ::std::slice::from_raw_parts(index_ptr, 1 << HASH_INDEX_BITS) };

        // shortcuts to aggregators
        let transactions = &data_file_header.aggregates[0];
        let outputs = &data_file_header.aggregates[1];
        let pages = &data_file_header.aggregates[2];

        let result = TxOutFile {
            rw_file, _mmap_file_header, _mmap, data_file_header,
            hash_index,
            transactions, outputs, pages
        };
        if is_new {
            let mut genesis: Transaction = decode_buf(chain::GENESIS_TX)?;
            genesis.set_meta(chain::GENESIS_TX);
            genesis.meta.tx_in_ptr = TxInPtr::GENESIS;

            let batch = &mut [genesis];
            result.add_batch(batch)?;
            result.index_batch(&mut [batch[0].tx_ptr.unwrap()])?;
            assert_eq!(batch[0].tx_ptr, Some(TxPtr::GENESIS));

        }
        Ok(result)
    }

    fn stats(&self) -> HashMap<String,u64> {
        vec![
            ("transactions".to_owned(), self.transactions.load(Ordering::Relaxed)),
            ("outputs".to_owned(),      self.outputs.load(Ordering::Relaxed)),
            ("pages".to_owned(),        self.pages.load(Ordering::Relaxed)),
        ].into_iter().collect()
    }
}




impl TxOutFile {


    /// Adds the set of transactions to the tx_out store,
    /// and updates the Transaction's TxPtr's
    ///
    /// This DOES NOT add it to the index
    ///
    /// This is for IBD-performance. We want to be able to store blocks ahead, without
    /// messing with the index, as if we would index them here, it would destroy the performace
    /// of our MRU collision resolution.
    pub fn add_batch(&self, txs: &mut [Transaction]) -> Result<(), DataFileError> {


        // Pack the outputs, and encode to bytes in parallel
        let packed_outputs: Vec<_> = txs
            .par_iter_mut()
            .map(|tx| {
                let packed = PackedOutputs::new(&tx.tx.outputs);
                self.outputs.fetch_add(tx.tx.outputs.len() as u64, Ordering::Relaxed);
                tx.meta.page_count = packed.pages();
                Self::to_storage(&tx.meta, &packed)
            })
            .collect();

        // Gather byte-offsets from start and collect to single buffer
        let mut buffer: Vec<u8> = Vec::with_capacity(txs.len() * 32 * 3);
        let mut offsets: Vec<u64> = Vec::with_capacity(txs.len());
        let mut cur_offset = 0;
        for tx_buf in packed_outputs {
            offsets.push(cur_offset);
            cur_offset += tx_buf.len() as u64;
            buffer.extend(tx_buf.into_iter());
        }

        let file_pos = write(&self.rw_file, &buffer, &self.data_file_header)?;

        // Generate the resulting txptr using the base file_pos and offsets
        txs
            .par_iter_mut()
            .zip(offsets.par_iter())
            .for_each(|(tx,offset)| tx.tx_ptr = Some(TxPtr::from_file_pos(file_pos + offset)));

        Ok(())
    }

    /// Indexes a set of transaction previously added
    pub fn index_batch(&self, txs: &[TxPtr]) -> Result<Vec<Transaction>, DbError> {

        let txs: Result<Vec<_>,_> = txs
            .into_par_iter()
            .map(|tx_ptr| -> Result<_,_> {
                let mut meta = self.get_meta(*tx_ptr)?;

                // Index in root hash table
                let root_index = get_hash_index(&meta.hash);

                if meta.prev_with_hash.is_some() {
                    warn!("Attempting indexing twice");
                    return self.get(*tx_ptr, Some(meta));
                }
                // Compare and swap loop to update the hash table without locks
                loop {
                    let old_ptr = self.hash_index[root_index].load(Ordering::Relaxed);
                    if old_ptr == Some(*tx_ptr) {
                        warn!("Attempting indexing twice");
                        break;
                    }

                    // try storing, point to previous collision
                    meta.prev_with_hash = old_ptr;
                    let encoded_meta = unsafe { ::std::slice::from_raw_parts(
                        (&meta as *const TxMetadata) as *const u8, 64) };
                    write_at(&self.rw_file, &encoded_meta, tx_ptr.to_file_pos())?;

                    if self.hash_index[root_index].compare_exchange_weak
                        (old_ptr, Some(*tx_ptr), Ordering::Relaxed, Ordering::Relaxed).is_ok()
                    {
                        // ok; update counters
                        self.transactions.fetch_add(1, Ordering::Relaxed);
                        self.pages.fetch_add(meta.page_count as u64, Ordering::Relaxed);

                        break;
                    }
                }

                // TODO not necessary with assume_valid
                //self.get(*tx_ptr, Some(meta))
                Ok(Transaction {
                    tx_ptr: Some(*tx_ptr),
                    meta,
                    tx: TransactionNorm {
                        outputs: vec![],

                        version: 0,
                        lock_time: 0,
                        inputs: vec![],
                    }
                })

            })
            .collect();
        Ok(txs?)
    }


    pub fn find(&self, hash: &Hash) -> Result<Option<(TxPtr,TxMetadata)>, DbError> {
        let idx = get_hash_index(hash);

        // find entry in root hash table
        let mut tx_ptr = self.hash_index[idx].load(Ordering::Relaxed);

        // loop over linked list of value-objects at idx
        while let Some(ptr) = tx_ptr {
            let meta = self.get_meta(ptr)?;
            if meta.hash == *hash {
                return Ok(Some((ptr, meta)));
            }

            tx_ptr = meta.prev_with_hash;
        }
        Ok(None)
    }


    /// Reads the outputs for this tx_ptr. The tx_ptr must point to the start (meta data)
    pub fn get_outputs(&self, tx_ptr: TxPtr, meta: &TxMetadata) -> Result<Vec<TxOutput>, DataFileError> {
        let file_pos = tx_ptr.to_file_pos() + 64;

        let outputs_buffer = read(&self.rw_file, file_pos, 32 * meta.page_count as u64)?;
        let packed = PackedOutputs::from_storage(&outputs_buffer);
        Ok(packed.unpack())
    }

    /// Loads the tx_out_file portion of a transaction from the database
    pub fn get(&self, tx_ptr: TxPtr, meta: Option<TxMetadata>) -> Result<Transaction, DbError> {
        let meta = match meta {
            None => self.get_meta(tx_ptr)?,
            Some(meta) => meta
        };

        let outputs = self.get_outputs(tx_ptr, &meta)?;
        Ok(Transaction {
            tx_ptr: Some(tx_ptr),
            meta,
            tx: TransactionNorm {
                outputs,

                version: 0,
                lock_time: 0,
                inputs: vec![],
            }
        })
    }

    /// Retrieves a TxMetadata from the file
    pub fn get_meta(&self, tx_ptr: TxPtr) -> Result<TxMetadata, DataFileError> {
        let file_pos = tx_ptr.to_file_pos();

        let meta_buf = read(&self.rw_file, file_pos, 64)?;
        let meta: TxMetadata = unsafe { std::ptr::read(meta_buf.as_ptr() as *const _) };
        Ok(meta)
    }


    /// Encodes to tx_out storage format
    fn to_storage(meta: &TxMetadata, packed_outputs: &PackedOutputs) -> Vec<u8> {

        let encoded_meta = unsafe { ::std::slice::from_raw_parts(
            (meta as *const TxMetadata) as *const u8, 64) };

        let encoded_outputs = packed_outputs.to_storage();

        let mut result: Vec<u8> = Vec::with_capacity(encoded_outputs.len() + encoded_meta.len());
        result.extend_from_slice(encoded_meta);
        result.extend_from_slice(encoded_outputs);
        result

    }

}

// Returns the index of the key in the hash index
fn get_hash_index(key: &Hash) -> usize {
    let key = key.into_inner();
    let bits32 = ((key[0] as usize) << 24) |
        ((key[1] as usize) << 16) |
        ((key[2] as usize) << 8) |
        ((key[3] as usize));

    bits32 >> (32 - HASH_INDEX_BITS)
}
