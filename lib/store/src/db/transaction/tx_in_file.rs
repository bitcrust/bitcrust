use std::collections::HashMap;
use std::{io, fs,path,mem};
use std::io::Seek;
use atomic::{Atomic,Ordering};

use crate::chain;
use crate::{DbError};
use network_encoding::*;
use crate::db::data_file::*;
use super::*;

/// TxInFile stores the transaction inputs (signatures)
/// These are stored separately from
pub struct TxInFile {
    rw_file:     fs::File,
    data_file_header: &'static DataFileHeader,

    _mmap:             memmap::Mmap,

    // aggregators
    transactions: &'static Atomic<u64>,
    inputs:       &'static Atomic<u64>,
}

// Persistent pointer
#[derive(Debug,PartialEq,Eq,Copy,Clone,Default)]
pub struct TxInPtr(u64);

impl TxInPtr {
    pub fn from_file_pos(file_pos: u64) -> Self { TxInPtr(file_pos) }
    pub fn to_file_pos(self) -> u64 { self.0 }
    pub const NULL: Self = TxInPtr(0);
    pub const GENESIS: TxInPtr = TxInPtr(mem::size_of::<DataFileHeader>() as u64);
}

impl DataFile for TxInFile {
    fn new<P : AsRef<path::Path>>(file_name: P) -> Result<Self, DbError> {
        let file_name = file_name.as_ref();
        let is_new = !file_name.exists();

        if  is_new {
            init_file(file_name, 0)?;
        }

        let rw_file = fs::OpenOptions::new().read(true).write(true).open(&file_name)?;

        // Setup memory map to header
        let (_mmap, data_file_header) = map_header(&rw_file)?;

        // shortcuts to aggregators
        let transactions = &data_file_header.aggregates[1];
        let inputs = &data_file_header.aggregates[0];

        let result = TxInFile {
            rw_file, _mmap, data_file_header,
            transactions, inputs
        };
        if is_new {
            let mut genesis: Transaction = decode_buf(chain::GENESIS_TX)?;
            result.add(&mut genesis)?;
            assert_eq!(genesis.meta.tx_in_ptr, TxInPtr::GENESIS);
        }
        Ok(result)
    }

    fn stats(&self) -> HashMap<String,u64> {
        vec![
            ("transactions".to_owned(), self.transactions.load(Ordering::Relaxed)),
            ("inputs".to_owned(), self.inputs.load(Ordering::Relaxed)),
        ].into_iter().collect()
    }
}


impl TxInFile {

    /// Adds the inputs,version,locktime to the tx_in_file , updating the pointer in tx
    pub fn add(&self, tx: &mut Transaction) -> Result<(), DbError> {
        let inputs = tx.tx.inputs.len();
        let mut cur = io::Cursor::new(Vec::with_capacity(150));
        Self::to_storage(tx, &mut cur)?;

        let file_pos = write(&self.rw_file, cur.get_ref(), &self.data_file_header)?;

        // update counters
        let _ = self.transactions.fetch_add(1, Ordering::Relaxed);
        let _ = self.inputs.fetch_add(inputs as u64, Ordering::Relaxed);

        // save pointer
        tx.meta.tx_in_ptr = TxInPtr::from_file_pos(file_pos);
        Ok(())
    }

    pub fn add_batch(&self, txs:&mut [Transaction]) -> Result<(), DbError> {
        let mut input_count = 0;
        let tx_count = txs.len();

        let mut offsets: Vec<u64> = Vec::with_capacity(tx_count);
        let mut cur = io::Cursor::new(Vec::with_capacity(150 * tx_count));
        for tx in txs.iter() {
            offsets.push(cur.position());
            Self::to_storage(tx, &mut cur)?;
            input_count += tx.tx.inputs.len();
        }

        let file_pos = write(&self.rw_file, cur.get_ref(), &self.data_file_header)?;

        // update counters
        let _ = self.transactions.fetch_add(tx_count as u64, Ordering::Relaxed);
        let _ = self.inputs.fetch_add(input_count as u64, Ordering::Relaxed);

        // change relative offsets to file positions
        for n in 0..tx_count {
            txs[n].meta.tx_in_ptr = TxInPtr::from_file_pos(offsets[n] + file_pos);
        }
        Ok(())
    }

    fn to_storage(tx: &Transaction, cur: &mut io::Cursor<Vec<u8>>) -> Result<(), DbError> {

        let start = cur.position();
        // leave space for length-prefix
        encode(&0u64, cur)?;
        encode(&tx.tx.version, cur)?;
        encode(&tx.tx.lock_time, cur)?;
        encode(&tx.tx.inputs, cur)?;

        // align
        while cur.position() % 8 != 0 { encode(&0u8, cur)?; }

        // length prefix
        let pos = cur.position();
        let len = pos - start;
        cur.seek(io::SeekFrom::Start(start))?;
        encode(&len, cur)?;
        cur.seek(io::SeekFrom::Start(pos))?;
        Ok(())
    }

    /// Amends the passed transaction data, with the data in tx_in
    pub fn get(&self, tx_start_ptr: TxInPtr, tx: &mut Transaction) -> Result<(), DbError> {
        let file_pos = tx_start_ptr.to_file_pos();

        // we read 1kb; usually enough
        let mut buffer = read(&self.rw_file, file_pos, 1024)?;
        let len: u64 = decode_buf(&buffer[0..8])?;

        if len > 1024 {
            // read more if needed
            buffer.extend_from_slice(&read(&self.rw_file, file_pos + 1024, len as u64 - 1024)?);
        }

        let mut cur = io::Cursor::new(buffer);
        let _: u64 = decode(&mut cur)?;
        tx.tx.version = decode(&mut cur)?;
        tx.tx.lock_time = decode(&mut cur)?;
        tx.tx.inputs = decode(&mut cur)?;

        Ok(())
    }
}
