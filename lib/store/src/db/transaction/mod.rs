//! Defines the Transaction struct and its relatives
//!
//!
use std::{io,mem};
use std::num::NonZeroU64;
use super::data_file::*;
use network_encoding::*;
use crate::{Db,DbError};
use rayon::prelude::*;

mod tx_out_file;
mod tx_in_file;
mod packed_output;
mod bulk_decode;

use self::packed_output::PackedOutputs;
pub use self::tx_out_file::{TxPtr,TxOutFile};
pub use self::tx_in_file::{TxInPtr,TxInFile};



// There are a few stages we can have a transaction in
//
// Hash
// Transaction

// TransactionNorm => all normative fields; result of decoding

// decode(&[u8]) =>                      (TransactionNorm, size, hash) = Transaction
// store(TransactionNorm, TxMeta) =>     (TxPtr, TxMeta)

// resolve(TransactionNorm) -> TxCached

// index(TxCached, Hash)

//

#[derive(Debug)]
pub struct Transaction {
    pub tx_ptr: Option<TxPtr>,
    pub meta: TxMetadata,
    pub tx: TransactionNorm,
}

#[derive(Debug,Decode,Encode,Default)]
pub struct TransactionNorm {
    pub version:     u32,           // Only valid if inputs are loaded
    pub inputs:      Vec<TxInput>,  // Zero-length if the inputs aren't loaded
    pub outputs:     Vec<TxOutput>, // Zero-length if the outputs aren't loaded
    pub lock_time:   u32,           // Only valid if inputs are loaded
}

#[derive(Debug)]
pub struct TxInputCached {
    pub out_ptr: TxPtr,
    pub out_idx: u32,
    pub sequence: u32,

}

#[derive(Debug)]
pub struct TxCached {
    pub tx_ptr: Option<TxPtr>,
    pub fee: i64,
    pub sig_ops: u32,
    pub min_verify: u32,  // TODO use typed scriptverifyflags
    pub lock_time: u32,
    pub inputs: Vec<TxInputCached>,
}

#[derive(Debug,Encode,Decode)]
pub struct TxInput {
    pub prev_tx_out: Hash,
    pub prev_tx_out_idx: u32,
    pub script: Vec<u8>,
    pub sequence: u32,
}

/// Meta data stored in tx_out
#[derive(Debug,Default)]
pub struct TxMetadata {

    pub page_count: u32, // packed pages in tx_out file
    pub hash: Hash,
    pub prev_with_hash: Option<TxPtr>, // previous collision on hash index
    pub tx_in_ptr: TxInPtr, // pointer in tx_in file
    pub size: u32,
    pub _reserved: u64,
}

#[derive(Debug,Encode,Decode)]
pub struct TxOutput {
    pub value:     i64,
    pub pk_script: Vec<u8>
}

impl TxCached {
    pub fn new(tx: &Transaction) -> TxCached {
        TxCached {
            lock_time: tx.tx.lock_time,
            tx_ptr: tx.tx_ptr,
            inputs: Vec::with_capacity(tx.tx.inputs.len()),
            fee: 0,
            sig_ops: 0,
            min_verify: 0,
        }
    }
}

impl Decode for Transaction {

    /// Decode from network format
    ///
    /// The caller must set hash and size
    fn decode<R: io::Read>(r: &mut R) -> Result<Self, io::Error> {
        Ok(Transaction {
            tx_ptr: None,
            meta: Default::default(),
            tx: decode(r)?,
        })

    }
}

impl Encode for Transaction {

    /// Encode from network format
    fn encode<W: io::Write>(&self, w: &mut W) -> Result<(), io::Error> {
        self.tx.encode(w)
    }
}

impl TxMetadata {

    /// Initialize meta data with tx hash & size
    fn new(tx_data: &[u8]) -> TxMetadata {
        TxMetadata {
            hash: Hash::from_data(&tx_data),
            size: tx_data.len() as u32,
            ..Default::default()
        }
    }
}

impl Transaction {

    pub fn find(db: &Db, hash: &Hash) -> Result<Option<Transaction>, DbError> {
        if let Some((tx_ptr, meta)) = db.tx_out.find(hash)? {
            let mut tx = db.tx_out.get(tx_ptr, Some(meta))?;

            db.tx_in.get(tx.meta.tx_in_ptr, &mut tx)?;
            Ok(Some(tx))
        } else {
            Ok(None)
        }
    }

    /// Initalize the size and hash fields of the `tx.meta` structure, from the raw blob
    pub fn set_meta(&mut self, tx_data: &[u8]) {
        self.meta = TxMetadata::new(tx_data);
    }

    pub fn get(db: &Db, tx_ptr: TxPtr) -> Result<Transaction, DbError> {
        let mut tx = db.tx_out.get(tx_ptr, None)?;

        db.tx_in.get(tx.meta.tx_in_ptr, &mut tx)?;
        Ok(tx)
    }

    pub(crate) fn decode_set(buffer: &[u8]) -> Result<Vec<Transaction>, DbError> {

        // Iterate, this will return (tx,a,b) pairs where a,b are start and end offsets in the buffer
        let txs: Result<Vec<_>, _> = bulk_decode::iter_decode(buffer)?.collect();

        // Find hashes (using set_meta) in parallel
        let txs = txs?.into_par_iter().map(|(tx, a, b)| {
            let mut tx = tx;
            tx.set_meta(&buffer[a..b]);
            tx
        }).collect();

        Ok(txs)
    }

    pub fn store_set(db: &Db, txs: &mut [Transaction]) -> Result<(), DbError> {
        db.tx_in.add_batch(txs)?;
        db.tx_out.add_batch(txs)?;
        Ok(())
    }

    pub fn index_set(db: &Db, txs: &[TxPtr]) -> Result<Vec<Transaction>, DbError> {
        let mut txs = db.tx_out.index_batch(txs)?;

        for mut tx in txs.iter_mut() {
            db.tx_in.get(tx.meta.tx_in_ptr, &mut tx)?;
        }

        Ok(txs)
    }

    /// Finds the pointers to the transactions being spend
    /// and returns them as TxCached *if* all outputs could be found
    pub fn resolve_spends(&self, db: &Db, assume_valid: bool) -> Result<Option<TxCached>, DbError> {
        assert!(self.tx.inputs.len() > 0);

        let mut min_verify_flags = crate::ScriptVerifyFlags::verify_max();
        let mut result = TxCached::new(self);
        let tx_data = if !assume_valid { encode_buf(self) } else { vec![] };

        for (input,tx_in) in self.tx.inputs.iter().enumerate() {
            assert!(!tx_in.prev_tx_out.is_null());

            if let Some((ptr, meta)) = db.tx_out.find(&tx_in.prev_tx_out)? {
                if !assume_valid {
                    let outputs = db.tx_out.get_outputs(ptr, &meta)?;
                    let output_index = tx_in.prev_tx_out_idx as usize;

                    let script_verify_result = bitcoinconsensus_sys::verify_script(
                        &outputs[output_index].pk_script,
                        outputs[output_index].value as u64,
                        &tx_data,
                        input as u32)?;

                    min_verify_flags = min_verify_flags & script_verify_result;
                    result.fee += outputs[output_index].value;
                }
                result.inputs.push(TxInputCached {
                    out_ptr: ptr,
                    out_idx: tx_in.prev_tx_out_idx,
                    sequence: tx_in.sequence,
                });
            } else {
                return Ok(None);
            }
        }

        if !assume_valid {
            for tx_out in self.tx.outputs.iter() {
                result.fee -= tx_out.value;
            }
            result.min_verify = min_verify_flags.raw();
        }
        Ok(Some(result))
    }
}


#[cfg(test)]
mod tests {
    use super::*;


    #[test]
    fn test_le_transmute() {
        let n: u32 = 1;
        let buf: [u8;4] = unsafe { ::std::mem::transmute(n) };
        assert_eq!(buf[0],1, "Big-endian host is not supported");
    }

    #[test]
    fn test_size() {
        assert_eq!(::std::mem::size_of::<TxMetadata>(), 64);
    }
}
