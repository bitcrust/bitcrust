//! This module packs outputs in to a set of 32-byte pages
//!
//! Although we call it "packing" and we do compress the script, the result
//! isn't actually smaller.
//!
//! The purpose is, to be able to directly address outputs without having to decode the entire
//! transaction. Page n will contain output n. If the output doesn't fit on the page, it will be
//! added to pages at the end.
//!
//! This way, we can find output n for a transaction at TxPtr at (TxPtr+n+2).


use super::TxOutput;

#[derive(Debug)]
pub struct PackedOutputs(Vec<[u8;32]>);


const PACKET_OUTPUT_NULL: [u8;32] = [0;32];

const ID_P2PKH: u8 = 0x01;
const ID_P2SH: u8 = 0x02;
const ID_OTHER: u8 = 0x03;

// Extra data pages is where the script of ID_OTHER outputs is stored
const ID_EXTRA_DATA: u8 = 0x04;


enum PackedScript<'a> {
    P2PKH(&'a [u8]),
    P2SH(&'a [u8]),
    Uncompressed(&'a [u8]),
}

// Script compression compresses p2sh and p2pkh outputs to fit in 32-bytes
fn compress_script(script: &[u8]) -> PackedScript {
    const OP_DUP: u8 = 0x76;
    const OP_HASH160: u8 = 0xa9;
    const OP_EQUAL: u8 = 0x87;
    const OP_EQUALVERIFY: u8 = 0x88;
    const OP_CHECKSIG: u8 = 0xac;
    if script.len() == 25 && script[0] == OP_DUP && script[1] == OP_HASH160 &&
        script[2] == 20 && script[23] == OP_EQUALVERIFY &&
        script[24] == OP_CHECKSIG
    {
        PackedScript::P2PKH(&script[3..23])

    } else if script.len() == 23 && script[0] == OP_HASH160 && script[1] == 20 &&
        script[22] == OP_EQUAL
    {
        PackedScript::P2SH(&script[2..22])

    } else {
        PackedScript::Uncompressed(&script)
    }

}

impl PackedOutputs {

    /// Packs the set of outputs to PackedOutputs
    ///
    /// This doesn't make it smaller. It compresses the outputs but this gets undone by padding
    /// The benefit is that after packing, you can directly access output n as index n of the resulting vec
    ///
    /// This absolves the need of decoding entire TxEnd's when looking up an output
    pub fn new(outputs: &Vec<TxOutput>) -> Self {

        // Start with 1 page per output
        let mut result: Vec<[u8;32]> = vec![PACKET_OUTPUT_NULL; outputs.len()];

        for output_n in 0..outputs.len() {

            // bytes 1-3 is index
            result[output_n][1] = output_n as u8;
            result[output_n][2] = (output_n >> 8) as u8;
            result[output_n][3] = (output_n >> 16) as u8;

            // bytes 24-31 is amount
            let amount = outputs[output_n].value;
            for n in 0..8 {
                result[output_n][24+n] = (amount >> (n*8)) as u8;
            }

            match compress_script(&outputs[output_n].pk_script) {
                PackedScript::P2PKH(hash) => {
                    // byte 0 is id
                    result[output_n][0] = ID_P2PKH;

                    // bytes 4-24 is hash
                    for n in 0..20 {
                        result[output_n][n+4] = hash[n];
                    }
                },
                PackedScript::P2SH(hash) => {
                    // byte 0 is id
                    result[output_n][0] = ID_P2SH;

                    // bytes 4-24 is hash
                    for n in 0..20 {
                        result[output_n][n+4] = hash[n];
                    }
                },
                PackedScript::Uncompressed(script) => {
                    // byte 0 is id
                    result[output_n][0] = ID_OTHER;

                    // bytes 4-8 is len
                    let len = script.len();
                    result[output_n][4] = len as u8;
                    result[output_n][5] = (len >> 8) as u8;
                    result[output_n][6] = (len >> 16) as u8;
                    result[output_n][7] = (len >> 24) as u8;

                    // bytes 8-12 is relative page offset
                    let script_page = result.len();
                    result[output_n][8] = script_page as u8;
                    result[output_n][9] = (script_page >> 8) as u8;
                    result[output_n][10] = (script_page >> 16) as u8;
                    result[output_n][11] = (script_page >> 24) as u8;


                    // now at pages chunked at 32-bytes
                    for chunk in script.chunks(31) {
                        let mut p = PACKET_OUTPUT_NULL;
                        p[0] = ID_EXTRA_DATA;
                        for n in 0..chunk.len() { p[n+1] = chunk[n]; }
                        result.push(p)
                    }
                }
            }
        }
        PackedOutputs(result)
    }


    pub fn pages(&self) -> u32 {
        self.0.len() as u32
    }


    /// Unpacks the outputs to `TxOutput` structs
    pub fn unpack(&self) -> Vec<TxOutput> {

        // OP codes needed to recognize common outputs
        const OP_DUP: u8 = 0x76;
        const OP_HASH160: u8 = 0xa9;
        const OP_EQUAL: u8 = 0x87;
        const OP_EQUALVERIFY: u8 = 0x88;
        const OP_CHECKSIG: u8 = 0xac;

        let mut result: Vec<TxOutput> = Vec::new();

        for packed_output in self.0.iter()
        {

            match packed_output[0] {
                ID_P2PKH => {
                    /* matching with  script.len() == 25 && script[0] == OP_DUP && script[1] == OP_HASH160 &&
                            script[2] == 20 && script[23] == OP_EQUALVERIFY &&
                            script[24] == OP_CHECKSIG */

                    let mut script = vec![OP_DUP, OP_HASH160, 20];
                    script.extend_from_slice(&packed_output[4..24]);
                    script.push(OP_EQUALVERIFY);
                    script.push(OP_CHECKSIG);

                    result.push(TxOutput {
                        pk_script: script,
                        value: get_amount(&packed_output),
                    });
                },
                ID_P2SH => {
                    /* matching with script.len() == 23 && script[0] == OP_HASH160 && script[1] == 20 &&
                        script[22] == OP_EQUAL */

                    let mut script = vec![OP_HASH160, 20];
                    script.extend_from_slice(&packed_output[4..24]);
                    script.push(OP_EQUAL);

                    result.push(TxOutput {
                        pk_script: script,
                        value: get_amount(&packed_output),
                    });
                },
                ID_OTHER => {
                    let mut len: u32 = packed_output[4] as u32 |
                            ((packed_output[5] as u32) << 8) |
                            ((packed_output[6] as u32) << 16) |
                            ((packed_output[7] as u32) << 23);

                    let mut page: u32 = packed_output[8] as u32 |
                        ((packed_output[9] as u32) << 8) |
                        ((packed_output[10] as u32) << 16) |
                        ((packed_output[11] as u32) << 23);

                    let mut script: Vec<u8> = Vec::with_capacity(len as usize);
                    while len > 0 {
                        let n = ::std::cmp::min(31,len) as usize;
                        script.extend_from_slice(&self.0[page as usize][1..1+n]);
                        page += 1;
                        len -= n as u32;
                    }
                    result.push(TxOutput {
                        pk_script: script,
                        value: get_amount(&packed_output),
                    });
                },
                ID_EXTRA_DATA => {
                    break;
                }
                _ => panic!("Corrupted packed output")
            }

        }
        result
    }

    pub fn to_storage(&self) -> &[u8] {
        unsafe { ::std::slice::from_raw_parts(
            (&self.0).as_ptr() as *const u8,
            32 * self.0.len())
        }
    }


    pub fn from_storage(buffer: &[u8]) -> Self {

        debug_assert!(buffer.len() % 32 == 0);
        let mut result: Vec<[u8;32]> = Vec::with_capacity(buffer.len()/32);

        for chunk in buffer.chunks(32) {
            let mut ar = [0u8;32];
            ar.copy_from_slice(chunk);
            result.push(ar);
        }
        PackedOutputs(result)
    }

}




fn get_amount(output: &[u8;32]) -> i64 {
    output[24] as i64 |
        ((output[25]  as i64) << 8 ) |
        ((output[26]  as i64) << 16 ) |
        ((output[27]  as i64) << 24 ) |
        ((output[28]  as i64) << 32 ) |
        ((output[29]  as i64) << 40 ) |
        ((output[30]  as i64) << 48 ) |
        ((output[31]  as i64) << 56 )
}


/// Unpacks a single TxOutput.
///
/// This gets a set of PacketOutput that already starts at the given output
/// If additional pages are needed, they are read from the given file
pub fn _unpack_one() -> TxOutput {
    unimplemented!()
}



/* This is our layout;
 * However this doesn't pack well in rust,
 * So we're going to do it manually
pub enum PackedOutput {
    P2PKH {
        index: [u8;2],
        hash: [u8;20],
        amount: i64,
    },
    P2SH {
        index: [u8;3],
        hash: [u8;20],
        amount: i64,
    },
    Uncompressed {
        index:u32,
        script_page:u32,
        script_len:u32,
        amount: i64,
    },
    UncompressedScript {
        script: [u8;31]
    },
}
*/
