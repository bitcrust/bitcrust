use std::io;
use super::*;


/// Returns an iterator that scans the buffer and decodes and yields transactions
///
/// The buffer must be size (varint) prefixed
pub fn iter_decode(buffer: &[u8]) -> Result<TransactionDecodingCursor, io::Error> {
    let buffer_size = buffer.len();
    let mut cursor = io::Cursor::new(buffer);
    let count: usize = decode(&mut cursor)?;
    Ok(TransactionDecodingCursor {
        cursor: cursor,
        buffer_size: buffer_size,
        count: count,
    })
}



/// Iterator used to decode a blob of transactions
pub struct TransactionDecodingCursor<'a> {
    pub cursor: io::Cursor<&'a [u8]>,
    pub buffer_size: usize,
    pub count: usize,
}


impl<'a> Iterator for TransactionDecodingCursor<'a> {
    type Item = Result<(Transaction, usize, usize), io::Error>;

    fn next(&mut self) -> Option<Result<(Transaction, usize, usize), io::Error>> {
        if self.count == 0 {
            if self.cursor.position() as usize != self.buffer_size {
                Some(Err(io::Error::new(io::ErrorKind::Other, "Block buffer not fully consumed")))
            }
                else {
                    None
                }
        } else {
            Some({
                let pos = self.cursor.position() as usize;
                self.count -= 1;
                decode(&mut self.cursor).map(|tx: Transaction|
                    (tx, pos, self.cursor.position() as usize)
                )
            })
        }
    }
}
