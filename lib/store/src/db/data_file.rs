///
/// Common trait for all store's datafiles
///
/// This implements some read write helper functions and the `DataFileHeader` struct
///


use std::{path,io, fs, mem, slice};

use std::collections::HashMap;
use atomic::{Atomic,Ordering};
use std::os::unix::io::AsRawFd;
use crate::DbError;
use libc::*;
use memmap;

/// Steps by which file should be preallocated
const INCREASE_SIZE: u64 = 100_000_000;

const MAGIC_FILE_ID: [u8;8] = *b"BCDFKM00";


extern {
    fn pwrite(fd: c_int, buf: *const c_void, count: size_t, offset: off_t) -> ssize_t;
    fn pread(fd: c_int, buf: *mut c_void, count: size_t, offset: off_t) -> ssize_t;
    fn ftruncate(fd: c_int, size: off_t) -> c_int;


}


#[derive(Debug)]
pub enum DataFileError {
    IoError(io::Error),
    MagicFileIdMismatch,
    FailedToReadCompleteData,
    FailedToWriteCompleteData,
    IncorrectZeroPointer,
}
impl From<io::Error> for DataFileError {
    fn from(err: io::Error) -> Self {
        DataFileError::IoError(err)
    }
}

/// Header used by all files in the store
#[derive(Default)]
#[repr(C)]
pub struct DataFileHeader {
    magic_id:     [u8;8],
    pub write_pos:    Atomic<u64>,
    pub size:         Atomic<u64>,
    pub aggregates32: [Atomic<u32>;6],
    pub aggregates:   [Atomic<u64>;10],
}


pub trait DataFile {

    // common functions

    /// Creates or opens the file at the given location
    fn new<P : AsRef<path::Path>>(filename: P) -> Result<Self, DbError>
        where Self : ::std::marker::Sized;

    fn stats(&self) -> HashMap<String,u64>;

    /// Creates a new file and destroys the existing one if it exists
    fn new_empty<P : AsRef<path::Path>>(filename: P)  -> Result<Self, DbError>
        where Self : ::std::marker::Sized {

        let p = filename.as_ref();
        if p.exists() {
            // We can unwrap as this is only useful for tests anyway
            fs::remove_file(p).unwrap();
        }
        Self::new(p)
    }

}

/// Initializes the file with the `DataFileHeader`
///
/// root_size is the number of bytes to allocate *after* the data file header.
pub fn init_file(file_name: &path::Path, root_size: usize) -> Result<(), DataFileError> {

    const HEADER_LEN: usize = mem::size_of::<DataFileHeader>();
    let file_len = (root_size + HEADER_LEN) as u64;

    // create path
    if let Some(dir) = file_name.parent() {
        fs::create_dir_all(dir)?;
    };

    let file = fs::File::create(&file_name)?;
    file.set_len(file_len)?;

    let header = DataFileHeader {
        magic_id:  MAGIC_FILE_ID,
        write_pos: Atomic::new(file_len),
        size:      Atomic::new(file_len),
        ..Default::default()
    };
    let result = unsafe {
        pwrite(file.as_raw_fd(),
               (&[header]).as_ptr() as *const c_void,
               HEADER_LEN,
               0i64)
    };
    if HEADER_LEN as isize > result {
        Err(DataFileError::FailedToWriteCompleteData)
    } else {
        Ok(())
    }
}

impl DataFileHeader {

    pub fn init_aggregate<T : Into<u64> + Copy>(&self, index: usize, initial_value: T) -> &Atomic<T>
    {
        assert_eq!(mem::size_of::<T>(), 8);
        // set default; failure means it is already initialized
        let _ = self.aggregates[index].compare_exchange(0, initial_value.into(),
                                                    Ordering::Relaxed, Ordering::Relaxed);

        unsafe { mem::transmute(&self.aggregates[index]) }
    }

    pub fn init_aggregate32<T : Into<u32> + Copy>(&self, index: usize, initial_value: T) -> &Atomic<T>
    {
        assert_eq!(mem::size_of::<T>(), 4);
        // set default; failure means it is already initialized
        let _ = self.aggregates32[index].compare_exchange(0, initial_value.into(),
                                                        Ordering::Relaxed, Ordering::Relaxed);
        unsafe { mem::transmute(&self.aggregates32[index]) }
    }
}

/// Returns a memmap handle to the header, and a reference to its content
pub fn map_header(file: &fs::File) -> Result<(memmap::Mmap, &'static DataFileHeader), DataFileError> {
    // setup memmap
    let mmap = memmap::Mmap::open_with_offset(
        &file,
        memmap::Protection::ReadWrite,
        0,
        mem::size_of::<DataFileHeader>()
    )?;

    // extract reference to header
    let header_pointer = mmap.ptr() as *const DataFileHeader;
    let header = unsafe { &* header_pointer };

    if header.magic_id != MAGIC_FILE_ID {
        Err(DataFileError::MagicFileIdMismatch)
    } else {
        Ok((mmap, header))
    }
}

/// Reads a raw byte sequence from the given location
pub fn read<R: AsRawFd>(rd: &R, offset: u64, size: u64)
                                    -> Result<Vec<u8>, DataFileError>
{
    let mut buffer = vec![0u8; size as usize];

    let read = unsafe { pread(rd.as_raw_fd(),
                              buffer.as_mut_ptr() as *mut c_void,
                              size as usize,
                              offset as i64)
    };
    if read != size as isize {
        // Trouble. If read is still positive, we call again to (presumably) get the errno
        // It could be that we just need another read, but I don't think that should happen
        // on mothern systems. As this has happened, we now dump more info to debug
        let retry = unsafe { pread(rd.as_raw_fd(),
                                  buffer.as_mut_ptr() as *mut c_void,
                                  size as usize,
                                  offset as i64) };
        error!("Failed to read coplete data read={:x}, size={:x}, offset={:x}, retry={:x}, os_err={}",
            read, size, offset, retry, ::std::io::Error::last_os_error());
        return Err(DataFileError::FailedToReadCompleteData);
    }

    Ok(buffer)
}

/// Writes an object in in-memory format
pub fn write_object<W: AsRawFd, T>(wr: &W, obj: &T, file_header: &DataFileHeader) -> Result<u64, DataFileError> {
    let content = unsafe {
        slice::from_raw_parts(
            (obj as *const T) as *const u8,
            mem::size_of::<T>())
    };
    write(wr, content, file_header)
}

/// Writes the given content to the given position
pub fn write_at<W: AsRawFd>(wr: &W, content: &[u8], pos: u64)
                         -> Result<(), DataFileError> {

    let len = content.len() as u64;
    let result = unsafe { pwrite(wr.as_raw_fd(), content.as_ptr() as *const c_void, len as usize, pos as i64) };
    if len as isize - result >= 8 {
        Err(DataFileError::FailedToWriteCompleteData)
    }
    else {
        Ok(())
    }
}

/// Writes the given content to the end-of-file and returns a pointer to it
///
/// This happens atomically using a fetch_add of the write-pos
///
pub fn write<W: AsRawFd>(wr: &W, content: &[u8], file_header: &DataFileHeader)
                         -> Result<u64, DataFileError>
{
    let len = content.len() as u64;
    debug_assert!(len % 8 == 0, "{} is not a multiple of 8", len);

    let target = file_header.write_pos.fetch_add(len, Ordering::Relaxed);

    // make space
    if target + len >= file_header.size.load(Ordering::Relaxed) {
        let new_size = file_header.size.fetch_add(INCREASE_SIZE, Ordering::Relaxed) + INCREASE_SIZE;

        unsafe { ftruncate(wr.as_raw_fd(), new_size as i64) };
    }

    let result = unsafe { pwrite(wr.as_raw_fd(), content.as_ptr() as *const c_void, len as usize, target as i64) };
    if len as isize - result >= 8 {
        Err(DataFileError::FailedToWriteCompleteData)
    }
    else {
        Ok(target)
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use std::fs;


    #[test]
    fn test_read_write() {
        let name = "test-data/data_file_rw";
        let _ = fs::create_dir_all("test-data");
        let fr = fs::OpenOptions::new().write(true).read(true)
            .create(true).truncate(true).open(name).unwrap();

        init_file(name.as_ref(),0).unwrap();
        let (_mmap, hdr) = map_header(&fr).unwrap();

        let p1 = write(&fr, &[17u8;64], hdr).unwrap();
        let p2 = write(&fr, &[113u8;256], hdr).unwrap();
        let p3 = write(&fr, &[94u8;32], hdr).unwrap();

        assert_eq!(read(&fr, p2,256).unwrap(), vec![113u8;256]);
        assert_eq!(read(&fr, p1,64).unwrap(), vec![17u8;64]);
        assert_eq!(read(&fr, p3,32).unwrap(), vec![94u8;32]);

    }


    #[test]
    fn test_file_header() {
        fs::create_dir_all("test-data").unwrap();

        {
            let fr = fs::OpenOptions::new().write(true).read(true)
                .create(true).truncate(true).open("test-data/data_file_header").unwrap();
            fr.set_len(1000).unwrap();

            assert!(map_header(&fr).is_err());

            init_file("test-data/data_file_header".as_ref(),0).unwrap();
            let (_mmap, _) = map_header(&fr).unwrap();
        }
    }
}
