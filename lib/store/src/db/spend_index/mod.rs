//! Index that stores the full state of transactions and spend outputs for each block
//!
//! This is implemented as a copy-on-write tree of pages
//!
//! The store is amended by adding blocks of 64-bit integers on top of other blocks.
//! If any integer already exist in an ancestor block the operation fails.
//!
//! Adding a block will conceptually create a new full bit index of all 64-bit integers. This bit index is divided in
//! 128-byte pages using a prefix tree; each branch page points to 16 children (and thus consumes 4 bits of the input u64's)
//! and the leaf pages contain 1024 bits, (consuming 10 bits of the input u64's).
//!
//! To conserve space, instead of writing the full bit index at every block; we only write changed pages.
//! The result of the add operation is a reference to the root page.
//!


use std::{fs,io,path};
use std::time::Instant;

use atomic::{Atomic,Ordering};
use std::collections::HashMap;
use libc::*;
use std::os::unix::io::AsRawFd;
use std::num::NonZeroU64;

use super::data_file::*;
use crate::DbError;

const MINIMUM_PAGES_AVAILABLE: u64 = 100 * 1024 ;
const PAGE_SIZE: u64 = 128;


/// PageNumber is an index to a page in the spend_index
///
/// The first four bits are used in root pages to store the tree depth
///
#[derive(Debug,Copy,Clone)]
pub struct PageNumber(NonZeroU64);


impl From<u64> for PageNumber {
    fn from(v: u64) -> PageNumber {
        unsafe { PageNumber(NonZeroU64::new_unchecked(v)) }
    }
}

impl Into<u64> for PageNumber {
    fn into(self) -> u64 {
        self.0.get()
    }
}

pub const GENESIS_ROOT_PAGE: PageNumber = unsafe { PageNumber(NonZeroU64::new_unchecked(1)) };


type Page = [u64;16];
const NEW_PAGE: Page = [0u64;16];

// the first four bits of a page index are used to store the depth
const PAGE_INDEX_MASK: u64 = 0x0fff_ffff_ffff_ffff;

extern {
    fn pwrite(fd: c_int, buf: *const c_void, count: size_t, offset: off_t) -> ssize_t;
    fn pread(fd: c_int, buf: *mut c_void, count: size_t, offset: off_t) -> ssize_t;
    //fn ftruncate(fd: c_int, size: off_t) -> c_int;
}

/// Handle to the index that maintains which transactions exist and which outputs are spend
/// at every block
///
///
pub struct SpendIndex {
    rw_file:    fs::File,
    data_file_header: &'static DataFileHeader,

    // Memory map to the first page only;
    _mmap:   memmap::Mmap,

    // aggregators
    blocks:   &'static Atomic<u64>,
    pages:    &'static Atomic<u64>,
    set_bits: &'static Atomic<u64>,
    max_bit:  &'static Atomic<u64>,
    net_size: &'static Atomic<u64>,

}

impl DataFile for SpendIndex {

    /// Creates or opens a spend index
    ///
    fn new<P : AsRef<path::Path>>(file_name: P) -> Result<Self, DbError> {

        let file_name = file_name.as_ref();
        let is_new = !file_name.exists();

        if  is_new {
            init_file(file_name, PAGE_SIZE as usize * 2)?;
        }

        let rw_file = fs::OpenOptions::new().read(true).write(true).open(&file_name)?;

        // Setup memory map to header
        let (_mmap, data_file_header) = map_header(&rw_file)?;

        // shortcuts to aggregators
        let blocks   = data_file_header.init_aggregate(0, 1);
        let pages    = data_file_header.init_aggregate(1, 1);
        let set_bits = data_file_header.init_aggregate(2, 1);
        let max_bit  = data_file_header.init_aggregate(3, 0);
        let net_size = data_file_header.init_aggregate(4, 0);

        Ok(SpendIndex {
            rw_file, _mmap, data_file_header,
            blocks, pages, set_bits, max_bit,
            net_size
        })

    }

    fn stats(&self) -> HashMap<String,u64> {
        vec![
            ("blocks".to_owned(),       self.blocks.load(Ordering::Relaxed)),
            ("pages".to_owned(),        self.pages.load(Ordering::Relaxed)),
            ("max_bit".to_owned(),      self.max_bit.load(Ordering::Relaxed)),
            ("set_bits".to_owned(),     self.set_bits.load(Ordering::Relaxed)),
            ("net_size".to_owned(),     self.net_size.load(Ordering::Relaxed)),

        ].into_iter().collect()
    }

}


impl SpendIndex {

    /// Adds a block of "spend_bits" to the spend index on top of previous
    ///
    /// spend_bits are u64 indices to the bits that should be set
    ///
    /// The method will return Ok(None) if one of the bits already exist
    ///
    /// It will also return Ok(None) if any of the verify bits doesn't exist.
    ///
    pub fn add_block(&self, spend_bits: &[u64], verify_bits: &[u64], previous: PageNumber) -> Result<Option<PageNumber>, io::Error> {

        let previous: u64 = previous.into();
        trace!("Start add_block with prev={} and cur_page={}", previous, self.data_file_header.write_pos.load(Ordering::Relaxed));
        trace!("Spend bits: {:x?}", spend_bits);
        let start = Instant::now();

        // Translates the set of spend_bits to a tree of changes
        let mut v = match self.create_block(spend_bits, previous)? {
            None => return Ok(None),
            Some(v) => v,
        };

        trace_tree(&v, 0);
        if v.len() == 0 {
            // No data ot add; we can reuse the previous root pointer as it is immutable
            self.blocks.fetch_add(1, Ordering::Relaxed);
            return Ok(Some(previous.into()))
        }

        // Allocate space for the new tree
        let new_root = self.allocate(v.len() as u64);

        // Merge the tree with the existing data, and adjusting the root relative to new_root to
        // absolute references
        if !self.merge_block(&mut v, previous, new_root)? {
            return Ok(None);
        }

        // Write back
        self.write_pages(&v, new_root)?;

        // Verify existing bits
        if !self.check_exists_all(verify_bits, new_root)? {
            return Ok(None);
        }

        // update counters
        self.pages.fetch_add(v.len() as u64, Ordering::Relaxed);
        self.set_bits.fetch_add(spend_bits.len() as u64, Ordering::Relaxed);
        self.max_bit.fetch_max(spend_bits[spend_bits.len()-1], Ordering::Relaxed);
        self.blocks.fetch_add(1, Ordering::Relaxed);

        let end = Instant::now();
        debug!("Wrote {:10} in {:8} o {} pages at {}", spend_bits.len(), (end-start).as_nanos()/1000, v.len(), new_root);
        Ok(Some(new_root.into()))
    }

    // Returns true if all of the verify_bits exist at root_page
    pub fn check_exists(&self, spendbit: u64, root_page: u64) -> Result<bool, io::Error> {
        let root = self.read_page(root_page)?;
        let depth = get_depth(&root);

        // get branch indices and leaf bit from spendbit
        let mut branch_index: Vec<usize> = vec![0;depth];
        let mut leaf_bit: u64 = 0;
        split_spendbit(spendbit, &mut branch_index, &mut leaf_bit);
        let mut page: Page = self.read_page(root[branch_index[0]])?;
        for n in 1..depth-1 {
            page = self.read_page(page[branch_index[n]])?;
        }

        Ok(page[branch_index[depth-1]] & leaf_bit != 0)
    }

    /// Returns true if all of the verify_bits exist at root_page
    fn check_exists_all(&self, verify_bits: &[u64], root_page: u64) -> Result<bool, io::Error> {

        let root = self.read_page(root_page)?;
        let depth = get_depth(&root);

        // Preallocate space to decode the spend-bits
        let mut branch_index: Vec<usize> = vec![0;depth];
        let mut leaf_bit: u64 = 0;

        // TODO this would be nice and easy to parellelize with rayon
        // (quite easier then the merge_block)
        // It's currently already fast enough though
        for spendbit in verify_bits.iter() {

            split_spendbit(*spendbit, &mut branch_index, &mut leaf_bit);

            let mut page: Page = self.read_page(root[branch_index[0]])?;
            for n in 1..depth-1 {
                page = self.read_page(page[branch_index[n]])?;
            }
            if page[branch_index[depth-1]] & leaf_bit == 0 {
                return Ok(false);
            }
        }
        Ok(true)
    }


    fn read_page(&self, page_number: u64) -> Result<Page, io::Error> {

        let mut buffer = NEW_PAGE;

        let start = (page_number & PAGE_INDEX_MASK) * PAGE_SIZE + ::std::mem::size_of::<DataFileHeader>() as u64;
        let read = unsafe { pread(self.rw_file.as_raw_fd(), buffer.as_mut_ptr() as *mut libc::c_void, PAGE_SIZE as usize, start as i64) };
        if read != PAGE_SIZE as isize {
            Err(io::Error::last_os_error())
        }
        else {
            Ok(buffer)
        }
    }

    // Used for debugging
    /*fn read_all(&self) -> Vec<Page> {
        let total = self.cur_ptr.load(Ordering::Acquire) as usize;
        let mut buffer: Vec<Page> = Vec::new();
        buffer.resize_default(total);

        let _read = unsafe { pread(self.file.as_raw_fd(), buffer.as_mut_ptr() as *mut libc::c_void, total * PAGE_SIZE as usize, 0) };

        buffer
    }*/

    // Allocate `size` pages
    fn allocate(&self, size: u64) -> u64 {
        self.data_file_header.write_pos.fetch_add(size, Ordering::AcqRel)
    }


    fn write_pages(&self, pages: &[Page], root_number: u64) -> Result<(), io::Error> {

        let start = (root_number & PAGE_INDEX_MASK) * PAGE_SIZE + ::std::mem::size_of::<DataFileHeader>() as u64;

        // check if allocated enough
        if self.data_file_header.size.load(Ordering::Acquire) <= root_number + pages.len() as u64 {
            let new_size = self.data_file_header.size.fetch_add(pages.len() as u64 + MINIMUM_PAGES_AVAILABLE, Ordering::AcqRel)
                + MINIMUM_PAGES_AVAILABLE + pages.len() as u64;

            info!("Resizing spend_index to {} pages", new_size );
            self.rw_file.set_len(new_size as u64 * PAGE_SIZE as u64)?;

        }

        let result = unsafe { pwrite(self.rw_file.as_raw_fd(), pages.as_ptr() as *const libc::c_void, pages.len() * PAGE_SIZE as usize, start as i64) };
        if result != (pages.len() * PAGE_SIZE as usize) as isize {
            Err(io::Error::last_os_error())
        }
        else {
            Ok(())
        }
    }

    /// Creates a new block on top of previous, that only contains the changed pages
    ///
    /// This preliminary block will use page-references in its branch pages that are relative to
    /// the new root page, as we don't know the target root_page number yet.
    fn create_block(&self, spendbits: &[u64], previous: u64) -> Result<Option<Vec<Page>>, io::Error> {
        if spendbits.len() == 0 {
            return Ok(Some(vec![]));
        }
        let prev = self.read_page(previous)?;
        let prev_depth = get_depth(&prev);

        let mut spendbits: Vec<_> = spendbits.to_vec();
        spendbits.sort_unstable();
        let depth = depth_from_spendbits(spendbits[spendbits.len()-1]);

        // never decrease depth
        let depth = ::std::cmp::max(depth, prev_depth);

        // We will need less but let's be generous
        let mut pages: Vec<Page> = Vec::with_capacity(spendbits.len() / 4);

        // We allocate these once and fill them per entry
        let mut branch_index: Vec<usize> = vec![0;depth];
        let mut leaf_bit: u64 = 0;

        // If we are increasing the depth, we start with a branch of 0-prefixes to ensure
        // the previous root will be merged
        let mut prev_branch_index = vec![0xffff;depth];
        if depth > prev_depth  {
            pages.resize(depth, NEW_PAGE);
            for n in 0..depth-1 {
                pages[n][0] = n as u64 +1;
                prev_branch_index[n] = 0;
            }

        }
        else {
            // Otherwise we start with start with an empty root page
            pages.push(NEW_PAGE);
        }

        // We browse throught the spend bits ordered and compare with the previous
        // Initialize the previous such that it is ensured to be different
        let mut prev_spend_bit = 0;

        for n in 0..spendbits.len() {
            if spendbits[n] == prev_spend_bit {
                // double spent within passed data
                return Ok(None);
            }
            split_spendbit(spendbits[n], &mut branch_index, &mut leaf_bit);
            let mut new_branch: bool = false;
            let mut cur_page = 0;
            for n in 0..depth-1 {
                if new_branch || branch_index[n] != prev_branch_index[n] {
                    // create new page if now or before our branch_index was different
                    pages.push(NEW_PAGE);
                    new_branch = true;
                    // and point to it
                    pages[cur_page][branch_index[n]] = pages.len() as u64-1;
                }

                // jump in
                cur_page = pages[cur_page][branch_index[n]] as usize;

            }
            // set bit
            pages[cur_page][branch_index[depth-1]] |= leaf_bit;

            prev_branch_index = branch_index.clone();
            prev_spend_bit = spendbits[n];
        }

        set_depth(&mut pages[0], depth);

        Ok(Some(pages))
    }

    /// Update the set of pages to:
    /// 1. Ensure all page references are shifted relative to root_offset
    /// 2. All previous data is merged
    fn merge_block(&self, pages: &mut Vec<Page>, previous: u64, root_offset: u64) -> Result<bool, io::Error> {

        debug!("Merge pages {} with previous={}", root_offset, previous);
        let prev_depth = get_depth(&self.read_page(previous)?);
        let depth = get_depth(&pages[0]);

        self.do_merge_block(pages, 0, previous, depth, prev_depth, root_offset)
    }


    /// Recursive method to merge a new tree with the existing tree
    ///
    /// Note, that this can be parellelized but it's too fast to bother ATM.
    fn do_merge_block(&self, pages: &mut Vec<Page>, page_idx: u64, previous: u64, depth: usize, prev_depth: usize, root_offset: u64) -> Result<bool, io::Error> {

        // We must:
        // A. adjust all pagenumbers on branch-pages to be relative to root
        // B. If our new tree has no branch at a spot, and the corresponding previous has a branch there, use the previous
        // C. If this is a leaf page, and we have a previous, merge them by ORing and verify double spends
        // D. If both new and old branches exist, recurse

        // assume no double spends until we find one
        let mut result = true;

        let i = page_idx as usize;
        debug_assert!(i < pages.len());

        if previous == 0 {
            // no more merging in this branch
            // only adjust root_index (A)

            if depth == 1 {
                // nothing to do for leaf page
                return Ok(result);
            }
            for n in 0..16 {
                if pages[i][n] > 0 {
                    let next_idx = pages[i][n] & 0x0fff_ffff_ffff_ffff;
                    pages[i][n] += root_offset;
                    result &= self.do_merge_block(pages, next_idx, 0, depth-1, 0, root_offset)?;
                }
            }
            return Ok(result);
        }

        if prev_depth < depth {
            // Our tree is deeper then the previous block ; at this level we should only adjust to root_offset

            // recurse into first (this is where our old tree *should* be merged)
            let next_idx = pages[i][0] & PAGE_INDEX_MASK;
            //debug_assert!(next_idx>0);
            if next_idx > 0 {
                pages[i][0] += root_offset;
                result &= self.do_merge_block(pages, next_idx, previous, depth - 1, prev_depth, root_offset)?;
            } else {
                assert_eq!(prev_depth+1, depth, "Depth grows more than one level. TODO: Possible, but not implemented.");
                pages[i][0] = (PAGE_INDEX_MASK & pages[i][0]) | (PAGE_INDEX_MASK & previous);

            }

            // for the other pages, only recurse to adjust root_offset
            for n in 1..16 {
                if pages[i][n] > 0 {
                    let next_idx = pages[i][n] & PAGE_INDEX_MASK;
                    pages[i][n] += root_offset;
                    result &= self.do_merge_block(pages, next_idx, 0, depth-1, 0, root_offset)?;
                }
            }

            return Ok(result);
        }

        // We actually need to merge; read previous
        let prev = self.read_page(previous)?;

        if depth == 1 {
            // merge leaf page
            for n in 0..16 {
                if pages[i][n] & prev[n] != 0 {
                    warn!("DOUBLE SPENT;");

                    return Ok(false);
                }
                pages[i][n] |= prev[n];
            }
        }
        else {
            // merge branch page
            for n in 0..16 {
                let next_idx = pages[i][n] & PAGE_INDEX_MASK;
                let prev_idx = prev[n] & PAGE_INDEX_MASK;

                if next_idx > 0 {
                    pages[i][n] += root_offset;

                    result &= self.do_merge_block(pages, next_idx, prev_idx, depth-1, prev_depth-1, root_offset)?;

                } else if prev_idx > 0 {
                    pages[i][n] = prev[n];
                }
            }
        }
        return Ok(result);
    }


}


// Used for debugging
fn trace_tree(pages: &[Page], start_index: u64) {
    if !log_enabled!(log::Level::Trace) {
        return;
    }
    if pages.len() == 0 {
        return;
    }
    let d = get_depth(&pages[start_index as usize]);
    trace!("DEPTH = {}, PAGES = {} ROOT={}", d, pages.len(), start_index);
    trace!("======================");

    fn dump_subtree(pages: &[Page], page_idx: u64, max_depth: usize, depth: usize) -> u32 {
        let depth_prefix = (0..(max_depth-depth)).map(|_| "*").collect::<String>();


        if depth == 1 {
            let bits: u32 = pages[page_idx as usize].iter().map(|x| x.count_ones()).sum();
            trace!("{}  BITS:{}", depth_prefix, bits);
            return bits;
        } else {
            let mut sum:u32 = 0;
            for n in 0..16 {
                let next = pages[page_idx as usize][n] & 0x0fff_ffff_ffff_ffff;
                if next != 0 {
                    trace!("{} branch ({}|{})", depth_prefix, n, next);
                    sum += dump_subtree(pages, next, max_depth, depth-1);
                }
            }
            return sum;
        }
    }
    let total = dump_subtree(pages, start_index, d, d);
    trace!("TOTAL_BITS = {}", total);
}




/// Splits the spendbit into branch indices and the bit index on the leaf page
fn split_spendbit(spendbit: u64, branch_index: &mut Vec<usize>, leaf_index_bit: &mut u64) {
    let mut b = spendbit as usize;
    *leaf_index_bit = 1u64  << (b & 0x3f);
    b >>= 6;
    let depth = branch_index.len();
    for n in (0..depth).rev() {
        branch_index[n] = b & 0xf;
        b >>= 4;
    }
}

// Gather the depth needed for an input spend bit value
fn depth_from_spendbits(spendbits: u64) -> usize {
    // consider 62 bits; of these 52 are for leafs so max 13 depth + 1 for leaf
    std::cmp::max((14 - (spendbits.leading_zeros() - 2)/4) as usize, 2)
}

/// Depth is stored in the first four bits of a (root) page
fn get_depth(page: &Page) -> usize {
    (page[0] as usize >> 60) + 2
}

/// Depth is stored in the first four bits of a (root) page
fn set_depth(page: &mut Page, depth: usize) {
    page[0] &= 0x0fff_ffff_ffff_ffff;
    page[0] |= (depth as u64-2) << 60;
}



#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_depth_from_spendbits() {
        assert_eq!(depth_from_spendbits(0b0100_1001_1010_1111_00), 3);
        assert_eq!(depth_from_spendbits(0b0000_1001_1010_1111_00), 2);

    }


    #[test]
    fn test_spend_index() {
        let si = SpendIndex::new_empty("test-data/spend-index2").unwrap();

        let block2 = si.add_block(&vec![1,2,5], &vec![], GENESIS_ROOT_PAGE).unwrap().unwrap();

        let block3 = si.add_block(&vec![3,55,300], &vec![], block2).unwrap().unwrap();

        // won't work; 55 double
        assert!(si.add_block(&vec![4,55], &vec![], block3).unwrap().is_none());

        // does work on block2; 55 doesn't exist there
        let block3b = si.add_block(&vec![4,55], &vec![], block2).unwrap().unwrap();

        // no double in same block
        assert!(si.add_block(&vec![400,400], &vec![], block3b).unwrap().is_none());

        // test verify bits
        // all exist:
        let block4 = si.add_block(&vec![400], &vec![5,2,55], block3b).unwrap().unwrap();
        // 56 doesn't exist
        assert!(si.add_block(&vec![500], &vec![5,2,56,55], block4).unwrap().is_none());
    }

    #[test]
    fn test_spend_index_unordered() {
        let si = SpendIndex::new_empty("test-data/spend-index3").unwrap();

        let block2 = si.add_block(&vec![1,2,5], &vec![], GENESIS_ROOT_PAGE).unwrap().unwrap();

        let block3 = si.add_block(&vec![3,55,300_000], &vec![], block2).unwrap().unwrap();

        let _block4 = si.add_block(&vec![450_000_000,900_000_000], &vec![], block2).unwrap().unwrap();

        // won't work; 55 double
        assert!(si.add_block(&vec![4,55], &vec![], block3).unwrap().is_none());

        // does work on block2; 55 doesn't exist there
        let block3b = si.add_block(&vec![4,55], &vec![], block2).unwrap().unwrap();

        // no double in same block
        assert!(si.add_block(&vec![400,400], &vec![], block3b).unwrap().is_none());

        // test verify bits
        // all exist:
        let block4b = si.add_block(&vec![400], &vec![5,2,55], block3b).unwrap().unwrap();
        // 56 doesn't exist
        assert!(si.add_block(&vec![500], &vec![5,2,56,55], block4b).unwrap().is_none());
    }

}
