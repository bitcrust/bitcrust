///
/// Provides a handle to the bitcrust_store database with transactions and blocks
///
///
///
use std::{fs,io};
use std::path::Path;
use bitcoinconsensus_sys::VerifyScriptError;
use network_encoding::*;
use self::data_file::{DataFile,DataFileError};
mod data_file;

pub mod header;
pub mod spend_index;
pub mod block;

pub mod transaction;
pub mod db_stats;

/// All DbErrors are unrecoverable data corruption errors
#[derive(Debug)]
pub enum DbError {
    DataFileError(DataFileError),
    IoError(io::Error),
    ScriptEngineError(VerifyScriptError),
    ParentNotFound,
    HeaderFileCorrupted,
    ObjectNotFound,
    HeaderNotFound(Hash),
    GenesisUnexpected,

    HeaderInvalidLength,

    // System Time set to less then unix epoch.
    // This isn't really a "db" error but still unrecoverable
    SystemTimeError,
}


pub type DbResult<T> = Result<T, DbError>;


impl From<DataFileError> for DbError {
    fn from(err: DataFileError) -> DbError {
        DbError::DataFileError(err)
    }
}


impl From<io::Error> for DbError {
    fn from(err: io::Error) -> DbError {
        DbError::IoError(err)
    }
}


impl From<VerifyScriptError> for DbError {
    fn from(err: VerifyScriptError) -> DbError {
        DbError::ScriptEngineError(err)
    }
}


/// Handle to an opened bitcrust database
///
/// The database verifies and stores transactions, headers and blocks.
///
/// All access to the database can be done using an immutable reference.
///
/// # Examples
///
/// ```rust
/// use bitcrust_store::*;
///
/// let db = init("test-data/example-store").expect("Failed to initialize store");
///
/// ```
pub struct Db {

    pub(crate) hdr: header::HeaderFile,
    pub(crate) blk: block::BlockFile,

    pub(crate) spn: spend_index::SpendIndex,
    tx_in: transaction::TxInFile,
    tx_out: transaction::TxOutFile,

}

impl<'a> Into <&'a header::HeaderFile> for &'a Db {
    fn into(self) -> &'a header::HeaderFile {
        &self.hdr
    }
}

/// Initializes a store and clears all data
///
/// Useful for tests only
///
pub fn init_empty<P: AsRef<Path>>(db_path: P) -> Result<Db, DbError> {
    let db_path = db_path.as_ref();
    let exists = db_path.exists();
    if exists {

        fs::remove_dir_all(db_path).unwrap();
    }
    init(db_path)
}

/// Opens the store at the given path or creates it if it doesn't exist
///
/// Returns a handle to the store. The store can be accessed concurrently,
/// and access to the store take immutable references to the handle
///
/// The store is initialized with Genesis if it is new
///
///
/// # Examples
///
/// ```rust
/// use bitcrust_store::*;
///
/// let db = init("test-data/example-init").expect("Failed to initialize store");
///
/// ```
///
pub fn init<P: AsRef<Path>>(db_path: P) -> Result<Db, DbError> {

    let db_path = db_path.as_ref();
    let db = Db {
        hdr: header::HeaderFile::new(Path::join(db_path, "headers"))?,
        blk: block::BlockFile::new(Path::join(db_path, "blocks"))?,
        spn: spend_index::SpendIndex::new(Path::join(db_path, "spend_index"))?,
        tx_in: transaction::TxInFile::new(Path::join(db_path, "tx_in"))?,
        tx_out: transaction::TxOutFile::new(Path::join(db_path, "tx_out"))?,
    };

    Ok(db)
}
