///
/// Exposes database statistics
///
use super::{Db, DbError};
use crate::db::data_file::DataFile;
use std::collections::HashMap;



/// Prefixes a string
fn prefix(prefix: &str, key: String) -> String {
    let mut new_key = prefix.to_owned();
    new_key.push_str(&key);
    new_key
}

/// Returns the stats of all store files, gathered in a hashmap
pub fn stats_get(db: &Db) -> Result<HashMap<String, u64>, DbError> {

    let mut result: HashMap<String, u64> = HashMap::new();

    result.extend(db.hdr.stats().into_iter().map(|(k,v)| { (prefix("headers.",k),v) }));
    result.extend(db.spn.stats().into_iter().map(|(k,v)| { (prefix("spend_index.",k),v) }));
    result.extend(db.blk.stats().into_iter().map(|(k,v)| { (prefix("blocks.",k),v) }));
    result.extend(db.tx_in.stats().into_iter().map(|(k,v)| { (prefix("tx_in.",k),v) }));
    result.extend(db.tx_out.stats().into_iter().map(|(k,v)| { (prefix("tx_out.",k),v) }));


    Ok(result)
}
