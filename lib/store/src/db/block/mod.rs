//! A block is the set of transaction pointers commited to a header
//!
//! It is stored in a rather bare `DataFile`. Each block is stored as a length prefixed set
//! of `TxPtr`s
//!
//!
use std::{fs,path,mem};
use std::num::NonZeroU64;
use std::collections::HashMap;
use atomic::{Atomic,Ordering};
use super::data_file::*;
use crate::db::transaction::TxPtr;
use network_encoding::*;
use crate::DbError;

/// Persistent pointer to a record in the Block File
#[derive(Debug,PartialEq,Eq,Copy,Clone)]
pub struct BlockPtr(NonZeroU64);

impl BlockPtr {
    fn from_file_pos(file_pos: u64) -> BlockPtr {
        unsafe {
            BlockPtr(NonZeroU64::new_unchecked(
                ((file_pos - mem::size_of::<DataFileHeader>() as u64) / 8) + 1
            ))
        }
    }
    fn to_file_pos(self) -> u64 {
        (self.0.get() - 1) * 8 + mem::size_of::<DataFileHeader>() as u64
    }

    pub const GENESIS: Self = unsafe { BlockPtr(NonZeroU64::new_unchecked(1)) };
}



pub struct BlockFile {
    rw_file:     fs::File,
    data_file_header: &'static DataFileHeader,

    _mmap:             memmap::Mmap,

    // aggregators
    blocks: &'static Atomic<u64>,
    transactions: &'static Atomic<u64>,
}


impl DataFile for BlockFile {

    /// Creates a new block file
    fn new<P : AsRef<path::Path>>(file_name: P) -> Result<Self, DbError> {
        let file_name = file_name.as_ref();
        let is_new = !file_name.exists();

        if  is_new {
            init_file(file_name, 0)?;
        }

        let rw_file = fs::OpenOptions::new().read(true).write(true).open(&file_name)?;

        // Setup memory map to header
        let (_mmap, data_file_header) = map_header(&rw_file)?;

        // shortcuts to aggregators
        let blocks = &data_file_header.aggregates[0];
        let transactions = &data_file_header.aggregates[1];

        let result = BlockFile {
            rw_file, _mmap, data_file_header,
            blocks, transactions,
        };
        if is_new {
            // add genesis
            let block_ptr = result.add(&vec![TxPtr::GENESIS])?;
            assert_eq!(block_ptr, BlockPtr::GENESIS);
        }
        Ok(result)
    }

    fn stats(&self) -> HashMap<String,u64> {
        vec![
            ("blocks".to_owned(),       self.blocks.load(Ordering::Relaxed)),
            ("transactions".to_owned(), self.transactions.load(Ordering::Relaxed)),
        ].into_iter().collect()
    }
}

impl BlockFile {

    /// Adds the block to the block_file returning a pointer
    pub fn add(&self, transactions: &Vec<TxPtr>) -> Result<BlockPtr, DbError> {
        let count = transactions.len() as u64;
        let block = encode_block(transactions);
        let file_pos = write(&self.rw_file, &block, &self.data_file_header)?;

        let _ = self.blocks.fetch_add(1, Ordering::Relaxed);
        let _ = self.transactions.fetch_add(count, Ordering::Relaxed);

        Ok(BlockPtr::from_file_pos(file_pos))

    }

    /// Retrieves a block from the block file
    pub fn get(&self, block_ptr: BlockPtr) -> Result<Vec<TxPtr>, DataFileError> {
        let file_pos = block_ptr.to_file_pos();

        let len_buf = read(&self.rw_file, file_pos, 8)?;
        let len: u64 = decode_buf(&len_buf)?;

        let data_buf = read(&self.rw_file, file_pos + 8, len * 8)?;
        let result = unsafe { ::std::slice::from_raw_parts(
            (data_buf.as_ptr() as *const u8) as *const TxPtr,
            len as usize)
        };

        Ok(result.to_vec())
    }

    /// Retrieves only the first TxPtr (=coinbase-tx) from the block file
    pub fn get_first(&self, block_ptr: BlockPtr) -> Result<TxPtr, DataFileError> {
        let file_pos = block_ptr.to_file_pos();

        let data_buf = read(&self.rw_file, file_pos + 8, 8)?;
        let result = unsafe { ::std::slice::from_raw_parts(
            (data_buf.as_ptr() as *const u8) as *const TxPtr,
            1)
        };
        Ok(result[0])
    }
}


// Encodes a Vec<TxPtr> to a byte-sequence
fn encode_block(transactions: &Vec<TxPtr>) -> Vec<u8> {
    let len = transactions.len() as u64;
    let encoded_len = unsafe { ::std::slice::from_raw_parts(
        ((&len) as *const u64) as *const u8, 8) };

    let encoded_txs : &[u8] = unsafe { ::std::slice::from_raw_parts(
        transactions.as_ptr() as *const u8,
        8 * len as usize)
    };
    let mut result: Vec<u8> = Vec::with_capacity(8 * (len as usize + 1));
    result.extend_from_slice(encoded_len);
    result.extend_from_slice(encoded_txs);
    result

}


#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_block_file() {

        let file = BlockFile::new_empty("test-data/block_file").unwrap();

        let v1 = vec![TxPtr::from_file_pos(1000_000_000+124*8), TxPtr::from_file_pos(1000_000_000+28394*8), TxPtr::from_file_pos(1000_000_000+11*8)];
        let v2 = vec![TxPtr::from_file_pos(1000_000_000+8), TxPtr::from_file_pos(1000_000_000+7*8)];
        let b1 = file.add(&v1).unwrap();
        let b2 = file.add(&v2).unwrap();

        assert_eq!(file.get(b1).unwrap(), v1);
        assert_eq!(file.get(b2).unwrap(), v2);

        /*
        let is_topological_order = (0..transactions.len()-1)
            .par_iter() // loop transactions
            .all(|n| transactions[n]
                .inputs
                .par_iter() // loop all inputs
                .map(|input| input.prev_tx_out)
                .all(|prev_hash| (n+1..transactions.len())
                    .par_iter() // loop transactions following the current
                    .all(|n| transactions[n].hash != prev_hash)
                )
            );
        */
       /*
        let is_canonical_order = (0..transactions.len() -1)
            .par_iter()
            .all(|n| transactions[n].hash < transactions[n+1].hash);

       */
    }
}