//!
//! All transaction and block verification functions are located here (except double-spends)
//!

use std::ops::Range;
use rayon::prelude::*;
use itertools::*;
use crate::pow;
use crate::chain;
use crate::merkle_tree;
use network_encoding::*;
use crate::db::header::Header;
use crate::db::{Db,DbError};
//use db::db_transaction::Transaction;
use crate::db::transaction::{Transaction,TxCached};
//use record::IndexedPtr;
use crate::StoreError;
use crate::ScriptVerifyFlags;

#[derive(Debug)]
pub enum VerifyError {

    BlockSizeExceeded { expected: usize, given: usize },

    MerkleRootMismatch,                             // "bad-txnmrklroot"
    BlockHashTooHigh,                               // "high-hash"
    BadDifficultyTarget { expected: pow::U256, given: pow::U256, height: u32 }, // "bad-diffbits"

    BlockTimeTooOld,           // "time-too-old"
    BlockTimeTooNew,           // "time-too-new"
    BlockIncorrectVersion { expected: i32, given: i32 },
                               // "bad-version"

    BlockNonTopologicalOrder,
    BlockNonCanonicalOrder,
    BlockDoubleSpent,

    // basic transaction checks
    TxNoInputs,                // "bad-txns-vin-empty"
    TxNoOutputs,               // "bad-txns-vout-empty"
    TxTooLarge,                // "bad-txns-oversize"
    TxAmountTooLarge,          // "bad-txns-vout-toolarge"
    TxAmountTotalTooLarge,     // "bad-txns-txouttotal-toolarge"
    TxSigOpsTooLarge,          // "bad-txn-sigops"
    TxDuplicateInputs,         // "bad-txns-inputs-duplicate"
    TxNegativeFee,             // "bad-txns-fee-negative"
    TxNonFinalTime,            // "bad-txns-nonfinal"
    TxNonFinalHeight,          // "bad-txns-nonfinal"
    TxSequenceTooEarly,

    TxSizeExceeded { expected: usize, given: usize },

    // Coinbase
    CoinbaseBadMissing,        // "bad-cb-missing"
    CoinbaseBadLength,         // "bad-cb-length"
    CoinbaseBadHeight,         // "bad-cb-height"
    CoinbaseBadAmount,         // "bad-cb-amount"

    ScriptFail { expected: ScriptVerifyFlags, given: ScriptVerifyFlags },

    PrevOutNotFound,           // "?"
}

// macro that raises error on boolean false
macro_rules! check {
    ($cond:expr, $err:expr, $hash:expr ) => {
        if $cond { Ok(()) } else { warn!("Error at {}", $hash); Err($err.into()) } };
    ($cond:expr, $err:expr ) => {
        if $cond { Ok(()) } else { Err($err.into()) } };

}

pub fn verify_block_size(size: usize) -> Result<(), VerifyError> {
    const MAX_BLOCK_SIZE: usize = 32_000_000;

    check!(size <= MAX_BLOCK_SIZE,
        VerifyError::BlockSizeExceeded { expected: MAX_BLOCK_SIZE, given: size })
}

/// Verify if the merkle root in the header equals the merkle root for the set of transactions
pub fn verify_merkle_root(hdr: &Header, txs: &Vec<Transaction>) -> Result<(), VerifyError> {

    let hashes: Vec<Hash> = txs.iter().map(|tx| tx.meta.hash).collect();
    let expected_root = merkle_tree::get_merkle_root(hashes);

    check!(expected_root == hdr.hdr.merkle_root, VerifyError::MerkleRootMismatch)
}

/// Verify if the block hash is less then the (nBits) target supplied
pub fn verify_proof_of_work(hdr: &Header) -> Result<(), VerifyError> {

    let target = pow::from_compact(hdr.hdr.bits);
    let hash = pow::U256::from(&hdr.hash);

    if cfg!(test) || cfg!(feature = "regtest") {
        Ok(()) // Disable this on testing
    } else {
        check!(hash <= target, VerifyError::BlockHashTooHigh)
    }
}

/// Verify that the (nBits) target is set as the correctly calculated difficulty
pub fn verify_work_target(db: &Db, hdr: &Header) -> Result<(), StoreError> {

    let target = hdr.hdr.bits;
    let expected_target = crate::pow::difficulty::get_target_required(db, &hdr)?;

    check!(target == expected_target, VerifyError::BadDifficultyTarget {
        expected: pow::from_compact(expected_target),
        given: pow::from_compact(target),
        height: hdr.height
    })

}

/// Verify the header time is larger then the median time of the previous 11 blocks
pub fn verify_median_time(db: &Db, hdr: &Header) -> Result<(), StoreError> {

    let prev = hdr.parent(db)?;
    let prev_median_time = prev.median_time(db, 11);

    check!(hdr.hdr.time > prev_median_time, VerifyError::BlockTimeTooOld)

}

/// Verify that the block time is not more the two hours ahead of the system time
/// The system time should be verified to the adjusted network time separately
///
/// This can raise a systemtime dberror if the systemtime is out of range of a 32-bit unix time
pub fn verify_network_time(hdr: &Header) -> Result<(), StoreError> {

    const MAX_FUTURE_BLOCK_TIME: u64 = 2 * 60 * 60;

    let net_time_plus_2h = hdr.network_time as u64 + MAX_FUTURE_BLOCK_TIME;

    // time overflow?
    if net_time_plus_2h > u32::max_value() as u64 {
        return Err(DbError::SystemTimeError.into())
    }

    check!(hdr.hdr.time as u64 <= net_time_plus_2h, VerifyError::BlockTimeTooNew)
}

/// Verify the version in the header is at least as required by historic softfork updates
pub fn verify_version(hdr: &Header) -> Result<(), VerifyError> {

    let min_version =
        if hdr.height < chain::BIP34_HEIGHT { 0 }
        else if hdr.height < chain::BIP66_HEIGHT { 2 }
        else if hdr.height < chain::BIP65_HEIGHT { 3 }
        else { 4 };

    check!(hdr.hdr.version >= min_version, VerifyError::BlockIncorrectVersion
        { expected: min_version, given: hdr.hdr.version }, Hash::from_data(&hdr.as_raw()))
}

/// Checks the script flags that are active at this height
pub fn verify_min_script_flags(hdr: &Header, verify_flags: crate::ScriptVerifyFlags) -> Result<(), VerifyError> {
    let height = hdr.height;

    // determine minimal flags needed based on height
    let min_flags =
        ScriptVerifyFlags::VERIFY_VALID
        | if height >= 173805 { ScriptVerifyFlags::VERIFY_P2SH } else { ScriptVerifyFlags::VERIFY_NONE }
        | if height >= 363725 { ScriptVerifyFlags::VERIFY_DERSIG } else { ScriptVerifyFlags::VERIFY_NONE }
        | if height >= 388381 { ScriptVerifyFlags::VERIFY_CHECKLOCKTIMEVERIFY } else { ScriptVerifyFlags::VERIFY_NONE }
        | if height >= 419328 { ScriptVerifyFlags::VERIFY_CHECKSEQUENCEVERIFY } else { ScriptVerifyFlags::VERIFY_NONE }
        | if height >= 478558 { ScriptVerifyFlags::ENABLE_SIGHASH_FORKID } else { ScriptVerifyFlags::NOT_ENABLE_SIGHASH_FORKID };

    // TODO later additions: LOW_S and NULL_FAIL

    check!(min_flags & verify_flags == min_flags, VerifyError::ScriptFail {
        expected: min_flags, given: verify_flags
    })
}

/// Checks the relative locktimes using the tx.sequence field
pub fn verify_sequence(db: &Db, tx: &TxCached, header: &Header) -> Result<(), StoreError> {

    // We must check if the time/height between inputs and outputs spend is large enough.
    // As we use a tree storage, we cannot simply find the height of an output; there can be multiple
    // Instead, we reverse the check: We test if the output exists at a given height

    // Inputs with a sequence with the highest bit set are ignored
    const SEQUENCE_DISABLE_FLAG: u32 = 1 << 31;

    // Type determines whether height-based or time-based
    const SEQUENCE_TYPE_FLAG: u32 = 1 << 22;

    const SEQUENCE_MASK: u32 = 0xFFFF;
    const SEQUENCE_TIME_UNITS: u32 = 512;

    for input in tx.inputs.iter() {
        if input.sequence & SEQUENCE_DISABLE_FLAG != 0 || input.sequence == 0 {
            continue;
        }
        // Turn the spend output into a spend_bit that we can lookup in the spend index
        let spend_bit = input.out_ptr.as_spend_bit_transaction();

        let min_coin_header = if input.sequence & SEQUENCE_TYPE_FLAG == 0 {

            // time based
            let time_delta = (input.sequence & SEQUENCE_MASK) * SEQUENCE_TIME_UNITS;
            let min_coin_time = header.median_time(db, 11) - time_delta;

            // This should be done more efficiently, but it is quite rare anyway
            header
                .iter_ancestors(db)
                .skip_while(|header| header.median_time(db, 11) > min_coin_time)
                .next()
        }
        else {
            // height based
            let height_delta = input.sequence & SEQUENCE_MASK;

            header.ancestor_at(db, header.height - height_delta)
        };

        // check if the spend
        if let Some(hdr) = min_coin_header {
            let root_page = hdr.get_spend_index_root().ok_or(DbError::ObjectNotFound)?;
            if !db.spn.check_exists(spend_bit, root_page.into())? {
                return Err(StoreError::VerifyError(VerifyError::TxSequenceTooEarly));
            }
        }
    }
    Ok(())
}


/// Checks if the transaction is "final" using the tx.lock_time field
pub fn verify_locktime(db: &Db, tx: &TxCached, header: &Header) -> Result<(), StoreError> {

    // Threshold for lock_time: below this value it is interpreted as block number,
    // otherwise as UNIX timestamp. Threshold is Tue Nov 5 00:53:20 1985 UTC
    const LOCKTIME_THRESHOLD: u32 = 500_000_000;

    const SEQUENCE_FINAL: u32 = 0xFFFF_FFFF;

    if tx.lock_time == 0 {
        return Ok(())
    }

    // Setting all sequence's to ffffffff disables locktime check
    if tx.inputs.iter().all(|input| input.sequence == SEQUENCE_FINAL) {
        return Ok(())
    }

    if tx.lock_time < LOCKTIME_THRESHOLD {
        // check by median time
        let median_past = header.parent(db)?.median_time(db, 11);
        check!(tx.lock_time < median_past, VerifyError::TxNonFinalTime)
    }
    else {
        check!(tx.lock_time < header.height, VerifyError::TxNonFinalHeight)
    }
}



/// Basic transaction checks
pub fn verify_transaction(tx: &Transaction) -> Result<(), VerifyError> {

    check!(!tx.tx.inputs.is_empty(), VerifyError::TxNoInputs)?;
    check!(!tx.tx.outputs.is_empty(), VerifyError::TxNoOutputs)?;

    const MAX_TRANSACTION_SIZE: usize = 1_000_000;
    check!(tx.meta.size as usize <= MAX_TRANSACTION_SIZE,
        VerifyError::TxSizeExceeded { expected: MAX_TRANSACTION_SIZE, given: tx.meta.size as usize})?;

    const MAX_MONEY: i64 = 21_000_000 * 100_000_000;
    const MONEY_RANGE: Range<i64> = (0i64..MAX_MONEY);

    // check amounts individually
    check!(tx.tx.outputs.iter().all(|tx_out| MONEY_RANGE.contains(&tx_out.value)),
        VerifyError::TxAmountTooLarge)?;

    // check total amount
    let sum = tx.tx.outputs.iter().map(|tx_out| tx_out.value).sum();
    check!(MONEY_RANGE.contains(&sum), VerifyError::TxAmountTotalTooLarge)?;

    // check double inputs
    let has_dups = tx.tx.inputs
        .iter()
        .combinations(2)
        .any(|pair|
                 pair[0].prev_tx_out == pair[1].prev_tx_out && pair[0].prev_tx_out_idx == pair[1].prev_tx_out_idx);
    check!(!has_dups, VerifyError::TxDuplicateInputs)?;

    // TODO sigops
    Ok(())
}

// Checks that the transaction fee is non-negative
pub fn verify_transaction_fee(fee: i64) -> Result<(), VerifyError> {

    check!(fee >= 0, VerifyError::TxNegativeFee)
}

/// Verify the order of transactions within a block
pub fn verify_block_order(txs: &[Transaction]) -> Result<(), VerifyError> {

    if true /* use_topological_order */  {

        // We test if for every spend prev_tx_out, no tx-hash comes after
        let is_topological_order = (0..txs.len() - 1)

            // loop all inputs
            .into_par_iter()
            .all(|n| txs[n].tx.inputs
                .iter()
                .map(|input| input.prev_tx_out)
                .all(|prev_hash| (n + 1..txs.len())

                    // loop transactions following the current
                    .into_par_iter()
                    .all(|n| txs[n].meta.hash != prev_hash)
                )
            );

        check!(is_topological_order, VerifyError::BlockNonTopologicalOrder)
    }
    else {
        // check if transactions are ordered by tx hash
        let is_canonical_order = txs
            // loop over pairs of two
            .par_windows(2)
            .all(|tx| pow::U256::from(&tx[0].meta.hash) < pow::U256::from(&tx[1].meta.hash));

        check!(is_canonical_order, VerifyError::BlockNonCanonicalOrder)
    }
}

pub fn verify_coinbase(hdr: &Header, coinbase: &Transaction, sum_fees: i64) -> Result<(), VerifyError> {

    // Is coinbase?
    check!(coinbase.tx.inputs.len() == 1 && coinbase.tx.inputs[0].prev_tx_out.is_null(),
        VerifyError::CoinbaseBadMissing)?;

    // Check coinbase-text length
    check!((2..=100).contains(&coinbase.tx.inputs[0].script.len()), VerifyError::CoinbaseBadLength)?;

    // Check amount
    let sum_output: i64 = coinbase.tx.outputs.iter().map(|tx_out| tx_out.value).sum();
    let halvings        = hdr.height / chain::HALVING_INTERVAL;
    let subsidy         = if halvings >= 64 { 0 } else { chain::INITIAL_SUBSIDY  >> halvings };

    check!(sum_output <= subsidy + sum_fees, VerifyError::CoinbaseBadAmount)?;
    if sum_output > subsidy + sum_fees {
        // Allowed but awkward
        warn!("Coinbase left money on the table; subsidy={}, fees={}, claimed={}", subsidy, sum_fees, sum_output);
    }

    // Check if the coinbase starts with the height
    if hdr.height >= chain::BIP34_HEIGHT {

        debug_assert!((0x8000..0x8000_0000).contains(&hdr.height));

        // As we have no internal Script interpreter yet, we encode it manually
        let expected_height = if hdr.height <= 0x7f_ffff {
            vec![0x03u8, hdr.height as u8, (hdr.height >> 1) as u8, (hdr.height >> 2) as u8]
        }  else  {
            vec![0x04u8, hdr.height as u8, (hdr.height >> 1) as u8, (hdr.height >> 2) as u8, (hdr.height >> 3) as u8]
        };
        let given_height = &coinbase.tx.inputs[0].script[0..expected_height.len()];
        check!(expected_height == given_height, VerifyError::CoinbaseBadHeight)?;
    }

    Ok(())
}
