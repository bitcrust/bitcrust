///
/// Provides the algorithms to calculate the target difficulty of a headers
///

use std::cmp;
use super::U256;
use crate::db::header::Header;
use crate::db::{Db, DbResult, DbError};


// TODO move some of these to chain.rs
const DAA1_RETARGET_TIMESPAN: u32 = (60 * 60 * 24 * 14);
const DAA1_RETARGET_TIMESPAN_U256:U256 = U256::new([DAA1_RETARGET_TIMESPAN as u64, 0u64, 0u64, 0u64]);

const DAA1_RETARGET_BLOCKS: u32   = DAA1_RETARGET_TIMESPAN / 600;
const DAA1_MAX_TARGET: U256       = U256::new([0xffffffff_ffffffffu64, 0xffffffff_ffffffffu64, 0xffffffff_ffffffffu64,0x00000000_ffffffffu64]);

const DAA2_FIRST_BLOCK: u32       = 478558;
const DAA3_FIRST_BLOCK: u32       = 504032;


pub fn get_target_required(db: &Db, hdr: &Header) -> DbResult<u32> {
    debug_assert!(hdr.height > 0); // doesn't work for genesis

    // In hindsight, we can safely select our difficulty-adjustment-algorithm on height
    if hdr.height < DAA2_FIRST_BLOCK {
        get_target_required_daa_original(db, hdr)
    } else if hdr.height < DAA3_FIRST_BLOCK {
        get_target_required_daa_eda(db, hdr)
    } else {
        get_target_required_daa_cash(db, hdr)
    }

}

/// Gets the difficulty target using the original DAA
fn get_target_required_daa_original(db: &Db, hdr: &Header) -> DbResult<u32> {
    let prev = hdr.parent(db)?;
    let prev_target = prev.hdr.bits;

    if hdr.height % DAA1_RETARGET_BLOCKS == 0 {

        let prev_target_u256 = super::from_compact(prev_target);

        // get timespan of 2015 blocks
        // there is a one-off error here in the original code that we replicate
        let first_of_period = hdr.ancestor_at(db, hdr.height - DAA1_RETARGET_BLOCKS).unwrap();
        let end_of_period = &prev;

        let timespan = end_of_period.hdr.time - first_of_period.hdr.time;

        // bound actual timespan by limits
        let timespan = cmp::max(DAA1_RETARGET_TIMESPAN / 4, timespan);
        let timespan = cmp::min(DAA1_RETARGET_TIMESPAN * 4, timespan);

        // new-target = old-target * timespan / expected-timespan
        let target = prev_target_u256.mul_u32(timespan) / DAA1_RETARGET_TIMESPAN_U256;

        trace!(target: "difficulty", "height={}, limited_timespan={}, real_timespan={}, exp_timespan={}, target={}",
            first_of_period.height, timespan, prev.hdr.time - first_of_period.hdr.time, DAA1_RETARGET_TIMESPAN, target);

        // bound by max target
        let target = cmp::min(DAA1_MAX_TARGET, target);
        Ok(super::to_compact(target))
    }
    else {
        Ok(prev_target)
    }
}

/// Gets the difficulty target using the original DAA but with the emergency adjustment
/// for Bitcoin Cash used from Aug 2017 to Nov 2017
fn get_target_required_daa_eda(db: &Db, hdr: &Header) -> DbResult<u32> {

    if hdr.height % DAA1_RETARGET_BLOCKS == 0 {

        // Per 2-weeks difficulty readjustment is the same as original
        get_target_required_daa_original(db, hdr)
    }
    else {
        let prev = hdr.parent(db)?;
        let prev_target = prev.hdr.bits;

        // we find median_time_11
        let ancestor7 = hdr.ancestor_at(db, hdr.height-7).unwrap();
        let ancestor1 = hdr.parent(db)?;
        let mtp6blocks = ancestor1.median_time(db, 11) - ancestor7.median_time(db, 11);

        // mtp of last 6 blocks within 12 hours? Then just use the previous one
        if mtp6blocks < 12 * 60 * 60 {
            Ok(prev.hdr.bits)
        }
        else {

            // If the last 6 blocks took more then 6 hours, add 1/4th to the target
            let prev_target_u256 = super::from_compact(prev_target);
            let target = prev_target_u256 + (prev_target_u256 >> 2);

            // bound by max target
            let target = cmp::min(DAA1_MAX_TARGET, target);
            Ok(super::to_compact(target))
        }
    }
}

/// gets the work required using the bitcoin cash DAA since nov 2017
fn get_target_required_daa_cash(db: &Db, hdr: &Header) -> DbResult<u32> {

    let prev3 = hdr.parent(db)?;
    let prev2 = prev3.parent(db)?;
    let prev1 = prev2.parent(db)?;

    let first3 = hdr.ancestor_at(db, hdr.height - 145).unwrap();
    let first2 = first3.parent(db)?;
    let first1 = first2.parent(db)?;

    fn get_median(hdrs: [&Header; 3]) -> &Header {
        let mut hdrs = hdrs;
        if hdrs[0].hdr.time > hdrs[2].hdr.time {
            hdrs = [hdrs[2], hdrs[1], hdrs[0]];
        }
        if hdrs[0].hdr.time > hdrs[1].hdr.time {
            hdrs = [hdrs[1], hdrs[0], hdrs[2]];
        }
        if hdrs[1].hdr.time > hdrs[2].hdr.time {
            hdrs = [hdrs[0], hdrs[2], hdrs[1]];
        }
        hdrs[1]
    }

    let prev = get_median([&prev1, &prev2, &prev3]);
    let first = get_median([&first1, &first2, &first3]);

    let timespan = prev.hdr.time - first.hdr.time;

    // bound to limits
    let timespan = cmp::max(72 * 600, timespan);
    let timespan = cmp::min(288 * 600, timespan);

    let work_delta = prev.acc_work - first.acc_work;
    let proj_work = work_delta.mul_u32(600) / U256::new([timespan as u64, 0, 0, 0]);

    // Translate work back to target.
    // We need to compute T = (2^256 / W) - 1 but 2^256 doesn't fit in 256 bits.
    // By expressing 1 as W / W, we get (2^256 - W) / W.
    // We use 2^256 - W = !W + 1
    let target = ((!proj_work) + U256::one()) / proj_work;

    // bound by max target
    let target = cmp::min(DAA1_MAX_TARGET, target);
    Ok(super::to_compact(target))
}
