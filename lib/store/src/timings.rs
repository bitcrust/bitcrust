///
/// Defines a helper struct to quickly gather performance timings.
///
/// The timings are identified by a sequential number and totals are maintained globally.
///
/// See add_block_transactions for usage
///
///


use std::time::{Instant, Duration};
use std::sync::atomic::{AtomicU64, Ordering};

const TIMERS: usize = 10;


pub struct Timings {
    start: Instant,
    timers: [Duration;TIMERS],
    current: usize,
    start_index: usize,
}

impl Timings {
    pub fn new_at(index: usize) -> Self {

        Timings {
            start: Instant::now(),
            timers: [Duration::new(0,0);TIMERS],
            current: index,
            start_index: index,
        }
    }

    pub fn new() -> Self {
        Self::new_at(0)
    }

    /// Stops the current timer and starts the next one.
    ///
    /// As in the "lap" function of a stopwatch
    pub fn next(&mut self) {
        let now = Instant::now();
        self.timers[self.current] = now - self.start;
        self.start = now;
        self.current += 1;
    }


    pub fn get_ms(&self) -> u64 {
        let sum: u64 = (self.start_index..self.current)
            .map(|n| (self.timers[n].as_nanos() / 1000u128) as u64)
            .sum();

        sum / 1000  // us => ms
    }

    /// Ends the timer and returns a displayable string, including totals for each timer since
    /// startup
    pub fn end(&mut self) -> String {

        let mut sum: u64 = 0;
        let mut totals = [0u64;TIMERS];

        let timers = (self.start_index..self.current).map(|n| {
            let micros = (self.timers[n].as_nanos() / 1000u128) as u64;
            sum += micros;
            totals[n] = (GLOBAL_TIMINGS[n].fetch_add(micros, Ordering::Relaxed) + micros)
                / 1_000_000;
            micros.to_string()
        }).collect::<Vec<_>>().join(",");

        let total_s = (self.start_index..self.current).map(|n| totals[n].to_string()).collect::<Vec<_>>().join(",");

        // total for this timer
        format!("us={} ({}), total_s={}", sum, timers, total_s)
    }
}



static GLOBAL_TIMINGS: [AtomicU64;TIMERS] = [
    AtomicU64::new(0),AtomicU64::new(0),AtomicU64::new(0),AtomicU64::new(0),AtomicU64::new(0),
    AtomicU64::new(0),AtomicU64::new(0),AtomicU64::new(0),AtomicU64::new(0),AtomicU64::new(0),
];
