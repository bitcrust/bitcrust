


#[macro_use]
extern crate hex_literal;

#[macro_use]
extern crate log;

#[macro_use]
extern crate encode_derive;

use bitcoinconsensus_sys::VerifyFlags as ScriptVerifyFlags;

mod api;
mod db;

mod pow;
mod merkle_tree;
mod verify;

pub mod builder;

mod chain;
mod timings;


pub use crate::db::transaction::Transaction;
pub use crate::db::header::{Header,HeaderNorm};

pub use network_encoding::*;

pub use crate::api::AddResult;
pub use crate::api::transaction::*;
pub use crate::api::stats::*;

pub use crate::db::{Db, DbError, init, init_empty};

pub use crate::api::error::Error as StoreError;
pub use crate::api::error::ResultExt;
pub use crate::verify::VerifyError;
