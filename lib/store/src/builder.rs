//! Builder functions to ergonomically create transactions and headers for tests
//!
//!
//! These functions work on are Vec<u8>'s.
//!



// These are only used in tests but always compiled for use in doc-tests
#![allow(dead_code)]

use std::collections::HashMap;
use std::sync::Mutex;


use std::sync::atomic;
use network_encoding::*;
use lazy_static::lazy_static;

use crate::pow::U256;

use crate::db::header::HeaderNorm;
use crate::db::transaction::*;

pub use crate::chain::{GENESIS,GENESIS_TX};

/// We use nonces to ensure every header is unique
static NONCE_COUNTER: atomic::AtomicU32 = atomic::AtomicU32::new(0);


// Maintain mapping of header->height
lazy_static! {
    static ref HEIGHTS: Mutex<HashMap<Vec<u8>,u32>> = Mutex::new({
        let mut hm = HashMap::new();
        hm.insert(crate::chain::GENESIS.to_vec(), 0);
        hm
    });
}

fn get_height(prev: &[u8]) -> u32 {

    let heights = HEIGHTS.lock().unwrap();
    * heights.get(&prev.to_vec()).expect("Header on unknown previous")
}

fn set_height(header: &[u8], height: u32) {
    let mut heights = HEIGHTS.lock().unwrap();
    heights.insert(header.to_vec(), height);
}

pub fn tx_coinbase(height: u32) -> Vec<u8> {
    TransactionBuilder::new_coinbase(height)
}

pub fn header_on(prev: &[u8]) -> Vec<u8> {
    <Vec<u8>>::new_header(prev)
}

pub fn block_on(prev: &[u8]) -> (Vec<u8>, Vec<u8>) {
    let height = get_height(prev)+1;

    (
        <Vec<u8>>::new_header(prev),
        txs(vec![TransactionBuilder::new_coinbase(height)]),
    )
}


pub fn txs(txs: Vec<Vec<u8>>) -> Vec<u8> {
    let mut result = encode_buf(&txs.len());
    for tx in txs {
        result.extend(tx);
    }
    result
}


pub trait TransactionBuilder {
    fn new_empty() -> Self;
    fn new_coinbase(height: u32) -> Self;
    fn add_output(self, amount: i64) -> Self;
    fn add_coinbase_input(self, height: u32) -> Self;
}

impl TransactionBuilder for Vec<u8> {

    // Creates an empty transaction
    // Note that this is not valid, but it is used as base, to add inputs and outputs to
    fn new_empty() -> Self {
        vec![0x1,0,0,0,0,0,0xff,0xff,0xff,0xff]
    }

    fn add_output(self, amount: i64) -> Self {
        let mut tx: Transaction = decode_buf(&self).unwrap();
        tx.tx.outputs.push(TxOutput {
            pk_script: vec![0x51],
            value: amount
        });
        encode_buf(&tx)
    }

    fn add_coinbase_input(self, height: u32) -> Self {
        let mut tx: Transaction = decode_buf(&self).unwrap();
        tx.tx.inputs.push(TxInput {
            prev_tx_out: Hash::null(),
            prev_tx_out_idx: 0xFFFF_FFFF,
            script: vec![0x03u8, height as u8, (height >> 8) as u8, (height >> 16) as u8],
            sequence: 0xffff_ffff,
        });
        encode_buf(&tx)
    }

    fn new_coinbase(height: u32) -> Self {
        Self::new_empty()
            .add_output(50_00_000_000)
            .add_coinbase_input(height)

    }
}



pub trait HeaderBuilder {

    fn new_header(prev: &[u8]) -> Self;

    fn get_time(&self) -> u32;

    fn set_delay_minutes(self, minutes: u32) -> Self;

    fn set_target(self, target: U256) -> Self;

}


impl HeaderBuilder for Vec<u8> {

    fn new_header(prev: &[u8]) -> Self {
        let mut hdr: HeaderNorm = decode_buf(prev).unwrap();
        let height = get_height(prev) + 1;
        hdr.time = hdr.time + (10 * 60);
        hdr.nonce = NONCE_COUNTER.fetch_add(1, atomic::Ordering::Relaxed);
        hdr.prev_hash = Hash::from_data(prev);
        hdr.merkle_root = Hash::from_data(&tx_coinbase(height));
        let result = encode_buf(&hdr);
        // remember height
        set_height(&result, height);
        result
    }


    fn get_time(&self) -> u32 {
        let hdr: HeaderNorm = decode_buf(self).unwrap();
        hdr.time
    }

    fn set_delay_minutes(self, minutes: u32) -> Self {
        let mut hdr: HeaderNorm = decode_buf(&self).unwrap();
        hdr.time = hdr.time - (60 * 10) + (60 * minutes);
        encode_buf(&hdr)
    }

    fn set_target(self, target: U256) -> Self {
        let mut hdr: HeaderNorm = decode_buf(&self).unwrap();
        hdr.bits = crate::pow::to_compact(target);
        encode_buf(&hdr)
    }
}


#[cfg(test)]
mod tests {
    use super::*;


    #[test]
    fn test_header_builder() {
        let hdr1 = header_on(crate::chain::GENESIS);
        let hdr2 = header_on(&hdr1);
        let hdr3 = header_on(&hdr1).set_delay_minutes(9);

        assert_eq!(hdr1.get_time() + 60*10, hdr2.get_time());
        assert_eq!(hdr1.get_time() + 60*9 , hdr3.get_time());
    }
}
