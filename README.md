[![Build Status](https://travis-ci.org/tomasvdw/bitcrust.svg?branch=master)](https://travis-ci.org/tomasvdw/bitcrust)
[![Coverage Status](https://coveralls.io/repos/github/tomasvdw/bitcrust/badge.svg)](https://coveralls.io/github/tomasvdw/bitcrust)

# Bitcrust


**Bitcrust** is a full software suite for bitcoin in development.

It includes storage back-end "bitcrust-store" (lib/store) that multiple processes to validate and update the transaction
 and process store, using lock-free concurrency.

The frontend (/peer) connects to a single peer but multiple processes will work together, giving full process isolation.


More information is available at [bitcrust.org](https://bitcrust.org)


## Table of Contents

- [Install](#install)
- [Architecture](#architecture)
- [Running](#running)
- [Contribute](#contribute)

## Install

Bitcrust depends on libbitcoinconsensus which is included in [bitcoinconsensus-sys](https://gitlab.com/bitcrust/bitcoinconsensus-sys)

First install that in a sibling directory of bitcrust.

Then,

    cargo build --release

build the binaries.


## Architecture

Some documentation on the architecture is given in the [wiki](https://gitlab.com/bitcrust/bitcrust/wikis/home).

API documentation is not yet hosted, but can be build with

    cargo doc --no-deps

The bitcrust-store (lib/store) is the main library responsible for validation, storage and indexing

## Running

Bitcrust uses a one peer per process model for process isolation. To run a single peer

    target/release/bitcrust-peer

Addional peers can be started and work together seemlessly

To run the rpc server, or direct rpc commands use

    target/release/bitcrust-rpc

To monitor the peers you can use the watch command:

    watch -n 1 target/release/bitcrust-rpc getdbstats
    watch -n 1 target/release/bitcrust-rpc getpeeerinfo

## Contribute

Help is very much wanted; if you have a fix or improvement, please submit a pull request.
 
Or contact the maintainer for suggestions on work to be done.
