
#![feature(proc_macro_hygiene)]

extern crate bitcrust_store as store;
extern crate bitcrust_sync as sync;

extern crate dirs;
#[macro_use]
extern crate json;
extern crate derive_command;
extern crate getopts;
extern crate shio;
extern crate futures;
extern crate hyper;

use std::collections::HashMap;
use std::{env,path};

use derive_command::command_map;

mod jsonrpc;
mod config;

mod misc;
use crate::misc::*;

mod cmdhelper;
use crate::cmdhelper::*;



fn run_jsonrpc(db: store::Db, cmd_map: CommandMap, _port: u16) {
    jsonrpc::run_server(db, cmd_map);
}

/// Dumps command-specific help
fn show_cmd_help(cmd_map: &CommandMap, cmd: &str) {
    match cmd_map.get::<str>(&cmd.to_string()) {
        None      => eprintln!("Command {} not found", cmd),
        Some(cmd) => println!("{}", (cmd.get_help)())
    };
}

/// Runs a command to stdout
fn run_cmd(db: &store::Db, cmd_map: &CommandMap, cmd: &str, args: Vec<&str>) {

    // convert params to JsonValue
    let params = args.iter().map(|x| json::JsonValue::from(&x[..]) ).collect::<Vec<_>>();
    let params = json::JsonValue::Array(params);

    let cmd = cmd_map.get::<str>(&cmd.to_string()).ok_or(CommandError::CommandNotFound);

    // run command
    match cmd.and_then(|cmd| {(cmd.invoke_json)(&db, params)}) {
        Ok(resp) => println!("{}", json::stringify_pretty(resp, 2)),
        Err(err) => eprintln!("error-code: {}\nerror-message: {:?}", err.jsonrpc_errorcode(), err)
    };

}


fn main() {

    // Generate the command-map from #[command] functions
    let map: CommandMap = command_map!();

    // parse options
    let matches = match arg_matches() {
        Ok(m) => { m }
        Err(f) => {
            eprintln!("{}", f);
            show_help(&map);
            return;
        }
    };

    if matches.opt_present("h")
        || matches.free.is_empty()
        || (matches.free[0] == "help" && matches.free.len() == 1) {
        show_help(&map);
        return;
    }

    let data_dir = matches.opt_str("d")
        .map(|s| path::PathBuf::from(s))
        .unwrap_or_else(|| {
            [
                dirs::home_dir().unwrap_or(path::PathBuf::from(".")),
                path::PathBuf::from("bitcrust-data")
            ].iter().collect()
    });

    let db = store::init(&data_dir)
        .expect("Failed to initialize store");

    sync::init(&data_dir)
        .expect("Failed to initialize sync directories");


    match matches.free[0].as_ref() {
        "help"    =>   show_cmd_help(&map, matches.free[1].as_ref()),
        "server"  => run_jsonrpc(db, map, 8332),
        cmd =>      run_cmd(&db,&map, cmd,
                       matches.free.iter().skip(1).map(|x| &x[..]).collect())
    };

}

fn arg_matches() -> getopts::Result {
    getopts::Options::new()
        .parsing_style(getopts::ParsingStyle::StopAtFirstFree)
        .optopt("d", "data-dir", "", "")
        .optflag("h", "help", "print this help menu")
        .parse(env::args().skip(1))

}

fn show_help(map: &HashMap<&str, Command>) {
println!("bitcrust-rpc 0.1.0

  USAGE:
    bitcrust-rpc [OPTIONS] server               Runs the HTTP-RPC server
    bitcrust-rpc [OPTIONS] <COMMAND> [PARAMS]   Runs a single RPC-command
    bitcrust-rpc [OPTIONS] help [COMMAND]       Show help for an RPC-command

  FLAGS:
    -h, --help       Prints help information

  OPTIONS:
    -d, --data-dir <path>      The data directory for bitcrust data (default=~/bitcrust/)
    -p, --port <port>          The network port to listen to in server mode

  RPC-COMMANDS:
");

    // Display first line for each command helptext
    for k in map.keys() {
        let help = (map[k].get_help)();
        let help_lines = help.lines().collect::<Vec<_>>();
        let help_l1 = help_lines.get(0).unwrap_or(&"no-help").to_string();
        let help_l2 = help_lines.get(1).unwrap_or(&"").to_string();

        println!("    {:32} {}", help_l1, help_l2);
    }
}
