use derive_command::command;
use crate::store;
use crate::store::Hash;
use crate::sync;
use crate::cmdhelper::{CommandError,CommandResult};
use json;


/// getblockcount
/// Returns the number of headers on the chain with the most accumulated work
#[command]
pub fn getblockcount(db: &store::Db) -> CommandResult<u32> {
    // height starts at zero, so count=height+1
    Ok(store::Header::get_best(db).height + 1)
}



/// getbestblockhash
/// Returns the hash of the block with the most accumulated work
///
#[command]
pub fn getbestblockhash(db: &store::Db) -> CommandResult<Hash> {

    Ok(store::Header::get_best(db).hash)
}


/// getblockhash <height>
/// Returns the hash of the block at <height>
///
#[command]
pub fn getblockhash(db: &store::Db, height: u32) -> CommandResult<Hash> {
    store::Header::get_best(db).ancestor_at(db, height)
        .map(|header| header.hash)
        .ok_or(CommandError::HeightOutOfBounds)
}

/// getdbstats
/// Returns general statistics of the blockchain storage
///
#[command]
pub fn getdbstats(db: &store::Db) -> CommandResult<json::JsonValue> {

    let db_stats = store::stats_get(db)?;
    let mut keys: Vec<_> = db_stats.keys().collect();
    keys.sort_unstable();
    let mut stats = json::JsonValue::new_object();
    for k in keys {
        stats[k] = db_stats[k].into();
    }

    Ok(stats)
}


/// gettransaction \"hash\"
/// Returns general statistics of the blockchain storage
///
#[command]
pub fn gettransaction(db: &store::Db, hash: Hash, ) -> CommandResult<json::JsonValue> {

    let tx = store::transaction_get(db, &hash)?.ok_or(CommandError::TransactionNotFound)?;
    let tx = tx.tx;
    // Mock; needs more fields
    Ok(object! {
        "version" => tx.version,
        "lock_time" => tx.lock_time,
        "inputs" => tx.inputs.len(),
        "outputs" => tx.outputs.len()
    })
}



/// getblock \"blockhash\"
/// Returns a block
///
#[command]
pub fn getblock(db: &store::Db, hash: Hash, ) -> CommandResult<json::JsonValue> {

    let header = store::Header::get(db, &hash).ok_or(CommandError::BlockNotFound)?;

    let mut result = object! {
        "hash" => hash.to_string(),
        "confirmations" => 0,
        "height" => header.height,
        "version" => header.hdr.version,
        "versionHex" => format!("{:08x}", header.hdr.version),
        "merkleroot" => header.hdr.merkle_root.to_string(),
        "time" => header.hdr.time,
        "mediantime" => header.median_time(db, 11),
        "nonce" => header.hdr.nonce,
        "bits" => format!("{:08x}", header.hdr.bits),
        "difficulty" => get_difficulty(header.hdr.bits),
        "chainwork" => header.accumulated_work().to_string(),
    };
    if header.parent(db).is_ok() {
        result["previousblockhash"] = header.hdr.prev_hash.to_string().into();
    }
    Ok(result)
}


/// getpeerinfo
/// Returns data on running peers
///
#[command]
pub fn getpeerinfo(_db: &store::Db) -> CommandResult<json::JsonValue> {

    let mut result = array![];

    for peer in sync::PeerInfo::get_all()? {
        let state = peer.state()?;
        result.push(object! {
                "pid" => peer.pid,
                "state" => state.to_string(),
                "host" => peer.get_address().to_string(),
                "user_agent" => peer.user_agent,
                "blocks_in_flight" => peer.blocks_in_flight,
                "task_current" => peer.task_current.unwrap_or("None".to_owned()),
                "read" => peer.bytes_read.total,
                "read_speed" => peer.bytes_read.average_s,
            }).unwrap();
    }
    Ok(result)

}



/// Helper to convert a target ("bits") to a difficulty as used by the RPC interface
///
fn get_difficulty(bits: u32) -> f64 {
    let mut shift = (bits >> 24) & 0xff;
    let mut diff: f64 = 0x0000ffff as f64 / ((bits & 0x00ffffff) as f64);

    while shift < 29 {
        diff *= 256.0;
        shift += 1;
    }
    while shift > 29 {
        diff /= 256.0;
        shift -= 1;
    }
    diff
}
