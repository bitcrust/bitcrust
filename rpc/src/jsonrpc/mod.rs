//!
//! JSON-RPC HTTP server using shio
//!
//!

use shio;
use shio::prelude::*;

use crate::store;
use json;
use crate::cmdhelper::{CommandMap, CommandError};


struct JsonRpcHandler {
    db: store::Db,
    cmd_map: CommandMap
}

impl shio::Handler for JsonRpcHandler {
    type Result = Response;

    fn call(&self, ctx: Context) -> Self::Result {

        ctx.data().concat2().wait().map(move |chunk| {
            let bytes = chunk.into_iter().collect::<Vec<u8>>();

            match run_json_rpc(&self.db, &self.cmd_map, bytes) {
                Err(err) => err,
                Ok(ok) => ok
            }


        }).unwrap()
    }
}

fn run_json_rpc(db: &store::Db, cmd_map: &CommandMap, input: Vec<u8>) -> Result<Response, Response> {

    // json from bytes
    let json = String::from_utf8(input)
        .map_err(|_| JsonRequestError::InvalidCharacterEncoding)
        .and_then(|body| json::parse(&body)
            .map_err(|_| JsonRequestError::InvalidJson)
        ).and_then(|json| match json {
        json::JsonValue::Object(obj) => Ok(obj),
        _ => Err(JsonRequestError::InvalidJson)
    })?;


    let cmd    = json.get("method").ok_or(JsonRequestError::InvalidJson)?;
    let id     = json.get("id").ok_or(JsonRequestError::InvalidJson)?;
    let params = json.get("params").ok_or(JsonRequestError::InvalidJson)?;

    let cmd = cmd_map.get::<str>(&cmd.to_string()).ok_or(CommandError::CommandNotFound);

    // run command
    let result = match cmd.and_then(|cmd| {(cmd.invoke_json)(&db, params.clone())}) {
        Ok(x) =>x,
        Err(err) => {
            // create error response with id
            let body = format!("{{\"result\":null,\"error\":{{\"code\":{},\"message\":\"{:?}\"}},\"id\":{}}}\n",
                                                err.jsonrpc_errorcode(), err, json::stringify(id.clone()));
            return Err(Response::build()
                .status(StatusCode::BadRequest)
                .header(http::header::ContentType::json())
                .header(http::header::ContentLength(body.len() as u64))
                .body(body));
        }
    };

    // success response body
    let response = format!("{{\"result\":{},\"error\":null,\"id\":{}}}\n",
                           json::stringify(result), json::stringify(id.clone()));

    Ok(Response::build()
        .header(http::header::ContentType::json())
        .header(http::header::ContentLength(response.len() as u64))
        .body(response))
}


pub fn run_server(db: store::Db, map: CommandMap) {
    Shio::default()
        .route((Method::POST, "/", JsonRpcHandler { db: db, cmd_map: map }))
        .run(":8332")
        .expect("Failed to start server");
}

#[derive(Debug)]
enum JsonRequestError {
    InvalidCharacterEncoding,
    InvalidJson
}

impl JsonRequestError {
    pub fn jsonrpc_errorcode(&self) -> i32 {
        match self {
            JsonRequestError::InvalidJson => -32700,
            JsonRequestError::InvalidCharacterEncoding => -32700
        }
    }
}

impl From<JsonRequestError> for Response {
    fn from(err: JsonRequestError) -> Response {
        let body = format!("{{\"result\":null,\"error\":{{\"code\":{},\"message\":\"{:?}\"}},\"id\":\"invalid\"}}",
                           err.jsonrpc_errorcode(), err);
        Response::build()
            .status(StatusCode::BadRequest)
            .header(http::header::ContentType::json())
            .header(http::header::ContentLength(body.len() as u64))
            .body(body)
    }
}