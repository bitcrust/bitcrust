
use clap;
use crate::store;
use crate::blk_file;
use std::ops::Range;
use std::path;
use std::collections;
use std::time::Instant;
use dirs;


/// Parse range argument
fn get_range(matches: &clap::ArgMatches) -> Range<usize> {
    match matches.value_of("range") {
        None => { (0..10_000) },
        Some(range) => {
            let parts = range
                .split("..")
                .map(|x| x.parse::<usize>().expect("Expected numerical argument to --range"))
                .collect::<Vec<_>>();

            if parts.len() > 2 {
                panic!("Argument to --range is not a range");
            } else if parts.len() == 1 {
                (0..parts[0])
            } else  {
                if parts[0] >= parts[1] {
                    panic!("Reverse range not allowed")
                } else {
                    (parts[0]..parts[1])
                }
            }
        }
    }
}


pub fn import(db: &store::Db, matches: &clap::ArgMatches) {
    let max_block: u64 = matches.value_of("max-block").unwrap_or("1000000").parse().unwrap();
    let range = get_range(matches);
    info!("Import started for blk files {} .. {}", range.start, range.end);


    let blk_dir: path::PathBuf = matches.value_of("blocks-dir").map(|p| path::PathBuf::from(&p)).unwrap_or_else(|| {
        let mut path = dirs::home_dir().expect("Can't figure out where your $HOME is");
        path.push(".bitcoin");
        path.push("blocks");
        path
    });

    let mut orphans = collections::HashMap::new();
    let now = Instant::now();
    let mut blocks = 0;

    for blk in blk_file::read_blocks(range, blk_dir) {
        let header = &blk[0..80];
        let txs = &blk[80..];
        let add_result = store::Header::add(&db, header).unwrap();
        match add_result {
            store::AddResult::Orphan { parent: prev_hash, .. } => {
                assert!(orphans.insert(prev_hash, (header.to_vec(),txs.to_vec())).is_none());
            },
            store::AddResult::Exists { .. }=> {
                //warn!("Header already exists");
            },
            store::AddResult::New ( header ) => {

                header.add_transactions(&db, txs).unwrap();
                blocks += 1;
                if blocks % 2000 == 0 && blocks > 0 {
                    info!("Processed {} blocks", blocks);
                }
                if blocks >= max_block {
                    break;
                }
                let mut h = header.hash;
                while let Some((orphan_header, txs)) = orphans.remove(&h) {
                    //println!("Adding decendent {} of {}", util::to_hex_rev(&orphan_header.hash[..]), util::to_hex_rev(&hash[..]));
                    if let store::AddResult::New ( header ) = store::Header::add(&db, &orphan_header).unwrap() {
                        header.add_transactions(&db, &txs).unwrap();
                        blocks += 1;
                        if blocks % 2000 == 0 && blocks > 0 {
                            info!("Processed {} blocks", blocks);
                        }
                        if blocks >= max_block {
                            break;
                        }

                        h = header.hash;
                    }
                        else {
                            panic!("insert failed");
                        }

                }
            }
        }

        if blocks >= max_block {
            break;
        }

    }
    let elapsed = now.elapsed().as_secs() * 1000 + now.elapsed().subsec_nanos() as u64 / 1000_000;
    let ms_block = elapsed / blocks;
    info!("Processed {} in {} ms,  {} ms/block", blocks, elapsed, ms_block);

    //let _ = store::header_get(&db,
    //                          &Hash::from("000000000000034a7dedef4a161fa058a2d67a173a90155f3a2fe6fc132e0ebf"))
    //    .unwrap().unwrap();


}
