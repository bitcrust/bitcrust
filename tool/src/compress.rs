use clap;
use crate::blk_file;
use std::path;

pub fn compress1(matches: &clap::ArgMatches) {
    info!("Compression testing");
    info!("1. export tx-ids");
    let blk_dir: path::PathBuf = matches.value_of("blocks-dir").map(|p| path::PathBuf::from(&p)).unwrap_or_else(|| {
        let mut path = dirs::home_dir().expect("Can't figure out where your $HOME is");
        path.push(".bitcoin");
        path.push("blocks");
        path
    });

    for blk in blk_file::read_blocks(0..1000000, blk_dir) {
        let _header = &blk[0..80];
        let _txs = &blk[80..];
    }

}
