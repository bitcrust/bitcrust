/// Tools for development
///
///
#[macro_use]
extern crate clap;

extern crate byteorder;
extern crate dirs;
extern crate bitcrust_store as store;

#[macro_use]
extern crate encode_derive;


extern crate serde;

#[macro_use]
extern crate log;
extern crate flexi_logger;

use std::path;
use clap::{App,Arg,ArgMatches, SubCommand};

mod import;
mod blk_file;
mod compress;

pub fn matches<'a>() -> ArgMatches<'a> {
    App::new("bitcrust-tool")
        .version(crate_version!())
        .author("Tomas van der Wansem")
        .arg(Arg::with_name("data-dir")
            .short("d")
            .long("data-dir")
            .takes_value(true)
            .help("Location of the Bitcrust data files. Default: $HOME/bitcrust/"))
        .subcommand(SubCommand::with_name("test-spendtree"))
        .subcommand(SubCommand::with_name("test-compress1"))
        .subcommand(SubCommand::with_name("perf"))
        .subcommand(SubCommand::with_name("import")
            .about("Imports data in the format of C++ implementations")
            .arg(Arg::with_name("blocks-dir")
                .short("b")
                .long("blocks-dir")
                .takes_value(true)
                .help("Location of the 'blocks'-files. Default: $HOME/.bitcoin/blocks/"))
            .arg(Arg::with_name("max-block")
                .short("m")
                .long("max-block")
                .takes_value(true)
                .help("Number of blocks to import"))
            .arg(Arg::with_name("range")
                .short("r")
                .long("range")
                .takes_value(true)
                .help("Range of block file numbers to import in the form start..end. Defaults to all files"))
        ).get_matches()

}


fn main() {
    flexi_logger::Logger::with_env_or_str("info")
        .format(flexi_logger::opt_format)
        .start()
        .expect("Failed to initialize logger");

    let matches = matches();
    let data_dir: path::PathBuf = matches.value_of("data-dir").map(|p| path::PathBuf::from(&p)).unwrap_or_else(|| {
        let mut path = dirs::home_dir().expect("Can't figure out where your $HOME is");
        path.push("bitcrust");
        path
    });
    let db = store::init(&data_dir)
        .expect(&format!("Couldn't initialize store at {}", &data_dir.to_string_lossy()));

    match matches.subcommand() {
        ("import", Some(matches)) => import::import(&db, matches),
        ("test-compress1", Some(matches)) => compress::compress1(matches),
        ("perf", Some(matches)) => perf(&db, matches),

        _ => println!("{}", matches.usage())
    }
}

fn perf(db: &store::Db, _matches: &ArgMatches) {
    const INTERVAL: u64 = 400_000_000;
    let mut blocks = 0;
    let mut txs = 0;

    loop {
        let stats = store::stats_get(db).unwrap();

        let blocks_delta = stats["blocks.count"] - blocks;
        let txs_delta = stats["transactions.count"] - txs;

        println!("Blocks {:8}  txs {:8}", (blocks_delta as u64 *1_000_000_000)/  INTERVAL,
                 (txs_delta as u64 *1_000_000_000)/  INTERVAL);
        blocks += blocks_delta;
        txs += txs_delta;

        std::thread::sleep(std::time::Duration::from_nanos(INTERVAL));

    }
}
