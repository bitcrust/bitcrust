//!
//! Module to access bitcoin-core style blk files
//! that store the integral blockchain



use byteorder::{ReadBytesExt, LittleEndian};
use std::io::BufReader;
use std::fs::File;
use std::path::PathBuf;
use std::ops::Range;
use std::io;


/// Magic number stored at the start of each block
const MAGIC: u32 = 0xD9B4BEF9;


pub struct BlkFileIterator {
    file_nr: usize,
    end_file_nr: usize,
    reader: BufReader<File>,
    base_path: PathBuf,

}

impl Iterator for BlkFileIterator {
    type Item = Vec<u8>;

    fn next(&mut self) -> Option<Vec<u8>> {

        let blk = read_block(&mut self.reader).unwrap();
        if blk.is_none() {
            self.file_nr += 1;
            if self.file_nr == self.end_file_nr {
                return None;
            }
            let mut path = self.base_path.clone();
            path.push(format!("blk{:05}.dat", self.file_nr));
            if !path.exists() {
                return None;
            }
            let f = File::open(path).unwrap();
            self.reader = BufReader::new(f);
            read_block(&mut self.reader).unwrap()

        }
        else {
            blk
        }
    }
}
pub fn read_blocks(range: Range<usize>, base_path: PathBuf) -> BlkFileIterator {

    let mut path = base_path.clone();
    path.push(format!("blk{:05}.dat", range.start));
    if !path.exists() {
        panic!("No blk-files found.");
    }
    let f = File::open(path).unwrap();
    let rdr = BufReader::new(f);

    BlkFileIterator { file_nr: range.start, end_file_nr: range.end, reader: rdr, base_path: base_path }
}

/// Reads a block from a blk_file as used by
/// bitcoin-core and various other implementations
pub fn read_block(rdr: &mut dyn io::Read) -> Result<Option<Vec<u8>>, io::Error> {

    loop {
        let magicnr = rdr.read_u32::<LittleEndian>();
        match magicnr {
            Err(_)     => return Ok(None), // assume EOF
            Ok(m) => match m {

                // TODO investigate; // Can't really find it in the cpp.
                // this happens on bitcrust-1 at block  451327
                // file blk000760, file pos 54391594
                // first 8 zero-bytes before magicnr
                // for now we skip them; not too important as we
                // might not want to support this type of import anyway
                0     => continue,

                MAGIC => break,
                _     =>return Err(io::Error::new(io::ErrorKind::InvalidData, "Incorrect magic number"))
            }
        }

    }


    let length     = rdr.read_u32::<LittleEndian>()?;
    let mut buffer = vec![0; length as usize];

    rdr.read_exact(&mut buffer)?;


    Ok(Some(buffer))


}


#[cfg(test)]
mod tests {

    use super::*;
    use network_encoding::*;
    use serde::{Serialize,Deserialize};
    use serde_bytes;
    use serde_bitcoin::{to_bytes};

    #[derive(Debug,Encode,Decode, Serialize)]
    pub struct Hash {
        h: [u8;32]
    }

    impl<'de> Deserialize<'de> for Hash {
        fn deserialize<D>(deserializer: D) -> Result<Hash, D::Error>
        where
            D: serde::Deserializer<'de>,
            {
                let h = <[u128;2]>::deserialize(deserializer)?;
                Ok(Hash {
                    h: unsafe { std::mem::transmute(h)}
                })
            }
    }

    #[derive(Debug,Decode,Encode,Default, Serialize, Deserialize)]
    pub struct TransactionNorm {
        pub version:     u32,           // Only valid if inputs are loaded
        pub inputs:      Vec<TxInput>,  // Zero-length if the inputs aren't loaded
        pub outputs:     Vec<TxOutput>, // Zero-length if the outputs aren't loaded
        pub lock_time:   u32,           // Only valid if inputs are loaded
    }

    #[derive(Debug,Encode,Decode, Serialize, Deserialize)]
    pub struct TxInput {
        pub prev_tx_out: Hash,
        pub prev_tx_out_idx: u32,
        #[serde(with = "serde_bytes")]
        pub script: Vec<u8>,
        pub sequence: u32,
    }

    #[derive(Debug,Encode,Decode, Serialize, Deserialize)]
    pub struct TxOutput {
        pub value:     i64,
        #[serde(with = "serde_bytes")]
        pub pk_script: Vec<u8>
    }

    #[derive(Encode,Decode,Debug, Serialize,Deserialize)]
    pub struct HeaderNorm {
        pub version:     i32,
        pub prev_hash:   Hash,
        pub merkle_root: Hash,
        pub time:        u32,
        pub bits:        u32,
        pub nonce:       u32,
    }



    #[test]
    fn test_decode_perf() {

        use std::time::Instant;
        use std::path;

        let blk_dir: path::PathBuf = {
            let mut path = dirs::home_dir().expect("Can't figure out where your $HOME is");
            path.push(".bitcoin");
            path.push("blocks");
            path
        };

        let start = Instant::now();
        let mut v: Vec<_> = read_blocks(1..2, blk_dir).collect();
        dbg!(start.elapsed());
        let start = Instant::now();
        let mut sum: usize=0;
        /*for blk in &v {

            //println!("hdr.ver={}", hdr.version);
            let hdr: HeaderNorm = decode_buf(&blk[0..80]).unwrap();
            let txs: Vec<TransactionNorm> = decode_buf(&blk[80..]).unwrap();
            sum += txs.len();
            //println!("{} txs", txs.len());
        }*/
        dbg!(start.elapsed());
        println!("SUM={}",sum);
        let start = Instant::now();
        for blk in &v {

            //println!("hdr.ver={}", hdr.version);
            let hdr: HeaderNorm = serde_bitcoin::from_bytes(&blk[0..80]).unwrap();
            let txs: Vec<TransactionNorm> = serde_bitcoin::from_bytes(&blk[80..]).unwrap();
            //println!("{} txs", txs.len());
            assert_eq!(hdr.prev_hash.h[31], 0);
            let mut buf_hdr = serde_bitcoin::to_bytes(&hdr);

            let mut buf_txs = serde_bitcoin::to_bytes(&txs);
            buf_hdr.append(&mut buf_txs);
            let b1 = &buf_hdr[..];
            let b2 = &blk[0..buf_hdr.len()];
            assert_eq!(b1,b2);
        }
        dbg!(start.elapsed());
    }
}
